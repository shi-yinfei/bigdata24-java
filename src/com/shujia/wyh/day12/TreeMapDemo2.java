package com.shujia.wyh.day12;

import java.util.Comparator;
import java.util.TreeMap;

/*
    使用TreeMap集合存储元素，key是学生类型，按照学生的年龄从大到小排序(只针对key的元素)
 */
public class TreeMapDemo2 {
    public static void main(String[] args) {
        //创建集合对象
        TreeMap<Student, String> map = new TreeMap<>(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                int i = o2.getAge() - o1.getAge();
                int i2 = (i == 0) ? o2.getName().compareTo(o1.getName()) : i;
                return i2;
            }
        });

        //创建学生元素对象
        Student s1 = new Student("陆澳", 18);
        Student s2 = new Student("李佳豪", 17);
        Student s3 = new Student("王宇杰", 19);
        Student s4 = new Student("张怀远", 18);
        Student s5 = new Student("陆澳", 18);

        //向集合中添加元素
        map.put(s1, "诸葛亮");
        map.put(s2, "李白");
        map.put(s3, "安其拉");
        map.put(s4, "小乔");
        map.put(s5, "嬴政");

        System.out.println(map);
    }
}
