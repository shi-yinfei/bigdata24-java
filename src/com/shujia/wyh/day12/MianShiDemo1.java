package com.shujia.wyh.day12;

import java.util.HashMap;
import java.util.Hashtable;

/*
    HashMap和Hashtable的区别（必问！）
    1、HashMap线程不安全，Hashtable线程是安全的
    2、HashMap中的key和value值都允许为空null,Hashtable的key和value都不允许为null
 */
public class MianShiDemo1 {
    public static void main(String[] args) {
//        HashMap<String, String> map1 = new HashMap<>();
//        map1.put("aaa",null);
//        map1.put(null,"bbb");

//        System.out.println(map1);

        Hashtable<String, String> map2 = new Hashtable<>();
//        map2.put("aaa",null);
//        map2.put(null,"bbb");


        System.out.println(map2);
    }
}
