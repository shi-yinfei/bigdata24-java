package com.shujia.wyh.day12;

import java.io.File;
import java.io.FilenameFilter;

/*
    文件名称过滤器
    public String[] list(FilenameFilter filter)
    public File[] listFiles(FilenameFilter filter)

 */
public class FileDemo6 {
    public static void main(String[] args) {
        //1、将E盘根目录封装成File对象
        File file = new File("E:\\");

        //2、调用listFiles()方法获取文件夹中所有的File对象
        File[] files = file.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
//                return true;
                //这里的返回值是要根据实际情况来返回
                //只有这里的返回值是true，才会将目标文件放入到数组中
                File file1 = new File(dir, name);
                return file1.isFile() & file1.getName().endsWith("jpg");
            }
        });

        for (File file1 : files) {
            System.out.println(file1);
        }
    }
}
