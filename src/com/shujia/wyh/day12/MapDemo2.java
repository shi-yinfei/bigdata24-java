package com.shujia.wyh.day12;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/*
    Map集合的成员方法：
        V get(Object key)
        Set<K> keySet()
        Collection<V> values()
        Set<Map.Entry<K,V>> entrySet()

 */
public class MapDemo2 {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();

        // V put(K key,V value)  返回历史key对应的value值
//        System.out.println(map.put("王宇杰", "打篮球"));
//        System.out.println(map.put("王宇杰", "敲代码"));
        map.put("王宇杰", "打篮球");
        map.put("张怀远", "唱歌");
        map.put("陆澳", "跳舞");
        map.put("杨志", "rapper");
        map.put("张玮", "敲代码");
        map.put("李佳豪", "敲代码");

        System.out.println(map);
        System.out.println("-------------------------------");
//        //V get(Object key)  根据key获取对应的value值
//        System.out.println(map.get("王宇杰"));
//        System.out.println(map);
//
//        //Set<K> keySet() 获取Map集合中所有的键
//        Set<String> keyedSet = map.keySet();
//        for (String s1 : keyedSet) {
//            System.out.println(s1);
//        }

        // Map集合遍历的第一种方式：先获取所有的key，然后根据key获取对应的value值
//        Set<String> keyedSet = map.keySet();
//        for (String key : keyedSet) {
//            String value = map.get(key);
//            System.out.println(key + "---" + value);
//        }

        //Collection<V> values()  获取所有的value值
//        Collection<String> values = map.values();
//        for (String value : values) {
//            System.out.println(value);
//        }

        //Set<Map.Entry<K,V>> entrySet() 获取所有的键值对组成的集合
        // Map集合遍历的第二种方式：先获取所有的key-value，然后分别获取每个键值对的key和value
        Set<Map.Entry<String, String>> entrySet = map.entrySet();
        for (Map.Entry<String, String> keyValue : entrySet) {
            String key = keyValue.getKey();
            String value = keyValue.getValue();
            System.out.println(key + "---" + value);
        }


    }
}
