package com.shujia.wyh.day12;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
    填坑：即使Vector是线程安全的，我们今后也不用它，我们使用使用Collections类将不安全的ArrayList变成线程安全的

    static <T> Collection<T> synchronizedCollection(Collection<T> c)
        返回由指定集合支持的同步（线程安全）集合。
    static <T> List<T> synchronizedList(List<T> list)
        返回由指定列表支持的同步（线程安全）列表。
    static <K,V> Map<K,V> synchronizedMap(Map<K,V> m)
        返回由指定地图支持的同步（线程安全）映射。
    static <T> Set<T> synchronizedSet(Set<T> s)
        返回由指定集合支持的同步（线程安全）集。


 */
public class CollectionsDemo2 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();

        List<String> list2 = Collections.synchronizedList(list);

        //后续使用和正常使用一个集合没有任何区别。
//        list2.add("das")
    }
}
