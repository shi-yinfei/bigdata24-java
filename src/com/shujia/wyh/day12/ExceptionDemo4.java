package com.shujia.wyh.day12;

import java.io.FileInputStream;

/*
        finally:无论try中的代码是否出现异常，都会执行，特殊情况除外
 */
public class ExceptionDemo4 {
    public static void main(String[] args) {
        // 运行时期异常一般都是因为自己写程序语法逻辑不够严谨导致，自己修改代码就可以
        try {
            int a = 10;
            int b = 0;
            System.out.println(a/b);
        }catch (Exception e){
            e.printStackTrace();
//            System.exit(0); // 特殊情况，程序强制退出
        }finally {
            System.out.println("这是finally中的代码");
        }

        System.out.println("Hello World!");

        try {
            //将来在写代码的过程中，发现语法都没有问题，但是程序出现了红色下划线，这时候是编译时期异常，必须要处理
            FileInputStream fileInputStream = new FileInputStream("");
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
