package com.shujia.wyh.day12;

import java.util.ArrayList;
import java.util.Collections;

/*
    Collections是java专门提供给我们操作集合的工具类
    public static <T> void sort(List<T> list)
    public static <T> int binarySearch(List<?> list,T key)
    public static <T> T max(Collection<?> coll)
    public static void reverse(List<?> list)
    public static void shuffle(List<?> list)


 */
public class CollectionsDemo1 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(12);
        list.add(54);
        list.add(3);
        list.add(89);
        list.add(35);
        System.out.println(list);
//        //public static <T> void sort(List<T> list)
//        //对List相关集合做排序
//        Collections.sort(list);  //底层调用的是Arrays的sort方法
//        System.out.println(list);
//
//        //public static <T> int binarySearch(List<?> list,T key)
//        //前提：序列是有序的
//        System.out.println(Collections.binarySearch(list,100));

        //public static <T> T max(Collection<?> coll)
//        System.out.println(Collections.max(list));

        //public static void reverse(List<?> list)
//        Collections.reverse(list);
//        System.out.println(list);

        //public static void shuffle(List<?> list) 理解为将数据打乱重新分布，至于有没有规律的打乱和分布不清楚
        //随机打乱
        Collections.shuffle(list);
        System.out.println(list);

    }
}
