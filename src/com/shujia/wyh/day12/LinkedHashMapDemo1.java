package com.shujia.wyh.day12;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/*
    LinkedHashMap: key底层数据结构是以双链表和哈希表构成
 */
public class LinkedHashMapDemo1 {
    public static void main(String[] args) {
        LinkedHashMap<Student, String> map = new LinkedHashMap<>();

        //创建key元素对象
        Student s1 = new Student("张怀远", 18);
        Student s2 = new Student("李佳豪", 19);
        Student s3 = new Student("陆澳", 18);
        Student s4 = new Student("王宇杰", 17);
        Student s5 = new Student("张怀远", 18);

        //需求：当学生的姓名和年龄一样的时候，我们认为是同一个学生对象
        //向集合中添加元素
        map.put(s1, "安徽合肥");
        map.put(s2, "安徽铜陵");
        map.put(s3, "安徽滁州");
        map.put(s4, "安徽淮南");
        map.put(s5, "安徽蚌埠");

        //遍历集合
        Set<Map.Entry<Student, String>> entries = map.entrySet();
        for (Map.Entry<Student, String> keyValue : entries) {
            Student key = keyValue.getKey();
            String value = keyValue.getValue();
            System.out.println(key + "---" + value);
        }
    }
}
