package com.shujia.wyh.day12;

import java.util.HashMap;
import java.util.Map;

/*
    Map相关集合特点：与Collection集合不同，而Map集合中的元素是由key-value格式构成的,Map集合继承体系和Collection集合继承体系无关
    借助子类HashMap来创建一个Map集合对象

    Map集合中共同的成员方法：
        V put(K key,V value)
        V remove(Object key)
        void clear()
        boolean containsKey(Object key)
        boolean containsValue(Object value)
        boolean isEmpty()
        int size()

    Map集合的特点：
        1、key不允许发生重复


 */
public class MapDemo1 {
    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();

        // V put(K key,V value)  返回历史key对应的value值
//        System.out.println(map.put("王宇杰", "打篮球"));
//        System.out.println(map.put("王宇杰", "敲代码"));
        map.put("王宇杰", "打篮球");
        map.put("张怀远", "唱歌");
        map.put("陆澳", "跳舞");
        map.put("杨志", "rapper");
        map.put("张玮", "敲代码");
        map.put("李佳豪", "敲代码");

        System.out.println(map);


        //V remove(Object key)  通过key删除一个键值对
//        System.out.println(map.remove("张玮"));

        //void clear()
//        map.clear();

//        //boolean containsKey(Object key) 判断key是否存在
//        System.out.println(map.containsKey("张玮"));
//
//        //boolean containsValue(Object value) 判断value值是否存在
//        System.out.println(map.containsValue("敲代码"));
//
//        //boolean isEmpty() 判断集合是否为空
//        System.out.println(map.isEmpty());

        //int size() 获取键值对的个数
        System.out.println(map.size());

        System.out.println(map);

    }
}
