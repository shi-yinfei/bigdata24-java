package com.shujia.wyh.day12;

import java.util.TreeMap;

/*
    TreeMap是Map接口的实现子类：key的底层数据结构是红黑树，保证元素的唯一性和排序性
    对key排序的两种方式：
        自然排序
        比较器排序

 */
public class TreeMapDemo1 {
    public static void main(String[] args) {
        //创建一个集合对象
        TreeMap<String, String> map = new TreeMap<>();

        //向集合中添加元素
        map.put("zhangjie","谢娜");
        map.put("dengchao","孙俪");
        map.put("wangyujie","刘亦菲");
        map.put("xiaohu","冯提莫");

        System.out.println(map);
    }
}
