package com.shujia.wyh.day12;

/*
    try..catch语句的注意事项
        1、可以写多个catch
        2、如果多个catch之间存在父子继承关系，需要将父类异常写在最后
        3、如果可能出现的多个异常之间没有继承关系的话，可以采用|的方式写在一个catch
 */
public class ExceptionDemo3 {
    public static void main(String[] args) {
        try {
            int a = 10;
            int b = 5;

            System.out.println(a / b); //ArithmeticException


            int[] arr = {11, 22, 33, 44};
            System.out.println(arr[4]); //ArrayIndexOutOfBoundsException
        } catch (ArithmeticException | ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        System.out.println("Hello World!");

    }
}
