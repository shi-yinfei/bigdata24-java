package com.shujia.wyh.day12;

import java.io.File;

/*
    File: 在java中使用File用来描述文件或者文件夹

    学习File类的构造方法，将磁盘中的文件或者文件夹封装成File对象
    public File(String pathname)
    public File(String parent,String child)
    public File(File parent,String child)

 */
public class FileDemo1 {
    public static void main(String[] args) {
        //路径：
        //相对路径：以项目作为根目录  举例：src//com//shujia//wyh//day12//a.txt
        //绝对路径（完整路径）：windows下，带有盘符的路径 E:\projects\IDEAProjects\bigdata24-java\bigdata24-java\src\com\shujia\wyh\day12\a.txt
        //public File(String pathname)
//        File file = new File("E:\\projects\\IDEAProjects\\bigdata24-java\\bigdata24-java\\src\\com\\shujia\\wyh\\day12\\a.txt");
//        System.out.println(file);

        //public File(String parent,String child)
//        File file = new File("E:\\projects\\IDEAProjects\\bigdata24-java\\bigdata24-java\\src\\com\\shujia\\wyh\\day12", "b.txt");
//        System.out.println(file);

        //public File(File parent,String child)
//        File file = new File("E:\\projects\\IDEAProjects\\bigdata24-java\\bigdata24-java\\src\\com\\shujia\\wyh\\day12");
//        File file1 = new File(file, "a.txt");
//        System.out.println(file1);

    }
}
