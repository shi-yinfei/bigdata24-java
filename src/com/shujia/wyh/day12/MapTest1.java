package com.shujia.wyh.day12;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/*
    "aababcabcdabcde",获取字符串中每一个字母出现的次数要求结果:a(5)b(4)c(3)d(2)e(1)

 */
public class MapTest1 {
    public static void main(String[] args) {
        //1、创建TreeMap集合对象
        TreeMap<Character, Integer> map = new TreeMap<>();

        String s = "aababcabcdabcde";

        //2、将字符串转成字符数组
        char[] chars = s.toCharArray();

        //3、遍历字符数组得到每一个字符
        for (char c : chars) {
            //4、判断集合中是否存在该字符作为key
            //如果不存在，就将字符作为key存储，value是1
            //如果存在，将value取出来+1，再重新赋值回去
            if (!map.containsKey(c)) {
                map.put(c, 1);
            } else {
                map.put(c, map.get(c) + 1);
            }
        }

        //5、遍历集合
        //创建StringBuffer来做字符串拼接
        StringBuffer sb = new StringBuffer();
        Set<Map.Entry<Character, Integer>> entries = map.entrySet();
        for (Map.Entry<Character, Integer> entry : entries) {
            Character key = entry.getKey();
            Integer value = entry.getValue();
            sb.append(key).append("(").append(value).append(")");
        }

        //6、将StringBuffer转成字符串输出
        System.out.println(sb);


    }
}
