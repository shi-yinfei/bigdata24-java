package com.shujia.wyh.day12;

import java.io.File;

/*
    创建功能
        public boolean createNewFile()
        public boolean mkdir()
        public boolean mkdirs()
    删除功能
        public boolean delete()
    重命名功能
        public boolean renameTo(File dest)
    判断功能
        public boolean isDirectory()
        public boolean isFile()
        public boolean exists()
        public boolean canRead()
        public boolean canWrite()
        public boolean isHidden()


 */
public class FileDemo2 {
    public static void main(String[] args) throws Exception {
        //需求：要在day12目录下创建一个文件b.txt
//        File file = new File("E:\\projects\\IDEAProjects\\bigdata24-java\\bigdata24-java\\src\\com\\shujia\\wyh\\day12\\b.txt");
//        //public boolean createNewFile()
//        System.out.println(file.createNewFile());
//
//        //public boolean mkdir()  创建单极文件夹
//        System.out.println(file.mkdir());
        //你要搞清楚今后要创建文件还是文件夹，因为类型不一样，骑白马不一定是王子

        //public boolean mkdirs()
//        File file = new File("E:\\projects\\IDEAProjects\\bigdata24-java\\bigdata24-java\\src\\com\\shujia\\wyh\\day12\\aaa\\bbb\\ccc");
////        file.mkdir();
//        file.mkdirs();

        //需求：删除刚刚创建的aaa文件
        //delete删除文件夹的时候，确保文件夹中没有其他内容
//        File file = new File("src\\com\\shujia\\wyh\\day12\\aaa\\bbb\\ccc");
//        System.out.println(file.delete());
//
//        File file1 = new File("src\\com\\shujia\\wyh\\day12\\aaa\\bbb");
//        System.out.println(file1.delete());
//
//        File file2 = new File("src\\com\\shujia\\wyh\\day12\\aaa");
//        System.out.println(file2.delete());

//        File file = new File("src\\com\\shujia\\wyh\\day12\\a.txt");
//        File file1 = new File("src\\com\\shujia\\wyh\\day12\\杨志.txt");
//        System.out.println(file.renameTo(file1));

        File file = new File("src\\com\\shujia\\wyh\\day12\\杨志.txt");
        //public boolean isDirectory() 判断是否是文件夹
        System.out.println(file.isDirectory());
        //public boolean isFile() 判断是否是文件
        System.out.println(file.isFile());
        //public boolean exists() 判断目标File对象是否存在
        System.out.println(file.exists());
        //public boolean canRead() 判断该File对象是否可读
        System.out.println(file.canRead());
        //public boolean canWrite() 判断该File对象是否可写
        System.out.println(file.canWrite());
        //public boolean isHidden() 判断该File对象是否为隐藏文件
        System.out.println(file.isHidden());


    }
}
