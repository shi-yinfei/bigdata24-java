package com.shujia.wyh.day12;

/*
    因为JVM默认遇到异常，程序就会终止，导致后续代码不执行，所以要学习如何处理异常的同时，还要保证程序能正常运行
    java遇到异常的处理方案：
        1、try…catch…finally
        2、throws
 */
public class ExceptionDemo2 {
    public static void main(String[] args) {
        /**
         * 语句定义格式：
         *   try{
         *       可能会出问题的代码；
         *   }catch(异常类类型 变量名){
         *       处理的方式;
         *   }finally{
         *      无论是否出现异常都会执行这块；
         *   }
         *
         */
        int[] arr = {11,22,34,55};
//        System.out.println(arr[4]); //ArrayIndexOutOfBoundsException

        //当try中的代码出现异常的时候，中止try中的代码执行，JVM内部会创建一个对应的异常类对象
        //假如现在出现了ArrayIndexOutOfBoundsException，JVM内部就会创建一个ArrayIndexOutOfBoundsException类的对象
        //去匹配对应的catch中的异常类型，如果有匹配对应的catch，就会执行对应catch中代码
        //程序不会停止，后续代码正常运行
        try {
            System.out.println(arr[4]);
        }catch (Exception e){ //Exception e = new ArrayIndexOutOfBoundsException()
            //void printStackTrace()
            //将此throwable和其追溯打印到标准错误流。
//            System.out.println("取了不该取的索引");
            e.printStackTrace();
        }


        System.out.println("陆澳真帅！！");
    }
}
