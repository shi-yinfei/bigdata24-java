package com.shujia.wyh.day12;

import java.io.File;

/*
    判断E盘目录下是否有后缀名为.jpg的文件，如果有，就输出此文件名称

 */
public class FileDemo5 {
    public static void main(String[] args) {
        //1、将E盘封装成File对象
        File file = new File("E:\\");

        //2、获取文件夹下所有File对象
        File[] files = file.listFiles();
        for (File file1 : files) {
            //今天因为没有学习递归，所以无法下钻到每一个文件夹
            if(file1.isFile() && file1.getName().endsWith("jpg")){
                System.out.println(file1.getName());
            }
        }
    }
}
