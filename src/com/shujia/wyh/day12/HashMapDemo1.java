package com.shujia.wyh.day12;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/*
    HashMap<Student,String>
    当key是自定义类型的时候，如果要满足key唯一，就必须让自定义类中重写父类中的hashCode()方法和equals()方法
 */
public class HashMapDemo1 {
    public static void main(String[] args) {
        //创建集合对象
        HashMap<Student, String> map = new HashMap<>();

        //创建key元素对象
        Student s1 = new Student("张怀远", 18);
        Student s2 = new Student("李佳豪", 19);
        Student s3 = new Student("陆澳", 18);
        Student s4 = new Student("王宇杰", 17);
        Student s5 = new Student("张怀远", 18);

        //需求：当学生的姓名和年龄一样的时候，我们认为是同一个学生对象
        //向集合中添加元素
        map.put(s1, "安徽合肥");
        map.put(s2, "安徽铜陵");
        map.put(s3, "安徽滁州");
        map.put(s4, "安徽淮南");
        map.put(s5, "安徽蚌埠");

        //遍历集合
        Set<Map.Entry<Student, String>> entries = map.entrySet();
        for (Map.Entry<Student, String> keyValue : entries) {
            Student key = keyValue.getKey();
            String value = keyValue.getValue();
            System.out.println(key + "---" + value);
        }
    }
}
