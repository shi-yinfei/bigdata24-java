package com.shujia.wyh.day12;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
    基本获取功能
    public String getAbsolutePath()
    public String getPath()
    public String getName()
    public long length()
    public long lastModified()

 */
public class FileDemo3 {
    public static void main(String[] args) {
        File file = new File("src\\com\\shujia\\wyh\\day12\\杨志.txt");

        //public String getAbsolutePath()
        //获取绝对路径（完整路径）
        System.out.println(file.getAbsolutePath());

        //public String getPath() 获取相对路径
        System.out.println(file.getPath());

        //public String getName() 获取File对象的名字
        System.out.println(file.getName());

        //public long length() 获取字节数
        System.out.println(file.length());

        //public long lastModified() 获取文件最后修改的时间戳、
        Long l = file.lastModified();
        Date date = new Date(l);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String s = sdf.format(date);
        System.out.println(s);
    }
}
