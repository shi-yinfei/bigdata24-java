package com.shujia.wyh.day12;

import java.io.File;

/*
    基本获取功能：
        public String[] list()
        public File[] listFiles()

 */
public class FileDemo4 {
    public static void main(String[] args) {
        File file = new File("D:\\");

        //public String[] list() 获取文件夹下的所有文件和文件夹的名称组成的数组
//        String[] list = file.list();
//        for (String s1 : list) {
//            System.out.println(s1);
//        }

        //public File[] listFiles()  获取文件夹下的所有文件和文件夹的File对象组成的数组
        File[] files = file.listFiles();
        for (File file1 : files) {
            System.out.println(file1);
        }
    }
}
