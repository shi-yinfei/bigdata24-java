package com.shujia.wyh.day12.homework;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class TestSet {

    public static void main(String[] args) {

        Book2 b1 = new Book2(1000, "b1", 30.5, "glls");

        Book2 b1_1 = new Book2(1000, "b1", 30, "glls");

        Book2 b2 = new Book2(1000, "b2", 50, "glls");

        Book2 b3 = new Book2(1001, "b3", 30.5, "glls");

        Book2 b4 = new Book2(1002, "b4", 30.5, "glls");

        Book2 b5 = new Book2(1003, "b5", 50, "glls1");

        //使用HashSet存储图书并遍历

        Set<Book2> hashSet = new HashSet<Book2>();

        hashSet.add(b1);

        hashSet.add(b1);

        hashSet.add(b2);

        hashSet.add(b3);

        hashSet.add(b4);

        hashSet.add(b5);

        hashSet.add(b1_1);

        System.out.println("遍历输出hashset");

        System.out.println(hashSet.size());

        for(Book2 book:hashSet){

            System.out.println(book.toString());

        }

        //使用TreeSet存储图书并遍历

        Set<Book2> treeSet = new TreeSet<Book2>();

        treeSet.add(b1);

        treeSet.add(b1);

        treeSet.add(b2);

        treeSet.add(b3);

        treeSet.add(b4);

        treeSet.add(b5);

        treeSet.add(b1_1);

        System.out.println("遍历输出treeset");
        System.out.println(treeSet.size());

        for(Book2 book:treeSet){

            System.out.println(book.toString());

        }

    }

}
