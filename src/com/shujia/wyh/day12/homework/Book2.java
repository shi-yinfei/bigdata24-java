package com.shujia.wyh.day12.homework;
public class Book2 implements Comparable<Book2> {

        public int id;

        public String name;

        public double price;

        public String press;

        public Book2() {

            super();

        }

        public Book2(int id, String name, double price, String press) {

            super();

            this.id = id;

            this.name = name;

            this.price = price;

            this.press = press;

        }

        public int compareTo(Book2 o) {

            return this.id-o.id;

        }

        @Override

        public int hashCode() {

            final int prime = 31;

            int result = 1;

            result = prime * result + id;

            result = prime * result + ((name == null) ? 0 : name.hashCode());

            result = prime * result + ((press == null) ? 0 : press.hashCode());

            long temp;

            temp = Double.doubleToLongBits(price);

            result = prime * result + (int) (temp ^ (temp >>> 32));

            return result;

        }

        @Override

        public boolean equals(Object obj) {

            if (this == obj)

                return true;

            if (obj == null)

                return false;

            if (getClass() != obj.getClass())

                return false;

            Book2 other = (Book2) obj;

            if (id != other.id)

                return false;

            if (name == null) {

                if (other.name != null)

                    return false;

            } else if (!name.equals(other.name))

                return false;

            if (press == null) {

                if (other.press != null)

                    return false;

            } else if (!press.equals(other.press))

                return false;

            if (Double.doubleToLongBits(price) != Double

                    .doubleToLongBits(other.price))

                return false;

            return true;

        }

        @Override

        public String toString() {

            return "Book [id=" + id + ", name=" + name + ", press=" + press

                    + ", price=" + price + "]";

        }

    }




