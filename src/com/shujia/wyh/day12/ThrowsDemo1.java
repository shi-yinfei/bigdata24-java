package com.shujia.wyh.day12;

/*
    异常处理的第二种方式：throws 抛给方法的调用者处理
    语句定义格式：throws是一个关键字，通常是作用在方法的定义上，表示该方法中出现了某种异常

 */
public class ThrowsDemo1 {
    public static void main(String[] args) {
//        try {
//            fun();
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        try {
            fun();
        }catch (Exception e){
            e.printStackTrace();
        }



        System.out.println("Hello World!");
    }

    public static void fun() throws Exception{
        int a = 10;
        int b = 3;
        System.out.println(a / b);

        int[] arr = {11,2,3,4};
        System.out.println(arr[4]);
    }

}
