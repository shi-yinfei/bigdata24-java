package com.shujia.wyh.day12;

/*
    java中的异常分类：
        所有异常共同的父类：Throwable
        两个子类：
            Error（错误）
            Exception（异常）
                RuntimeException（运行时期的异常）
                其他异常（编译时期异常）

    说一个小故事：李佳豪今天心情不错，天气也非常的好，要出去透透气，于是决定去爬大蜀山，于是先选择了一辆共享哈罗单车，这时候发现车没电。（编译时期异常：
    这个问题应该是出发前就要检查的问题），于是选择了另一辆有电的单车（问题解决），骑车的路上车爆胎了（运行时期异常：运行过程中出现的问题），就找到附近一家修车店修好了（问题解决）
    在大蜀山的山腰处，突然发生了泥石流（Error: 出现了问题，这个问题单靠个人解决不了，就不解决了）


 */
public class ExceptionDemo1 {
    public static void main(String[] args) {
//        int a = 10;
//        int b = 0;
//        System.out.println(a/b); //ArithmeticException 运行时期异常

        System.out.println("hello world!");
        System.out.println("hello world!");
        System.out.println("hello world!");
        System.out.println("hello world!");
        System.out.println("hello world!");

        int[] arr = {11,22,33};
        System.out.println(arr[3]);  //ArrayIndexOutOfBoundsException运行时期异常

        System.out.println("hello world!");
        System.out.println("hello world!");
        System.out.println("hello world!");
        System.out.println("hello world!");
        System.out.println("hello world!");
    }
}
