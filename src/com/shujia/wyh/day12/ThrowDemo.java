package com.shujia.wyh.day12;

/*
    当方法内部某一个部分确定会出现问题的时候，抛出异常的对象

    说一个小故事理解throw,throws和try..catch..
    故事纯属虚构，陆澳是一个快乐的小矿工，每一天都在挖矿，有一天他挖到了一个炸弹（throw 代表方法内部一定出现了问题），但是向外面传递（throws）
    传递给了矿工负责人（理解为方法的调用者），矿工负责人进行处理（try..catch）

 */
public class ThrowDemo {
    public static void main(String[] args) {
        try {
            fun();
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("Hello World");
    }

    public static void fun() throws Exception{
        int a = 10;
        int b = 5;
        if(b==0){
            throw new Exception();
        }else {
            System.out.println(a/b);
        }
    }
}
