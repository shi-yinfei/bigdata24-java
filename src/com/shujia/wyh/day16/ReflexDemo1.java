package com.shujia.wyh.day16;

/*
    反射：
        java中如何获取一个class文件对应的Class类型的对象
 */
public class ReflexDemo1 {
    public static void main(String[] args) throws Exception {
        //1、在类对象已经存在的情况下获取
//        Student student = new Student();
//        Class<? extends Student> studentClass = student.getClass();

        //2、在已经有类的情况下，但是没有对象来获取
        //每一个类都有一个属性class,获取对应类的Class类型对象
//        Class<Student> studentClass = Student.class;

        //3、Class类中有一个静态方法
        //static 类<?> forName(String className)
        //返回与给定字符串名称的类或接口相关联的 类对象。
        Class<?> studentClass = Class.forName("com.shujia.wyh.day16.Student");
        System.out.println(studentClass);
        //JDBC


    }
}
