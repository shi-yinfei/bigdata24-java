package com.shujia.wyh.day16.udpdemo1;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/*
    UDP协议接收端编程
    1:建立udp的socket服务.
    2:通过receive方法接收数据
    3:将收到的数据存储到数据包对象中
    4:通过数据包对象的功能来完成对接收到数据进行解析.
    5:可以对资源进行关闭

 */
public class ReceiveDemo1 {
    public static void main(String[] args) throws Exception {
        //1:建立udp的socket服务.
        //DatagramSocket(int port)
        //构造数据报套接字并将其绑定到本地主机上的指定端口。
        DatagramSocket ds = new DatagramSocket(10086);

        //创建用于接收数据的数据包
        byte[] bytes = new byte[1024];
        DatagramPacket dp = new DatagramPacket(bytes, bytes.length);

        //使用新的数据包接收发送过来的数据
        ds.receive(dp); //阻塞中正在等待数据过来接收，一旦接收到了数据就立刻往下运行

        //解析数据
        String ip = dp.getAddress().getHostAddress();
        String hostName = dp.getAddress().getHostName();
        //获取接收到的数据，但是是一个字节数组类型
        //dp.getLength()返回实际获取到的字节长度
        //dp.getData()返回整个字节数组
        String info = new String(dp.getData(),0,dp.getLength());

        //打印数据在控制台中
        System.out.println("IP地址为：" + ip + " ，用户是：" + hostName + " 发送来的数据是：" + info);

        //关闭连接
        ds.close();

    }
}
