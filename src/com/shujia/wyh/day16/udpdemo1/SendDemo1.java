package com.shujia.wyh.day16.udpdemo1;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/*
    UDP协议发送端编程：
    使用java提供给的一个类DatagramSocket中的无参构造方法来创建发送端的Socket对象
 */
public class SendDemo1 {
    public static void main(String[] args) throws Exception {
        //创建Socket对象
        DatagramSocket ds = new DatagramSocket();


        //将要发送的数据封装成数据包DatagramPacket
        //DatagramPacket(byte[] buf, int length, InetAddress address, int port)
        //构造用于发送长度的分组的数据报包 length指定主机上到指定的端口号。
        //这里的字节数组指的是要发送的数据转成字节数组类型 今天的天气十分炎热
        byte[] bytes = "今天的天气十分炎热".getBytes();
        //length代表的是字节数组的长度
        int length = bytes.length;
        //address代表的是目标接收端的ip地址
        InetAddress inetAddress = InetAddress.getByName("192.168.7.25");
        //port是接收端所占用的端口
        int port = 10086;

        //创建一个数据包
        DatagramPacket dp = new DatagramPacket(bytes, length, inetAddress, port);


        //Socket对象调用方法发送数据包
        ds.send(dp);

        //关闭ds连接
        ds.close();

    }
}
