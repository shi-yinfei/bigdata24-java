package com.shujia.wyh.day16.tcpdemo1;

import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/*
    tcp协议服务器端编程：
        1:建立服务器端的socket服务，需要一个端口
        2:服务端没有直接流的操作,而是通过accept方法获取客户端对象，在通过获取到的客户端对象的流和客户端进行通信
        3:通过客户端的获取流对象的方法,读取数据或者写入数据
        4:如果服务完成,需要关闭客户端,然后关闭服务器，但是,一般会关闭客户端,不会关闭服务器,因为服务端是一直提供服务的

 */
public class ServerDemo1 {
    public static void main(String[] args) throws Exception {
        //创建服务器端Socket对象
        //ServerSocket(int port)
        //创建绑定到指定端口的服务器套接字。
        ServerSocket ss = new ServerSocket(12345);

        //监听客户端的连接
        Socket socket = ss.accept();  //当程序运行到这一步的时候，发生阻塞，等待客户端的连接

        //获取通道中的字节输入流对象，接收客户端发送的数据
        InputStream inputStream = socket.getInputStream();

        //获取客户端的ip地址
        String ip = socket.getInetAddress().getHostAddress();
        //获取客户端的主机名
        String hostName = socket.getInetAddress().getHostName();

        byte[] bytes = new byte[1024];
        int length = 0;
        while ((length = inputStream.read(bytes)) != -1) {
            String info = new String(bytes, 0, length);
            System.out.println("IP地址为：" + ip + " ，用户是：" + hostName + " 发送来的数据是：" + info);
        }

        //关闭与客户端的通道连接
        socket.close();
        //关闭整个服务器
        ss.close();

    }
}
