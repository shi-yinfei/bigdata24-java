package com.shujia.wyh.day16.tcpdemo4;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
    tcp协议服务器端编程：
        1:建立服务器端的socket服务，需要一个端口
        2:服务端没有直接流的操作,而是通过accept方法获取客户端对象，在通过获取到的客户端对象的流和客户端进行通信
        3:通过客户端的获取流对象的方法,读取数据或者写入数据
        4:如果服务完成,需要关闭客户端,然后关闭服务器，但是,一般会关闭客户端,不会关闭服务器,因为服务端是一直提供服务的
    tcp协议服务器端没开，客户端无法连接，且服务器端只能启动1次。
 */
public class ServerDemo1 {
    public static void main(String[] args) throws Exception {
        //创建服务器端Socket对象
        //ServerSocket(int port)
        //创建绑定到指定端口的服务器套接字。
        ServerSocket ss = new ServerSocket(12345);

        while (true){
            //监听客户端的连接
            Socket socket = ss.accept();  //当程序运行到这一步的时候，发生阻塞，等待客户端的连接
            String hostName = socket.getInetAddress().getHostName();
            System.out.println("================="+hostName+"用户已连接，处于在线状态=====================");
            new ClientThread(socket).start();
        }
    }
}

class ClientThread extends Thread{
    private Socket socket;

    public ClientThread() {
    }

    public ClientThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {

        InputStream inputStream = null;
        String ip = null;
        String hostName = null;
        try {
            //获取通道中的字节输入流对象，接收客户端发送的数据
            inputStream = socket.getInputStream();
            //获取客户端的主机名
            hostName = socket.getInetAddress().getHostName();

            long l = System.currentTimeMillis();
            Date date = new Date(l);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String time = sdf.format(date);

            while (true) {
                byte[] bytes = new byte[1024];
                int length = 0;
                while ((length = inputStream.read(bytes)) != -1) {
                    String info = new String(bytes, 0, length);
                    if("886".equals(info)){
                        System.out.println("用户："+hostName+" 已经下线......");
                        break;
                    }
                    System.out.println(time);
                    System.out.println("用户" + hostName + " ：" + info);
                    break;
                }

            }
        }catch (Exception e){
            System.out.println("用户："+hostName+" 已经下线......");
        }

    }
}
