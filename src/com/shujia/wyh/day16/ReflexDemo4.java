package com.shujia.wyh.day16;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/*
    通过反射获取成员方法并使用

 */
public class ReflexDemo4 {
    public static void main(String[] args) throws Exception {
        Class<?> aClass = Class.forName("com.shujia.wyh.day16.Student");

        //获取类中以及父类中的所有被public修饰的成员方法
//        Method[] methods = aClass.getMethods();
//        for (Method method : methods) {
//            System.out.println(method);
//        }

        //获取本类中的所有成员方法，包括私有
//        Method[] declaredMethods = aClass.getDeclaredMethods();
//        for (Method declaredMethod : declaredMethods) {
//            System.out.println(declaredMethod);
//        }

        //使用反射获取构造方法创建对象
        Constructor<?> declaredConstructor = aClass.getDeclaredConstructor();
        Object o = declaredConstructor.newInstance();

        //获取单个公共的成员方法
//        Method fun1 = aClass.getMethod("fun1");
        //Object invoke(Object obj, Object... args)
        //在具有指定参数的 方法对象上调用此 方法对象表示的底层方法。
//        fun1.invoke(o);

        //获取单个公共的带参数的成员方法
//        Method fun1 = aClass.getDeclaredMethod("fun1", String.class);
//        fun1.invoke(o,"今天天气不错");

        //获取单个私有的无参方法
//        Method fun2 = aClass.getDeclaredMethod("fun2");
//        //暴力访问
//        fun2.setAccessible(true);
//        fun2.invoke(o);

        //获取单个私有带参数的方法
        Method fun2 = aClass.getDeclaredMethod("fun2", String.class);
        //暴力访问
        fun2.setAccessible(true);
        Object o2 = fun2.invoke(o,"数加科技666");
        System.out.println(o2);


    }
}
