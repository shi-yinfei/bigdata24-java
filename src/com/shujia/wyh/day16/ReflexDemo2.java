package com.shujia.wyh.day16;

import java.lang.reflect.Constructor;

/*
    获取构造方法并使用

 */
public class ReflexDemo2 {
    public static void main(String[] args) throws Exception {
        //		getConstructors
        //		getDeclaredConstructors

        Class<?> studentClass = Class.forName("com.shujia.wyh.day16.Student");

        //获取所有的被public修饰的构造方法
//        Constructor<?>[] constructors = studentClass.getConstructors();
//        for (Constructor<?> constructor : constructors) {
//            System.out.println(constructor);
//        }

        //获取所有的构造方法包括私有的
//        Constructor<?>[] declaredConstructors = studentClass.getDeclaredConstructors();
//        for (Constructor<?> declaredConstructor : declaredConstructors) {
//            System.out.println(declaredConstructor);
//        }

        //获取某一个被public修饰的构造方法
//        Constructor<?> constructor = studentClass.getConstructor();
//        System.out.println(constructor);
//        Constructor<?> constructor = studentClass.getConstructor(int.class);
//        System.out.println(constructor);

        //获取某一个私有的构造方法
        Constructor<?> declaredConstructor = studentClass.getDeclaredConstructor(String.class);
        System.out.println(declaredConstructor);


        //通过获取到的公共的构造方法创建对象
        //T newInstance(Object... initargs)
        //使用此 Constructor对象表示的构造函数，使用指定的初始化参数来创建和初始化构造函数的声明类的新实例。
//        Object o = constructor.newInstance();
//        System.out.println(o);
        //绕过检测机制，暴力访问
        declaredConstructor.setAccessible(true);

        Object o = declaredConstructor.newInstance("王宇杰");
        System.out.println(o);


    }
}
