package com.shujia.wyh.day16;

/*
    成员变量
    构造方法
    成员你方法
 */
public class Student {
    private String name;
    int age;
    protected String address;
    public String like;
    public Double score;

    public Student() {
    }

    public Student(int age) {
        this.age = age;
    }

    Student(Double score) {
        this.score = score;
    }

    private Student(String name) {
        this.name = name;
    }

    public void fun1() {
        System.out.println("这是公共的方法");
    }

    public void fun1(String s) {
        System.out.println("这是公共的方法" + s);
    }

    private void fun2() {
        System.out.println("这是私有的方法");
    }

    private String fun2(String s) {
        System.out.println("这是私有的方法" + s);
        return "李佳豪在喝水";
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                ", like='" + like + '\'' +
                ", score=" + score +
                '}';
    }
}
