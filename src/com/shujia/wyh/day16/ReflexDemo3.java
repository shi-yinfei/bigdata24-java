package com.shujia.wyh.day16;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

/*
    反射获取成员变量并使用
    getFields
 */
public class ReflexDemo3 {
    public static void main(String[] args) throws Exception {
        //获取Student类的Class对象
        Class<?> studentClass = Class.forName("com.shujia.wyh.day16.Student");

        //获取所有的被public修饰的成员变量
//        Field[] fields = studentClass.getFields();
//        for (Field field : fields) {
//            System.out.println(field);
//        }
        //获取所有的成员变量，包括私有的
//        Field[] declaredFields = studentClass.getDeclaredFields();
//        for (Field declaredField : declaredFields) {
//            System.out.println(declaredField);
//        }
        //获取某一个成员变量,根据获取对应的成员变量
        Field like = studentClass.getField("like");
        Field name = studentClass.getDeclaredField("name");
//        System.out.println(like);
//        System.out.println(name);

        //如何给成员变量赋值
        //void set(Object obj, Object value)
        //将指定对象参数上的此 Field对象表示的字段设置为指定的新值。
        //使用反射获取构造方法创建对象
        Constructor<?> declaredConstructor = studentClass.getDeclaredConstructor();
        Object o = declaredConstructor.newInstance();

        like.set(o,"打篮球");  //给对象o中的成员变量like赋值打篮球
        //暴力访问
        name.setAccessible(true);
        name.set(o,"王宇杰");

        System.out.println(o);



    }
}
