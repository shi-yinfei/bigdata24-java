package com.shujia.wyh.redis_jdbc;

import com.shujia.wyh.redis_jdbc.utils.MySQLTool;
import redis.clients.jedis.Jedis;

import java.sql.*;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

//redis充当缓存，存储热点数据案例
public class RedisApiDemo2 {
    static Connection conn = null;
    static Statement statement = null;
    static Scanner sc = new Scanner(System.in);
    static Jedis jedis = null;

    public static void main(String[] args) {
        try {
            //获取与redis的连接对象
            jedis = new Jedis("192.168.157.111", 7000);

            //使用工具类获取连接对象
            Connection conn = MySQLTool.getConnection();
            statement = conn.createStatement();

            //获取能够防止sql注入问题的数据库操作对象
            PreparedStatement preparedStatement = conn.prepareStatement("select username,password from login where username=? and password=?");

            System.out.println("==================欢迎来到xxx商城网=====================");
            System.out.print("欢迎登录，请输入用户名：");
            String name1 = sc.nextLine();
            System.out.print("请输入密码：");
            String pwd1 = sc.nextLine();
            //设置sql的值
            preparedStatement.setString(1, name1);
            preparedStatement.setString(2, pwd1);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                System.out.println("对不起，该用户未注册，请先注册");
                System.out.println("是否要进行注册？（Y/N）:");
                String flag = sc.nextLine();
                if ("Y".equals(flag)) {
                    register();
                } else if ("N".equals(flag)) {
                    System.out.println("欢迎下次使用!!");
                }
            } else {
                System.out.println("登录成功！！开始挑选商品！>_^");
                System.out.println("请输入您想要查询商品的名称：");
                String goodsName = sc.next();
                //先去redis中查询
                List<String> list = jedis.lrange(goodsName, 0, -1);
                if (list.size() == 0) {
                    System.out.println("=================== 这是直接走MySQL的查询 =========================");
                    //redis中没有该类商品
                    //再去mysql中查询
                    PreparedStatement prepstate1 = conn.prepareStatement("select * from jd_goods where name like ?");
                    prepstate1.setString(1, "%" + goodsName + "%");
                    //执行sql语句
                    ResultSet resultSet1 = prepstate1.executeQuery();
                    while (resultSet1.next()){
                        StringBuilder sb = new StringBuilder();
                        //id
                        String id = resultSet1.getString(1);
                        sb.append(id);
                        sb.append(" | ");
                        //name
                        String name = resultSet1.getString(2);
                        sb.append(name);
                        sb.append(" | ");
                        //prices
                        String prices = resultSet1.getString(3);
                        sb.append(prices);
                        sb.append(" | ");
                        //shop
                        String shop = resultSet1.getString(4);
                        sb.append(shop);
                        sb.append(" | ");
                        //flags
                        String flags = resultSet1.getString(5);
                        sb.append(flags);
                        sb.append(" | ");
                        //area
                        String area = resultSet1.getString(6);
                        sb.append(area);
                        sb.append(" | ");
                        //cpu
                        String cpu = resultSet1.getString(7);
                        sb.append(cpu);
                        sb.append(" | ");
                        //size
                        String size = resultSet1.getString(8);
                        sb.append(size);
                        sb.append(" | ");
                        //memorySize
                        String memorySize = resultSet1.getString(9);
                        sb.append(memorySize);
                        sb.append(" | ");
                        //gpu
                        String gpu = resultSet1.getString(10);
                        sb.append(gpu);
                        sb.append(" | ");
                        //comments
                        String comments = resultSet1.getString(11);
                        sb.append(comments);

                        String s = sb.toString();
                        System.out.println(s);
                        //mysql中查到数据后，向redis中写一份
                        jedis.rpush(goodsName,s);
                        //设置过期时间
                        jedis.expire(goodsName,120);
                    }
                }else {
                    System.out.println("================= 这是直接走Redis查询数据 ==================");
                    //redis中包含该类商品
                    for (String s : list) {
                        System.out.println(s);
                    }

                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //释放资源
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (jedis != null) {
                jedis.close();
            }
        }
    }

    //注册功能
    public static void register() {
        //欢迎注册李佳豪婚恋介绍所
        System.out.println("============欢迎注册xxx商城网============");
        System.out.print("请输入新用户的用户名：");
        String name = sc.nextLine();
        System.out.print("请输入新用户的密码：");
        String pwd = sc.nextLine();
        System.out.println("注册中，请稍等.....^_^");

        //java中提供了一个类UUID
        String id = UUID.randomUUID().toString();

        String sql = "insert into login value('" + id + "','" + name + "','" + pwd + "')";
        try {
            int i = statement.executeUpdate(sql);
            Thread.sleep(3000);
            if (i == 1) {
                System.out.println("新用户注册成功，正在跳转首页。。。");
            } else {
                System.out.println("新用户注册失败，请1小时后再进行操作..");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
