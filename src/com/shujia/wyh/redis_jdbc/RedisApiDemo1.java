package com.shujia.wyh.redis_jdbc;


import redis.clients.jedis.Jedis;
import redis.clients.jedis.resps.Tuple;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RedisApiDemo1 {
    public static void main(String[] args) {
        //创建与Redis数据库的连接对象
        //public Jedis(String host, int port)
        Jedis jedis = new Jedis("192.168.157.111", 7000);
        System.out.println(jedis);

        //0-15
        jedis.select(3);

        //操作redis数据库，本质上是存储各种各样的数据类型的数据
        //string,list,set,sorted set,hash
        //string
//        String s = jedis.set("name", "陆澳");
//        System.out.println(s);
//        String s1 = jedis.get("name");
//        System.out.println(s1);
//        //对key设置过期时间
////        jedis.set("yzm","qYj5");
////        jedis.expire("yzm",60);
//        String s2 = jedis.get("yam"); //如果获取不到这个键，就返回null
//        System.out.println(s2);


        //list
//        jedis.lpush("like","打篮球","唱歌","跳舞");
//        List<String> list = jedis.lrange("like", 0, -1);
//        for (String s3 : list) {
//            System.out.println(s3);
//        }

        //set
//        jedis.sadd("ids","1001","1002","1003","1001");
//        Set<String> ids = jedis.smembers("ids");
//        for (String id : ids) {
//            System.out.println(id);
//        }

        //zSet
//        jedis.zadd("zset2",98,"李佳豪");
//        HashMap<String, Double> map = new HashMap<>();
//        map.put("小虎",60.0);
//        map.put("陆澳",90.0);
//
//        jedis.zadd("zset2",map);

//        List<String> zset2 = jedis.zrange("zset2", 0, -1);
//        for (String s : zset2) {
//            System.out.println(s);
//        }
//        List<String> zset2 = jedis.zrevrange("zset2", 0, -1);
//        for (String s : zset2) {
//            System.out.println(s);
//        }
//        List<String> zset2 = jedis.zrangeByScore("zset2", 0, 100);
//        for (String s : zset2) {
//            System.out.println(s);
//        }
//        List<Tuple> zset2 = jedis.zrandmemberWithScores("zset2", 10);
//        for (Tuple tuple : zset2) {
//            String student = tuple.getElement();
//            double score = tuple.getScore();
//            System.out.println(student+"---"+score);
//        }


        //hash
//        HashMap<String, String> map = new HashMap<>();
//        map.put("name","小虎");
//        map.put("age","18");
//        map.put("content","数加科技");
//        map.put("address","安徽合肥");
//
//        jedis.hset("map1",map);

//        Map<String, String> map1 = jedis.hgetAll("map1");
//        Set<Map.Entry<String, String>> entrySet = map1.entrySet();
//        for (Map.Entry<String, String> stringStringEntry : entrySet) {
//            String key = stringStringEntry.getKey();
//            String value = stringStringEntry.getValue();
//            System.out.println(key + "---" + value);
//        }

        List<String> list = jedis.lrange("qwe", 0, -1);
        System.out.println(list);


        //释放资源
        jedis.close();
    }
}
