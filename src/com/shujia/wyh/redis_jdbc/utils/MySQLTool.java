package com.shujia.wyh.redis_jdbc.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/*
    通过这个工具类获取与数据库连接对象
 */
public class MySQLTool {
    //构造方法私有化
    private MySQLTool(){}

    public static Connection getConnection(){
        Connection conn = null;
        try {
            //创建一个配置文件对象
            Properties prop = new Properties();
            //加载配置文件
            prop.load(new BufferedReader(new FileReader("E:\\数加\\java\\IdeaProjects\\bigdata24-java\\src\\com\\shujia\\wyh\\redis_jdbc\\utils\\mysqlinfo.properties")));
            //根据key获取对应的value值
            String url = prop.getProperty("url");
            String user = prop.getProperty("user");
            String password = prop.getProperty("password");

            //加载驱动
            Class.forName("com.mysql.jdbc.Driver");

            //获取连接对象
            conn = DriverManager.getConnection(url, user, password);
        }catch (Exception e){
            e.printStackTrace();
        }

        return conn;
    }
}
