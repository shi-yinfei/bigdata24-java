/*
 Navicat Premium Data Transfer

 Source Server         : master
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : 192.168.169.100:3306
 Source Schema         : bigdata24

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 24/05/2023 16:55:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`  (
  `DEPTNO` int(11) NOT NULL,
  `DNAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LOC` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`DEPTNO`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES (10, 'ACCOUNTING', 'NEW YORK');
INSERT INTO `dept` VALUES (20, 'RESEARCH', 'DALLAS');
INSERT INTO `dept` VALUES (30, 'SALES', 'CHICAGO');
INSERT INTO `dept` VALUES (40, 'OPERATIONS', 'BOSTON');

-- ----------------------------
-- Table structure for emp
-- ----------------------------
DROP TABLE IF EXISTS `emp`;
CREATE TABLE `emp`  (
  `EMPNO` int(11) NOT NULL,
  `ENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `JOB` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `MGR` int(11) NULL DEFAULT NULL,
  `HIREDATE` date NULL DEFAULT NULL,
  `SAL` decimal(10, 0) NULL DEFAULT NULL,
  `COMM` decimal(10, 0) NULL DEFAULT NULL,
  `DEPTNO` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`EMPNO`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emp
-- ----------------------------
INSERT INTO `emp` VALUES (7369, 'SMITH', 'CLERK', 7902, '1980-12-17', 800, NULL, 20);
INSERT INTO `emp` VALUES (7499, 'ALLEN', 'SALESMAN', 7698, '1981-02-20', 1600, 300, 30);
INSERT INTO `emp` VALUES (7521, 'WARD', 'SALESMAN', 7698, '1981-02-22', 1250, 500, 30);
INSERT INTO `emp` VALUES (7566, 'JONES', 'MANAGER', 7839, '1981-04-02', 2975, NULL, 20);
INSERT INTO `emp` VALUES (7654, 'MARTIN', 'SALESMAN', 7698, '1981-09-28', 1250, 1400, 30);
INSERT INTO `emp` VALUES (7698, 'BLAKE', 'MANAGER', 7839, '1981-05-01', 2850, NULL, 30);
INSERT INTO `emp` VALUES (7782, 'CLARK', 'MANAGER', 7839, '1981-06-09', 2450, NULL, 10);
INSERT INTO `emp` VALUES (7788, 'SCOTT', 'ANALYST', 7566, '1987-07-13', 3000, NULL, 20);
INSERT INTO `emp` VALUES (7839, 'KING', 'PRESIDENT', NULL, '1981-11-17', 5000, NULL, 10);
INSERT INTO `emp` VALUES (7844, 'TURNER', 'SALESMAN', 7698, '1981-09-08', 1500, 0, 30);
INSERT INTO `emp` VALUES (7876, 'ADAMS', 'CLERK', 7788, '1987-07-13', 1100, NULL, 20);
INSERT INTO `emp` VALUES (7900, 'JAMES', 'CLERK', 7698, '1981-12-03', 950, NULL, 30);
INSERT INTO `emp` VALUES (7902, 'FORD', 'ANALYST', 7566, '1981-12-03', 3000, NULL, 20);
INSERT INTO `emp` VALUES (7934, 'MILLER', 'CLERK', 7782, '1982-01-23', 1300, NULL, 10);

-- ----------------------------
-- Table structure for jd_goods
-- ----------------------------
DROP TABLE IF EXISTS `jd_goods`;
CREATE TABLE `jd_goods`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `prices` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shop` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `flags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `area` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cpu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `memorySize` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gpu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `comments` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jd_goods
-- ----------------------------
INSERT INTO `jd_goods` VALUES ('100052591904', '华为笔记本电脑MateBook 16s 2023 13代酷睿版 标压i7 16G 1T 16英寸轻薄本/2.5K触控全面屏/手机互联 深空灰', '￥7999.00', '华为京东自营官方旗舰店', '自营', '中国大陆', 'intel i7', '16.0-16.9英寸', '16GB', '集成显卡', '5万+条评价');
INSERT INTO `jd_goods` VALUES ('100032149194', '京品电脑\n华为笔记本电脑MateBook D 14 SE版 14英寸 11代酷睿 i5 8G+512G 轻薄本/高清护眼防眩光屏/手机互联 银', '￥3999.00', '华为京东自营官方旗舰店', '自营', '中国大陆', 'intel i5', '14.0-14.9英寸', '8GB', '集成显卡', '50万+条评价');
INSERT INTO `jd_goods` VALUES ('100033060796', '联想(Lenovo)台式机电脑主机 扬天M4000q 英特尔酷睿i3(i3-12100 8G 512G Type-C Win11)21.45英寸整机', '￥2999.00', '联想商用京东自营旗舰店', '自营', '中国大陆', 'intel i3', '20-21.5英寸', '8GB', '集成显卡', '2万+条评价');
INSERT INTO `jd_goods` VALUES ('100021044009', '酷开 创维电脑 八核主机办公商用台式机电脑整机（AMD八核A9 8G 256G M.2 WiFi 全国联保）23.8英寸全套', '￥1399.00', '创维酷开电脑京东自营官方旗舰店', '自营', '中国大陆', 'AMD', '23-25英寸', '8GB', '集成显卡', '5万+条评价');
INSERT INTO `jd_goods` VALUES ('100011526087', '京品电脑\n小米笔记本电脑 红米 RedmiBookPro 14英寸 2.5K高清屏 高性能轻薄本(R5 16G 512G 商务办公 长续航 全金属)', '￥3199.00', '小米京东自营旗舰店', '自营', '中国大陆', 'AMD R5', '14.0-14.9英寸', '16GB', 'AMD 集成显卡', '20万+条评价');
INSERT INTO `jd_goods` VALUES ('13836773522', '酷耶（Cooyes） 八核游戏16G台式机电脑主机整机全套组装家用电竞企业办公 套餐三(十核+GTX1660S)+显示器', '￥2098.00', '酷耶电脑旗舰店', '京东物流\n免邮', '中国大陆', 'intel', '23-25英寸', '16GB', 'GTX1660S', '10万+条评价');
INSERT INTO `jd_goods` VALUES ('100052135160', '华为笔记本电脑MateBook 14s 2023 英特尔Evo 13代酷睿标压 i5 16G 512G 14.2英寸120Hz触控/轻薄本/手机互联 灰', '￥6599.00', '华为京东自营官方旗舰店', '自营', '中国大陆', 'intel i5', '14.0-14.9英寸', '16GB', '集成显卡', '5万+条评价');
INSERT INTO `jd_goods` VALUES ('100024424036', '京品电脑\nAOC AIO大师734 23.8英寸高清办公一体机电脑台式主机(N5095 8G 256GSSD 双频WiFi 3年上门)', '￥1699.00', 'AOC电脑京东自营旗舰店', '自营', '中国大陆', 'intel 奔腾 赛扬', '22-24.5英寸', '8GB', '集成显卡', '5万+条评价');
INSERT INTO `jd_goods` VALUES ('100047694264', '惠普（HP）暗影精灵9 Intel 16.1英寸游戏本 笔记本电脑(13代i9-13900HX RTX4060 16G 1TBSSD 2.5K 240Hz)', '￥8999.00', '惠普（HP）OMEN暗影精灵京东自营旗舰店', '自营\n新品', '中国大陆', 'intel i9', '16.0-16.9英寸', '16GB', 'RTX 4060', '10万+条评价');
INSERT INTO `jd_goods` VALUES ('100041351132', '联想(Lenovo)天逸510S英特尔酷睿i5商务台式机电脑整机(13代i5-13400 16G 1TB HDD+512G SSD win11)23英寸', '￥4399.00', '联想京东自营旗舰店', '自营\n秒杀', '中国大陆', 'intel i5', '23-25英寸', '16GB', '集成显卡', '10万+条评价');
INSERT INTO `jd_goods` VALUES ('10055406984027', '爱心东东\n启稚 英特尔十二核RTX3060独显大容量1T固态办公游戏家用组装吃鸡台式机主机水冷整机全套 套餐五：十二核+32G/1T/3060 光追特效 主机+27英寸显示器', '￥3548.00', '启稚电脑整机旗舰店', '京东物流', '', 'intel E5', '27英寸及以上', '32GB', 'RTX 3060', '2万+条评价');
INSERT INTO `jd_goods` VALUES ('1085794603', '硕扬 i5 12400F升12490F/RTX2060/电竞游戏直播吃鸡台式组装电脑主机整机DIY', '￥3045.00', '硕扬DIY电脑旗舰店', '免邮\n券3000-300\n每满3015-49', '中国大陆', 'intel i5', '', '', 'RTX 2060', '5万+条评价');
INSERT INTO `jd_goods` VALUES ('100026507017', '惠普HP 小欧S01电脑主机 商务办公台式机（i3-12100 8G 512GSSD WiFi Win11 注册五年上门）+21.45英寸显示器', '￥2899.00', '惠普京东自营官方旗舰店', '自营', '中国大陆', 'intel i3', '20-21.5英寸', '8GB', '集成显卡', '1万+条评价');
INSERT INTO `jd_goods` VALUES ('100011483893', '京品电脑\n联想笔记本电脑 小新Air14 英特尔酷睿i5 14英寸轻薄本(i5 16G 512G 高色域 大电池)银 商务办公手提', '￥3499.00', '联想京东自营旗舰店', '自营', '中国大陆', 'intel i5', '14.0-14.9英寸', '16GB', '集成显卡', '去看二手\n20万+条评价');
INSERT INTO `jd_goods` VALUES ('100041545018', '戴尔(Dell)成就3020台式机电脑主机 商用办公电脑整机 (13代i5-13400 16G 256GSSD+1TB)23.8英寸', '￥4299.00', '戴尔京东自营官方旗舰店', '自营', '中国大陆', 'intel i5', '23-25英寸', '16GB', 'UHD730', '10万+条评价');
INSERT INTO `jd_goods` VALUES ('100053553141', '然硕 AMD 锐龙R5 5600G电脑主机台式机办公游戏家用企业全新组装整机全套（AMD 5600G 16G 500G M.2 nvme）', '￥1655.00', '然硕电脑京东自营旗舰店', '自营', '中国大陆', '', '', '', '', '1000+条评价');
INSERT INTO `jd_goods` VALUES ('100041351108', '联想(Lenovo)天逸510S 个人商务台式机电脑整机(13代i5-13400  16G 1TB SSD wifi win11 )23英寸', '￥4299.00', '联想京东自营旗舰店', '自营', '中国大陆', 'intel i5', '23-25英寸', '16GB', '集成显卡', '10万+条评价');
INSERT INTO `jd_goods` VALUES ('100039568404', '联想(Lenovo)天逸510S英特尔酷睿i5个人商务台式机电脑整机(12代i5-12400 16G 512G SSD win11)23英寸', '￥3999.00', '联想京东自营旗舰店', '自营', '中国大陆', 'intel i5', '23-25英寸', '16GB', '集成显卡', '10万+条评价');
INSERT INTO `jd_goods` VALUES ('100058570587', '华为笔记本电脑MateBook 14 2023 13代酷睿版 i5 16G 512G 14英寸轻薄办公本/2K触控全面屏/手机互联 深空灰', '￥5499.00', '华为京东自营官方旗舰店', '自营', '中国大陆', 'intel i5', '14.0-14.9英寸', '16GB', '集成显卡', '20万+条评价');
INSERT INTO `jd_goods` VALUES ('100018005951', '京品电脑\n华为笔记本电脑MateBook D 14 SE版 14英寸 11代酷睿 i5 8G+512G 轻薄本/高清护眼防眩光屏/手机互联 灰', '￥3999.00', '华为京东自营官方旗舰店', '自营', '中国大陆', 'intel i5', '14.0-14.9英寸', '8GB', '集成显卡', '50万+条评价');
INSERT INTO `jd_goods` VALUES ('100051520281', '戴尔(Dell)成就3020台式机电脑主机 商用办公电脑整机 (13代i3-13100 16G 256GSSD+1TB)23.8英寸', '￥3699.00', '戴尔京东自营官方旗舰店', '自营', '中国大陆', 'intel i3', '23-25英寸', '16GB', 'UHD730', '10万+条评价');
INSERT INTO `jd_goods` VALUES ('100016960357', '华为笔记本电脑MateBook D 14 2022款 14英寸 11代酷睿 i5 16G+512G 轻薄本/护眼全面屏/手机互联 银', '￥4799.00', '华为京东自营官方旗舰店', '自营', '中国大陆', 'intel i5', '14.0-14.9英寸', '16GB', '集成显卡', '去看二手\n50万+条评价');
INSERT INTO `jd_goods` VALUES ('100042655890', '机械革命（MECHREVO）极光Pro 15.6英寸游戏本 笔记本电脑(i7-12650H 16G 512G RTX4060 165HZ 2.5K屏)', '￥6488.00', '机械革命京东自营官方旗舰店', '自营\n新品', '中国大陆', 'intel i7', '15.0-15.9英寸', '16GB', 'RTX 4060', '10万+条评价');
INSERT INTO `jd_goods` VALUES ('10066784094740', '品纯（PINCHUN）英特尔十二核32G内存/台式机电脑主机整机全套组装家用游戏办公 主机+32英寸显示器全套 套六：十二核+32G内存丨RTX3060光追显卡', '￥3747.00', '品纯旗舰店', '京东物流\n赠', '中国大陆', 'intel', '27英寸及以上', '32GB', 'RTX 3060', '2万+条评价');
INSERT INTO `jd_goods` VALUES ('100056764051', '华为笔记本电脑MateBook D16 2023 13代酷睿版 i5 16G 1T/轻薄商务办公本/16英寸护眼全面屏/手机互联 皓月银', '￥5699.00', '华为京东自营官方旗舰店', '自营', '中国大陆', 'intel i5', '16.0-16.9英寸', '16GB', '集成显卡', '2万+条评价');
INSERT INTO `jd_goods` VALUES ('100055147461', '联想（Lenovo）拯救者Y9000P 2023游戏笔记本电脑 16英寸专业电竞本(13代i9-13900HX 16G 1T RTX4060显卡 2.5k 240Hz屏)灰', '￥10999.00', '联想京东自营旗舰店', '自营\n新品', '中国大陆', 'intel i9', '16.0-16.9英寸', '16GB', 'RTX 4060', '5万+条评价');
INSERT INTO `jd_goods` VALUES ('100052856635', 'ThinkPad联想ThinkBook 14+ 2023款 13代酷睿i5英特尔Evo平台 14英寸标压轻薄笔记本i5-13500H 16G 512G 2.8K 90Hz', '￥5299.00', 'ThinkPad京东自营旗舰店', '自营\n新品', '中国大陆', 'intel i5', '14.0-14.9英寸', '16GB', '集成显卡', '10万+条评价');
INSERT INTO `jd_goods` VALUES ('100055147457', '联想（Lenovo）拯救者Y7000P 2023游戏笔记本电脑 16英寸超能电竞本((13代i7-13700H 16G 1T RTX4060显卡 2.5K高刷屏)灰', '￥8499.00', '联想京东自营旗舰店', '自营\n新品', '中国大陆', 'intel i7', '16.0-16.9英寸', '16GB', 'RTX 4060', '5万+条评价');
INSERT INTO `jd_goods` VALUES ('100052883927', '联想笔记本电脑小新Pro14轻薄本 英特尔酷睿i5 14英寸超能本(13代标压i5-13500H 16G 1T 2.8K高刷屏)灰 办公', '￥5799.00', '联想京东自营旗舰店', '自营', '中国大陆', 'intel i5', '14.0-14.9英寸', '16GB', '集成显卡', '20万+条评价');
INSERT INTO `jd_goods` VALUES ('100016751652', 'AppleMacBookAir【教育优惠】13.3 8核M1芯片(7核图形处理器) 8G 256G SSD 银色 笔记本 MGN93CH/A', '￥7199.00', 'Apple产品京东自营旗舰店', '自营\n满4000-1400', '中国大陆', 'Apple M1', '13.0-13.9英寸', '8GB', 'APPLE M1 集成显卡', '20万+条评价');
INSERT INTO `jd_goods` VALUES ('100041766592', '联想商用台式机ThinkCentre K70英特尔酷睿i5（i5-12500/8G/512G SSD/无光驱/集显/260W/OKR/Win11/21.45液晶', '￥3699.00', '联想智慧办公京东自营官方旗舰店', '自营', '中国大陆', 'intel i5', '20-21.5英寸', '8GB', '集成显卡', '2000+条评价');
INSERT INTO `jd_goods` VALUES ('10055406984028', '爱心东东\n启稚 英特尔十二核RTX3060独显大容量1T固态办公游戏家用组装吃鸡台式机主机水冷整机全套 套餐五：十二核+32G/1T/3060 光追特效 主机+32英寸显示器', '￥3748.00', '启稚电脑整机旗舰店', '京东物流', '', 'intel E5', '27英寸及以上', '32GB', 'RTX 3060', '2万+条评价');
INSERT INTO `jd_goods` VALUES ('100035852182', '京品电脑\n惠普(HP)战66 15.6英寸(英特尔酷睿 i5 16G 512GB 长续航 高色域低功耗屏)高性能轻薄本笔记本', '￥3899.00', '惠普京东自营官方旗舰店', '自营\n满99-2', '中国大陆', 'intel i5', '15.0-15.9英寸', '16GB', 'INTEL IRIS XE', '20万+条评价');
INSERT INTO `jd_goods` VALUES ('100053492945', '华硕灵耀14 2023 13代英特尔酷睿i7 Evo 2.8K OLED屏高颜值超轻薄商务办公笔记本(i7-1360P 16G 512G)银', '￥5998.00', '华硕京东自营官方旗舰店', '自营\n新品\n赠', '中国大陆', 'intel i7', '14.0-14.9英寸', '16GB', '集成显卡', '2000+条评价');
INSERT INTO `jd_goods` VALUES ('100056919919', '联想（Lenovo）拯救者R9000P 2023游戏笔记本电脑 16英寸专业电竞本(新R7-7745HX 16G 1T RTX4060显卡 2.5k 240Hz高色域)灰', '￥8999.00', '联想京东自营旗舰店', '自营\n新品', '中国大陆', 'AMD R7', '16.0-16.9英寸', '16GB', 'RTX 4060', '10万+条评价');
INSERT INTO `jd_goods` VALUES ('100042659840', '机械革命（MECHREVO）蛟龙16K 16英寸游戏电竞笔记本电脑（R7-7735H 16G 512G RTX4050 165HZ 2.5K）', '￥5499.00', '机械革命京东自营官方旗舰店', '自营\n新品', '中国大陆', 'AMD R7', '16.0-16.9英寸', '16GB', 'RTX 4050', '2万+条评价');
INSERT INTO `jd_goods` VALUES ('10062930451658', '联想（Lenovo） 台式机 2022款12代英特尔i5/i7设计师6G独显办公会议台式电脑主机 i5-12400F GTX1650s 4G独显 定制：16G 1T+512G SSD', '￥5999.00', '联想电脑办公京东自营旗舰店', '自营', '', 'intel i5', '无显示器', '16GB', 'GTX1650S', '100+条评价');
INSERT INTO `jd_goods` VALUES ('100044025811', 'Apple iPad Pro 11英寸平板电脑 2022年款(128G WLAN版/M2芯片Liquid视网膜屏/MNXD3CH/A) 深空灰色', '￥6799.00', 'Apple产品京东自营旗舰店', '自营\n券2000-150', '中国大陆', 'Apple M2芯片', '11英寸', '128GB', '', '去看二手\n10万+条评价');
INSERT INTO `jd_goods` VALUES ('100057677955', '联想笔记本电脑小新14轻薄本 英特尔酷睿i5 14英寸超薄本(高性能标压i5 16G 512G)灰 商务办公学生', '￥5299.00', '联想京东自营旗舰店', '自营\n新品', '中国大陆', 'intel i5', '14.0-14.9英寸', '16GB', '集成显卡', '20万+条评价');
INSERT INTO `jd_goods` VALUES ('100055335683', '惠普(HP)战66 六代2023酷睿15.6英寸(英特尔13代i5-1340P 16G 1T 2.5K高色域屏120HZ)高性能轻薄本笔记本', '￥4899.00', '惠普京东自营官方旗舰店', '自营\n新品', '中国大陆', 'intel i5', '15.0-15.9英寸', '16GB', 'INTEL IRIS XE', '20万+条评价');
INSERT INTO `jd_goods` VALUES ('100039924870', '京品电脑\n联想笔记本电脑小新Pro14轻薄本 英特尔Evo 14英寸超能本(12核标压i5 16G 512G 2.8K高刷屏)银 商务办公学生', '￥4798.00', '联想京东自营旗舰店', '自营', '中国大陆', 'intel i5', '14.0-14.9英寸', '16GB', '集成显卡', '20万+条评价');
INSERT INTO `jd_goods` VALUES ('100009554947', 'AppleMacBookAir【教育优惠】13.3 8核M1芯片(7核图形处理器) 8G 256G SSD 深空灰 笔记本 MGN63CH/A', '￥7199.00', 'Apple产品京东自营旗舰店', '自营\n满4000-1400', '中国大陆', 'Apple M1', '13.0-13.9英寸', '8GB', 'APPLE M1 集成显卡', '20万+条评价');
INSERT INTO `jd_goods` VALUES ('10068747364657', '宏碁（acer）27英寸i5/i7一体机电脑摄像头高清大屏台式商用办公家用娱乐游戏全套 12核I7-12700 32G 1000G固态', '￥5799.00', '宏碁一体机旗舰店', '', '', 'intel i7', '25-27英寸', '32GB', '集成显卡', '1000+条评价');
INSERT INTO `jd_goods` VALUES ('100050134190', '华硕（ASUS）天选4 锐龙版 15.6英寸高性能电竞游戏本 笔记本电脑(新R9-7940H 16G 512G RTX4060 2.5K 165Hz P3广色域)青', '￥8299.00', '华硕京东自营官方旗舰店', '自营\n新品', '中国大陆', 'AMD R9', '15.0-15.9英寸', '16GB', 'RTX 4060', '1万+条评价');
INSERT INTO `jd_goods` VALUES ('100042671778', 'Apple Mac mini【教育优惠】 八核M2芯片 8G 256G SSD 台式电脑主机 MMFJ3CH/A', '￥3699.00', 'Apple产品京东自营旗舰店', '自营\n满3000-300', '中国大陆', 'Apple M2', '无显示器', '8GB', '集成显卡', '1万+条评价');
INSERT INTO `jd_goods` VALUES ('49816652076', '宏硕（hongshuo） 英特尔 酷睿i7/独立显卡/台式机电脑主机家用游戏办公组装电脑整机全套 套餐一 酷睿i7丨16G丨512G丨4G旗舰独显', '￥1198.00', '宏硕电脑旗舰店', '京东物流\n免邮', '中国大陆', 'intel i7', '无显示器', '16GB', 'HD7570', '10万+条评价');
INSERT INTO `jd_goods` VALUES ('100023239577', '联想拯救者R9000P 16英寸游戏笔记本电脑(8核16线程R7-6800H 16G 512G RTX3060 2.5k 165Hz高色域)灰', '￥6999.00', '联想京东自营旗舰店', '自营', '中国大陆', 'AMD R7', '16.0-16.9英寸', '16GB', 'RTX 3060', '20万+条评价');
INSERT INTO `jd_goods` VALUES ('100052940709', 'ROG魔霸新锐 2023 第13代英特尔酷睿i7 16英寸 星云屏 电竞游戏本笔记本电脑(i7-13650HX 液金导热16G 1T RTX4060 2.5K 240Hz P3广色域)', '￥10499.00', '玩家国度ROG京东自营官方旗舰店', '自营', '中国大陆', 'intel i7', '16.0-16.9英寸', '16GB', 'RTX 4060', '1万+条评价');
INSERT INTO `jd_goods` VALUES ('100041770928', '联想商用台式机ThinkCentre K70英特尔酷睿i5(i5-12500/16G/512 SSD/无光驱/集显/260W/OKR/Win11/21.45液晶', '￥4199.00', '联想智慧办公京东自营官方旗舰店', '自营', '中国大陆', 'intel i5', '20-21.5英寸', '16GB', '集成显卡', '2000+条评价');
INSERT INTO `jd_goods` VALUES ('100032989979', '攀升 商睿2代 办公商用家用台式电脑主机（12代i3-12100 16G 512G WiFi蓝牙 商务键鼠）', '￥1988.00', '攀升京东自营官方旗舰店', '自营', '中国大陆', 'intel i3', '无显示器', '16GB', '集成显卡', '5000+条评价');
INSERT INTO `jd_goods` VALUES ('100043941956', '酷开 创维电脑 23.8英寸办公商用家用娱乐一体机台式电脑（11代N5095 8G 256G SSD 双频WiFi 蓝牙）', '￥1399.00', '创维酷开电脑京东自营官方旗舰店', '自营\n新品', '中国大陆', 'intel 奔腾 赛扬', '22-24.5英寸', '8GB', '集成显卡', '1万+条评价');
INSERT INTO `jd_goods` VALUES ('23388780571', '线下同款\n亚当贝尔 电竞独显 办公游戏迷你家用组装吃鸡台式机电脑主机整机 主机+24英寸电竞显示器整套全套 配置四（E5十核/16G/512G/8G旗舰独显）', '￥1650.00', '亚当贝尔电脑旗舰店', '京东物流', '', 'intel', '23-25英寸', '16GB', 'NVIDIA Geforce GTX580', '5万+条评价');
INSERT INTO `jd_goods` VALUES ('100042659892', '机械革命（MECHREVO）蛟龙16K 16英寸游戏电竞笔记本电脑（R7-7735H 16G 512G RTX4060 165HZ 2.5K）', '￥6699.00', '机械革命京东自营官方旗舰店', '自营\n新品', '中国大陆', 'AMD R7', '16.0-16.9英寸', '16GB', 'RTX 4060', '2万+条评价');
INSERT INTO `jd_goods` VALUES ('100041351162', '联想(Lenovo)天逸510S 个人商务台式机电脑整机(13代i3-13100  16G 512G SSD wifi win11 )23英寸', '￥3599.00', '联想京东自营旗舰店', '自营', '中国大陆', 'intel i3', '23-25英寸', '16GB', '集成显卡', '5000+条评价');
INSERT INTO `jd_goods` VALUES ('10027766118128', 'i5 12400F/RTX3060独显电竞游戏台式机电脑主机', '￥3178.00', '酷耶电脑旗舰店', '', '中国大陆', 'intel i5', '无显示器', '16GB', 'GTX1660S', '5000+条评价');
INSERT INTO `jd_goods` VALUES ('100039098622', '联想笔记本电脑小新Pro16轻薄本 英特尔酷睿i5 16英寸超能本(12核标压i5 16G 512G 2.5K高刷屏)银 商务办公', '￥5299.00', '联想京东自营旗舰店', '自营\n满99-2', '中国大陆', 'intel i5', '16.0-16.9英寸', '16GB', '集成显卡', '20万+条评价');
INSERT INTO `jd_goods` VALUES ('100054131015', '华硕无畏15 2023 13代酷睿i5标压 15.6英寸轻薄便携大屏高性能办公笔记本电脑(i5-13500H 16G 512G 护眼屏)银', '￥4386.00', '华硕京东自营官方旗舰店', '自营\n新品\n赠', '中国大陆', 'intel i5', '15.0-15.9英寸', '16GB', '集成显卡', '5万+条评价');
INSERT INTO `jd_goods` VALUES ('100023681778', '京品电脑\n荣耀MagicBook 14 锐龙版 14英寸6核锐龙R5轻薄笔记本电脑 16G大内存 512G 冰河银 多屏协同 高色域', '￥3299.90', '荣耀京东自营旗舰店', '自营\n满10-0.9', '中国大陆', 'AMD R5', '14.0-14.9英寸', '16GB', 'AMD 集成显卡', '去看二手\n5万+条评价');
INSERT INTO `jd_goods` VALUES ('100032768490', '京品电脑\n戴尔（DELL）游匣G15 15.6英寸游戏本 笔记本电脑(14核i7 16G 512G RTX3060 165Hz高色域电竞屏)黑', '￥6499.00', '戴尔京东自营官方旗舰店', '自营', '中国大陆', 'intel i7', '15.0-15.9英寸', '16GB', 'RTX 3060', '5万+条评价');
INSERT INTO `jd_goods` VALUES ('100052658283', '华硕（ASUS）天选4 锐龙版 15.6英寸高性能电竞游戏本 笔记本电脑(新R7-7735H 16G 512G RTX4060 144Hz高色域电竞屏)青', '￥7299.00', '华硕京东自营官方旗舰店', '自营', '中国大陆', 'AMD R7', '15.0-15.9英寸', '16GB', 'RTX 4060', '1万+条评价');

-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES ('1001', '张玮', 'qwerdf');
INSERT INTO `login` VALUES ('1002', '张怀志', 'asdfer');
INSERT INTO `login` VALUES ('59509552-7caf-46ba-a28d-744f886f0f90', '张怀远', '123456');
INSERT INTO `login` VALUES ('c3b675ae-6e4c-46d5-95cb-f231ce43ee1f', '杨志', '123456');
INSERT INTO `login` VALUES ('c49ef96a-6e00-4abd-b5b2-0ead8c08986f', '李佳豪', '123456');
INSERT INTO `login` VALUES ('cb9ee8b3-1bc7-4527-8436-67458d513e94', '陆澳', '123456');

-- ----------------------------
-- Table structure for salgrade
-- ----------------------------
DROP TABLE IF EXISTS `salgrade`;
CREATE TABLE `salgrade`  (
  `GRADE` int(11) NULL DEFAULT NULL,
  `LOSAL` int(11) NULL DEFAULT NULL,
  `HISAL` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of salgrade
-- ----------------------------
INSERT INTO `salgrade` VALUES (1, 700, 1200);
INSERT INTO `salgrade` VALUES (2, 1201, 1400);
INSERT INTO `salgrade` VALUES (3, 1401, 2000);
INSERT INTO `salgrade` VALUES (4, 2001, 3000);
INSERT INTO `salgrade` VALUES (5, 3001, 9999);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1001, '王宇杰', 18, '安徽合肥');

SET FOREIGN_KEY_CHECKS = 1;
