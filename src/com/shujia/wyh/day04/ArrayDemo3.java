package com.shujia.wyh.day04;

/*
    定义两个数组，分别输出数组名及元素。然后分别给数组中的元素赋值，分别再次输出数组名及元素。

 */
public class ArrayDemo3 {
    public static void main(String[] args) {
        //定义第一个数组，假设元素是int类型
        int[] arr1 = new int[3];
        System.out.println(arr1);
        System.out.println(arr1[0]);
        System.out.println(arr1[1]);
        System.out.println(arr1[2]);
        System.out.println("------------------------------------");
        arr1[0] = 100;
        arr1[1] = 200;
        arr1[2] = 200; //数组中的元素值是可以发生重复的
        System.out.println(arr1);
        System.out.println(arr1[0]);
        System.out.println(arr1[1]);
        System.out.println(arr1[2]);
        System.out.println();
        //定义第二个数组，假设元素是int类型
        int[] arr2 = new int[2];
        System.out.println(arr2);
        System.out.println(arr2[0]);
        System.out.println(arr2[1]);
        System.out.println("--------------------------------------------");
        arr2[0] = 11;
        arr2[1] = 22;
        System.out.println(arr2);
        System.out.println(arr2[0]);
        System.out.println(arr2[1]);

    }
}
