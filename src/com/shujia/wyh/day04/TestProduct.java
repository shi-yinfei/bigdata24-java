package com.shujia.wyh.day04;

public class TestProduct {
    public static void main(String[] args) {
//指定商品信息并盘点
        ProductCategory category1 = new ProductCategory("11", "洗发水");
        Product p1 = new Product("111", "潘婷洗发水400ml",
                16, 40.5, category1);
        p1.check();

        System.out.println("==============");
//指定商品信息并盘点
        Product p2 = new Product();
        p2.setPid("222");
        p2.setName("蜂花洗发水250ml");
        p2.setPrice(11.5);
        p2.setAmount(-5);
        p2.setCategory(category1);
        p2.check();
    }
}