package com.shujia.wyh.day04;

/*
    数组的静态初始化：初始化时指定每个数组元素的初始值，由系统决定数组长度。
    语句定义格式：数据类型[] 数组名 = new int[]{元素1,元素2,元素3,...};
    举例：将11，22，33这三个元素使用数组进行存储
    int[] arr = new int[]{11,22,33};

    一维数组静态初始化的简化写法：数据类型[] 数组名 = {元素1,元素2,元素3,...};
    注意：简化写法只是针对上面的写法转换而言，实际上默认会进行添加new 数据类型[]，如果要简化写法的话，必须要与定义连接在一起。

 */
public class ArrayDemo5 {
    public static void main(String[] args) {
//        int[] arr = new int[]{11,22,33};
//        System.out.println(arr);
//        System.out.println(arr[0]);
//        System.out.println(arr[1]);
//        System.out.println(arr[2]);

        int[] arr = {11, 22, 33};
        System.out.println(arr);
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);
//        System.out.println(arr[3]); //ArrayIndexOutOfBoundsException  索引越界异常

        arr = null;
//        System.out.println(arr[0]);  //NullPointerException 空指针异常

        System.out.println("=============================");

//        int[] arr2;
//        arr2 = {100, 200, 300, 400, 500};
//        System.out.println(arr2);
    }
}
