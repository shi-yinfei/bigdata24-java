package com.shujia.wyh.day04;

/*
    数组元素逆序 (就是把元素对调)
    arr: 1,2,3,4,5
    对调后: 5,4,3,2,1
 */
public class ArrayTest3 {
    public static void main(String[] args) {
        //1、定义一个数组
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println("逆序前：");
        printArray(arr);

        //将第1个元素与最后一个元素对调
        int tmp1 = arr[0];
        arr[0] = arr[arr.length - 1 - 0];
        arr[arr.length - 1 - 0] = tmp1;

        //将第2个元素与倒数第2个元素对调
        int tmp2 = arr[1];
        arr[1] = arr[arr.length - 1 - 1];
        arr[arr.length - 1 - 1] = tmp2;

//        解决方案1：以找规律的方式解决
        for (int i = 0; i < arr.length / 2; i++) {
            int tmp = arr[i];
            arr[i] = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = tmp;
        }


        //解决方案2：
//        for (int start = 0, end = arr.length - 1; start < end; start++, end--) {
//            int tmp = arr[start];
//            arr[start] = arr[end];
//            arr[end] = tmp;
//        }
//
//
        //对调的次数是length/2
        System.out.println("逆序后：");
        printArray(arr);
//
//
    }

    /**
     * 返回值类型：void
     * 参数列表：int[] array
     */
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (i == 0) {
                System.out.print("[" + array[i] + ",");
            } else if (i == array.length - 1) {
                System.out.println(array[i] + "]");
            } else {
                System.out.print(array[i] + ",");
            }
        }
    }
}
