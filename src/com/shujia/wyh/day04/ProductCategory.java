package com.shujia.wyh.day04;
public class ProductCategory {
private String cid;
private String name;
public ProductCategory() {
        super();
        }
public ProductCategory(String cid, String name) {
        super();
        this.cid = cid;
        this.name = name;
        }
public String getCid() {
        return cid;
        }
public void setCid(String cid) {
        this.cid = cid;
        }
public String getName() {
        return name;
        }
public void setName(String name) {
        this.name = name;
        }
        }



