package com.shujia.wyh.day04;

public class TestITWorker{
    public static void main(String[] args){
        ITWorker it1=new ITWorker("小红",35,"数据库维护",10);
        ITWorker it2=new ITWorker("张三",10,"Java开发",1);

        it1.work("腾讯实业","数据库维护工程师"); //对象调用方法
        System.out.println("=================================");
        it2.work("鼎盛科技","Java开发工程师");

    }
}