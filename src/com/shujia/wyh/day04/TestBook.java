package com.shujia.wyh.day04;

public class TestBook {
    public static void main(String[] args) {
        Book b1 = new Book("鹿鼎记","金庸","人民文学出版社",100.0);
        b1.show();
        System.out.println("================");
        Book b2 = new Book("绝代双骄", "古龙", "中国长安出版社", 55.5);
        b2.show();
    }
}

