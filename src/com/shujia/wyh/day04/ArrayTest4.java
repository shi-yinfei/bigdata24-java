package com.shujia.wyh.day04;

import java.util.Scanner;

/*
    数组查表法(根据键盘录入索引,查找对应星期（作业）)
    一个星期：星期1，....，星期日
 */
public class ArrayTest4 {
    public static void main(String[] args) {
        //定义一个数组存储一个星期7天
        String[] arr = {"星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期天"};

        //创建键盘录入对象
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入(1-7)：");
        int index = sc.nextInt();
        if (index > 0 & index <= 7) {
            System.out.println(arr[index - 1]);
        } else {
            System.out.println("您输入的星期有误！");
        }

    }
}
