package com.shujia.wyh.day04;

public class Product {
    private String pid;
    private String name;
    private int amount;
    private double price;
    private ProductCategory category;
    public Product() {
        super();
    }
    public Product(String pid, String name, int amount, double price) {
        super();
        this.pid = pid;
        this.name = name;
        this.amount = amount;
        this.price = price;
    }
    public Product(String pid, String name, int amount, double price,
                   ProductCategory category) {
        super();
        this.pid = pid;
        this.name = name;
        this.setAmount(amount);
        this.price = price;
        this.category = category;
    }
    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        if(amount<0){
            System.out.println("库存数量异常，请联系管理员");
            this.amount = 0;
        }else{
            this.amount = amount;
        }
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public ProductCategory getCategory() {
        return category;
    }
    public void setCategory(ProductCategory category) {
        this.category = category;
    }
    public void check(){
        System.out.println("商品名称："+this.name);
        System.out.println("所属类别："+this.category.getName());
        System.out.println("库存数量："+this.price);
        System.out.println("商品售价："+this.amount);
        System.out.println("商品总价："+this.price*this.amount);

    }
}
