package com.shujia.wyh.day04;

public class Dog {
    private String  strain;
    private int age;
    private String mood;
    private String name;
    public Dog() {
        super();
    }
    public Dog(String strain, int age, String mood,String name) {
        super();
        this.strain = strain;
        this.age = age;
//this.mood = mood;
        this.setMood(mood);
        this.name = name;
    }
    public String getStrain() {
        return strain;
    }
    public void setStrain(String strain) {
        this.strain = strain;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getMood() {
        return mood;
    }
    public void setMood(String mood) {
        if("心情好".equals(mood) || "心情不好".equals(mood)){
            this.mood = mood;
        }else{
            System.out.println("输入信息有误，这只狗狗今天心情很好");
            this.mood ="心情好";
        }
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    //跑
    public void run(){
        if("心情好".equals(mood)){
            System.out.println("名字叫"+name+"的"+strain
                    +mood+",开心的围着主人身边转");
        }else{
            System.out.println("名字叫"+name+"的"
                    +strain+mood+",伤心的一动不动");
        }

    }
    //叫
    public void bark(){
        if("心情好".equals(mood)){
            System.out.println("名字叫"+name+"的"+strain
                    +mood+",开心的汪汪叫");
        }else{
            System.out.println("名字叫"+name+"的"
                    +strain+mood+",伤心的呜呜叫");
        }
    }
}


