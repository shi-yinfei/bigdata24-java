package com.shujia.wyh.day04;

public class TestDog {
    public static void main(String[] args) {
//1.过来一个狗狗
        Dog dog1 = new Dog();
        dog1.setName("甜心");
        dog1.setAge(2);
        dog1.setMood("心情不咋地");
        dog1.setStrain("贵宾犬");
        dog1.run();
        dog1.bark();
        System.out.println("================================");
//2.再过来一只狗狗
        Dog dog2 = new Dog("德国牧羊犬", 3, "心情不好", "太子");
        dog2.run();
        dog2.bark();
    }
}