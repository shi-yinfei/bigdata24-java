package com.shujia.wyh.day04;

/*
    二维数组：其实二维数组其实就是一个元素为一维数组的数组。
    语句定义格式1：
        数据类型[][] 数组名 = new 数据类型[m][n];
     举例：
        int[][] arr = new int[3][2];
        表示的是定义一个二维数组并初始化，二维数组中有3个元素，每个元素是一个长度为2的一维数组。
 */
public class Array2Demo1 {
    public static void main(String[] args) {
        //定义一个二维数组并初始化
        int[][] arr = new int[3][2];
        System.out.println(arr);
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);

        //获取二维数组中的第一个一维数组的第2个元素
        System.out.println(arr[0][1]);

//        int[] arr2 = new int[4]{11,22,33,44};
    }
}
