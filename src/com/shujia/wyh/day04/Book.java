package com.shujia.wyh.day04;
public class Book {
    public String title;//书名
    public String writer;//作者
    public String press;//出版社名
    public double price;//价格
    public Book(String title, String writer, String press, double price) {
        this.title = title;
        this.writer = writer;
        this.press = press;
        this.setPrice(price);
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getWriter() {
        return writer;
    }
    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getPress() {
        return press;
    }
    public void setPress(String press) {

        this.press = press;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        if(price <= 10){
            System.out.println("限定价格必须大于10");
            this.price = price;
        }else {
            this.price = price;
        }
    }
    public void show(){
        System.out.println("书名："+this.title);
        System.out.println("作者："+this.writer);
        System.out.println("出版社："+this.press);
        System.out.println("价格："+this.price + "元");
    }
}
