package com.shujia.wyh.day04;

/*
    数组遍历(依次输出数组中的每一个元素)

 */
public class ArrayTest1 {
    public static void main(String[] args) {
        //定义一个数组int类型
        int[] arr = {32, 44, 12, 5, 233, 65, 2, 35, 676};
//        System.out.println(arr[0]);
//        System.out.println(arr[1]);
//        System.out.println(arr[2]);
//        System.out.println(arr[3]);
//        System.out.println(arr[4]);
//        System.out.println(arr[5]);
//        System.out.println(arr[6]);
//        System.out.println(arr[7]);
//        System.out.println(arr[8]);

        //我们使用之前的技术，根据索引一个一个的获取可以拿到每一个元素值，但是我们有可能会一不小心多写一个，如果多写一个就会出现索引越界异常
        //我们上面没有出现这个问题的原因是，我们数了总共多少个元素，然后计算出最大的索引是多少
        //但是呢，当数组的元素过多，你还能数得过来码
        int[] arr2 = {32, 44, 12, 5, 233, 65, 2, 35, 676, 32, 44, 12, 5, 233, 65, 2, 35, 676, 32, 44, 12, 5, 233, 65, 2, 35, 676, 32, 44, 12, 5, 233, 65, 2, 35, 676, 32, 44, 12, 5, 233, 65, 2, 35, 67632, 44, 12, 5, 233, 65, 2, 35, 676, 32, 44, 12, 5, 233, 65, 2, 35, 67632, 44, 12, 5, 233, 65, 2, 35, 67632, 44, 12, 5, 233, 65, 2, 35, 67632, 44, 12, 5, 233, 65, 2, 35, 67632, 44, 12, 5, 233, 65, 2, 35, 676};
        //元素一多，我们就数不过来了。
        //java中的数组提供了一个属性给我们使用
        //这个属性叫做length
        //如何使用呢？语句定义格式：数组名.length 整体是一个int类型的结果
        int length = arr2.length;
        System.out.println("数组的长度是：" + length);
//        System.out.println(arr2[0]);
//        System.out.println(arr2[1]);
//        System.out.println(arr2[2]);
//        System.out.println(arr2[3]);
//        //..
//        System.out.println(arr2[arr2.length - 1]);
        //使用for循环
//        for (int i = 0; i < arr2.length; i++) {
//            if (i == 0) {
//                System.out.print("[" + arr2[i] + ",");
//            } else if (i == arr2.length - 1) {
//                System.out.println(arr2[i] + "]");
//            } else {
//                System.out.print(arr2[i] + ",");
//            }
//        }

        //将来我们可能会使用很多次打印数组的功能，于是使用方法进行封装
        printArray(arr2);
        System.out.println("---------------------------------------------");
        printArray(arr2);


    }


    /**
     * 返回值类型：void
     * 参数列表：int[] array
     */
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (i == 0) {
                System.out.print("[" + array[i] + ",");
            } else if (i == array.length - 1) {
                System.out.println(array[i] + "]");
            } else {
                System.out.print(array[i] + ",");
            }
        }
    }

}
