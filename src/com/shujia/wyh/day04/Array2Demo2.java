package com.shujia.wyh.day04;

/*
    当二维数组中的一维数组元素个数不确定时候，语句定义格式1就不适用了
    于是我们引出二维数组的定义格式2：
        数据类型[][] 数组名 = new 数据类型[3][];

 */
public class Array2Demo2 {
    public static void main(String[] args) {
        //使用语句定义格式2创建一个二维数组
        int[][] arr = new int[3][];

        System.out.println(arr);
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);

        int[] arr1 = new int[10];
        int[] arr2 = new int[11];
        int[] arr3 = new int[20];

        //将一维数组arr1赋值给二维数组中的第一个元素
        arr[0] = arr1;
        arr[1] = arr2;
        arr[2] = arr3;
        System.out.println("--------------------------------------");
        System.out.println(arr);
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);



    }
}
