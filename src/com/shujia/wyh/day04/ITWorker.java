package com.shujia.wyh.day04;

public class ITWorker{
    //创建成员变量
    String name;
    int age;
    String tend; //工作方向
    int workAge; //工作年限


    public ITWorker(){ //无参构造方法
    }

    public ITWorker(String name,int age,String tend,int workAge){ //有参构造方法
        this.name=name;
        this.age=age;
        this.tend=tend;
        this.workAge=workAge;
    }

    public void work(String company,String job){ //  公司  职位
        if(age<15){
            age=15;
            System.out.println("年龄信息无效!已修改默认年龄为15");
        }
        System.out.println("姓名:"+name);
        System.out.println("年龄:"+age);
        System.out.println("技术方向:"+tend);
        System.out.println("工作年限:"+workAge);
        System.out.println("目前就职于:"+company);
        System.out.println("职务是:"+job);
    }

}



