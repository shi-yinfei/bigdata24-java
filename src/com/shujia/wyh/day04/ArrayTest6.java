package com.shujia.wyh.day04;

/*
    看程序写结果

    结论：
        1、当基本数据类型作为方法实参传入的时候，传的是具体的数值，对于原本的变量值不影响
        2、当引用数据类型作为方法实参传入的时候，传的是地址值，会影响到地址值对应的内存空间的数值。
 */
public class ArrayTest6 {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        System.out.println("a:" + a + ",b:" + b);  //a:10 ,b:20
        change(a, b);
        System.out.println("a:" + a + ",b:" + b);  //a:10 ,b:20

        int[] arr = {1, 2, 3, 4, 5};
        System.out.println(arr[1]); // 2
        change(arr);
        System.out.println(arr[1]); // 4
    }

    public static void change(int a, int b) {
        System.out.println("a:" + a + ",b:" + b); // a:10 ,b:20
        a = b; // a: 20
        b = a + b; // b: 40
        System.out.println("a:" + a + ",b:" + b); // a:20 ,b:40
    }

    public static void change(int[] arr) {
        for (int x = 0; x < arr.length; x++) {
            if (arr[x] % 2 == 0) {
                arr[x] *= 2;
            }
        }
    }

}
