package com.shujia.wyh.day04;

public class Array2Test1 {
    public static void main(String[] args) {
        //遍历得到每一个楼层的房间号，假设共3层，每一层有5个房间。楼层与房间的顺序组成房间号  203
        //定义一个二维数组表示这栋楼
        int[][] arr = {{101, 102, 103, 104, 105}, {201, 202, 203, 204, 205}, {301, 302, 303, 304, 305}};

        //外层for循环遍历得到每一个一维数组
        for (int i = 0; i < arr.length; i++) {
            System.out.println("===============当前在第 "+(i+1)+" 层=====================");
            //内层for循环遍历得到一维数组中的每一个元素
            for (int j = 0; j < arr[i].length; j++) {
                System.out.println(arr[i][j]);
            }
        }

    }
}
