package com.shujia.wyh.day04;

/*
    数组获取最值(获取数组中的最大值最小值)

 */
public class ArrayTest2 {
    public static void main(String[] args) {
        //1、定义一个数组并初始化
        int[] arr = {34, 2, 41, 55, 3, 1, 67, 3556, 34, 22, 76};

        //2、将数组中的第一个元素默认为是最大值或者是最小值
        int maxNumber = arr[0];

        //3、遍历数组，拿着这个最大值依次比较，当比较的元素值比最大值还要大，直接替换
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > maxNumber) {
                maxNumber = arr[i];
            }
        }

        System.out.println("数组中最大值为：" + maxNumber);


        //2、将数组中的第一个元素默认为是最大值或者是最小值
        int minNumber = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < minNumber) {
                minNumber = arr[i];
            }
        }

        System.out.println("数组中最小值为：" + minNumber);
    }
}
