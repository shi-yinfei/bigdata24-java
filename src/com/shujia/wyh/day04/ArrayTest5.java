package com.shujia.wyh.day04;

import java.util.Scanner;

/*
    数组元素查找(查找指定元素第一次在数组中出现的索引（作业）)

 */
public class ArrayTest5 {
    public static void main(String[] args) {
        //定义一个数组并初始化
        int[] arr = {34, 2, 41, 55, 3, 1, 67, 3556, 34, 22, 76};

        Scanner sc = new Scanner(System.in);
        System.out.println("请输入您要查找的元素：");
        int number = sc.nextInt();

        //定义一个标志位，往往是boolean类型的
        boolean flag = true;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == number) {
                System.out.println("找到该元素，索引为："+i);
                //当找到元素的时候，修改标志位的值
                flag = false;
                break;
            }
        }

        if(flag){
            System.out.println("对不起，数组中没有该元素！！");
        }




    }
}
