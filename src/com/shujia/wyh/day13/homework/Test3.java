package com.shujia.wyh.day13.homework;

import java.util.HashMap;

/*
     假如有以下email数据“aa@sohu.com,bb@163.com,cc@sina.com”
     现需要把email 中的用户部分和邮件地址部分分离，分离后以键值对应的方式放入HashMap？
 */
public class Test3 {
    public static void main(String[] args) {
        String s = "aa@sohu.com,bb@163.com,cc@sina.com";
        HashMap<String, String> map = new HashMap<>();

        String[] youxiang1 = s.split(",");
        for (String s1 : youxiang1) {
            String username = s1.split("@")[0];
            String address = s1.split("@")[1];

            map.put(username, address);
        }

        System.out.println(map);
    }
}
