package com.shujia.wyh.day13.homework;

import java.util.*;

/*
    实现List和Map数据的转换。具体要求如下：
    功能1 ：定义方法public void listToMap( ){ }将List中Student元素封装到Map中
    1)    使用构造方法Student(int id,String name,int age,String sex )创建多个学生 信息并加入List
    2)    遍历List ，输出每个Student信息
    3)    将List中数据放入Map ，使用Student的id属性作为key ，使用Student对象 信息作为value
    4)    遍历Map ，输出每个Entry的key和value
    功能2 ：定义方法public void mapToList( ){ }将Map中Student映射信息封装到List
    1)     创建实体类StudentEntry ，可以存储Map中每个Entry的信息
    2)    使用构造方法Student(int id,String name,int age,String sex )创建多个学生 信息，并使用Student的id属性作为key ，存入Map
    3)    创建List对象，每个元素类型是StudentEntry
    4)    将Map中每个Entry信息放入List对象
 */
public class Test2 {
    public static void main(String[] args) {
        ArrayList<Student> list1 = new ArrayList<>();

        Student s1 = new Student(1001, "王宇杰", 18, "男");
        Student s2 = new Student(1002, "张怀远", 17, "男");
        Student s3 = new Student(1003, "史俊超", 16, "男");
        Student s4 = new Student(1004, "陆澳", 19, "男");

        list1.add(s1);
        list1.add(s2);
        list1.add(s3);
        list1.add(s4);
        System.out.println(list1);
        System.out.println("-----------------------------");
        Test2 test2 = new Test2();
        test2.listToMap(list1);
    }

    public void listToMap(List<Student> list) {
        HashMap<Integer, Student> map = new HashMap<>();
        //遍历List集合得到学生对象
        for (Student student : list) {
            int id = student.getId();
            map.put(id, student);
        }

        //遍历map集合
        Set<Map.Entry<Integer, Student>> entries = map.entrySet();
        for (Map.Entry<Integer, Student> entry : entries) {
            Integer key = entry.getKey();
            Student value = entry.getValue();
            System.out.println(key + "---" + value);
        }
    }
}
