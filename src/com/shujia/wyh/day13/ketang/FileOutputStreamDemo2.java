package com.shujia.wyh.day13.ketang;

import java.io.FileOutputStream;

/*
    字节输出流写数据的方式：
        public void write(int b)
        public void write(byte[] b)
        public void write(byte[] b,int off,int len)

    两种写文件的方式：
        覆盖：其实内部做了两件事：a.清空数据  b.写数据
        追加：在文件数据末尾处写数据

 */
public class FileOutputStreamDemo2 {
    public static void main(String[] args) throws Exception {
        //1、创建一个字节输出流对象
        //默认是以覆盖的形式写数据
        FileOutputStream fos = new FileOutputStream("src/com/shujia/wyh/day13/ketang/a.txt",true);


        //public void write(int b)
//        fos.write(97);  //写入的是一个ASCII码值

        //public void write(byte[] b)
        //将字节数组写入到文件中
        byte[] bytes = {97,98,99,100,101};
//        fos.write(bytes);

        // public void write(byte[] b,int off,int len)
        //将字节数组的一部分写入到文件中
        fos.write(bytes,1,3);
        //写一个换行符
        fos.write("\r\n".getBytes());

        //注意：在使用完io流后，要关闭连接
        fos.close();

    }
}
