package com.shujia.wyh.day13.ketang;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

/*
    java为了加快字节输入和输出流的速度，分别对应提供了字节缓冲输入流和字节缓冲输出流
    字节缓冲输入流：InputStream
                        -- FileInputStream
                        -- BufferedInputStream
    字节缓冲输出流：OutputStream
                        -- FileOutputStream
                        -- BufferedOutputStream

 */
public class BufferedOutputStreamDemo1 {
    public static void main(String[] args) throws Exception {
        //BufferedOutputStream(OutputStream out)
        //创建一个新的缓冲输出流，以将数据写入指定的底层输出流。
        //创建字节缓冲输出流对象  在写完数据后，推荐每写一次，刷新一次缓冲区
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("src/com/shujia/wyh/day13/ketang/c.txt",true));
//        System.out.println(bos);
//        bos.write("张怀远比王宇杰更帅！".getBytes());
//        bos.flush();

        bos.write(97);
        bos.flush();




        //释放资源
        bos.close();

//        bos.write("数加科技666".getBytes());
//        bos.close();

    }
}
