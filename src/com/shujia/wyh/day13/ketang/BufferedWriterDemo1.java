package com.shujia.wyh.day13.ketang;

import java.io.*;

/*
    字符缓冲输出流：BufferedWriter

    字符缓冲输入流：BufferedReader
 */
public class BufferedWriterDemo1 {
    public static void main(String[] args) throws Exception {
        //BufferedWriter(Writer out)
        //创建使用默认大小的输出缓冲区的缓冲字符输出流。
//        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/com/shujia/wyh/day13/ketang/f.txt")));
        //简化写法
//        BufferedWriter bw = new BufferedWriter(new FileWriter("src/com/shujia/wyh/day13/ketang/f.txt",true));
//        bw.write("张怀远也很帅！！");
////        bw.write("\r\n");  // 在不同的操作系统下，换行符是不同的
//        bw.newLine(); //写一个换行符，可以根据操作系统来生成换行符
//        bw.flush();

        //BufferedReader(Reader in)
        //创建使用默认大小的输入缓冲区的缓冲字符输入流。
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("src/com/shujia/wyh/day13/ketang/f.txt")));
//        char[] chars = new char[1024];
//        int length = 0;
//        while ((length=br.read(chars))!=-1){
//            String s1 = new String(chars, 0, length);
//            System.out.print(s1);
//        }
        //特殊的读取数据方法
        //public String readLine()
//        System.out.println(br.readLine());
//        System.out.println(br.readLine());
//        System.out.println(br.readLine());
//        System.out.println(br.readLine());
//        System.out.println(br.readLine());
        String line = null;
        //readline本身不会读取到换行符
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }


        //释放资源
//        bw.close();
    }
}
