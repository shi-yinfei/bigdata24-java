package com.shujia.wyh.day13.ketang;

import java.io.File;
import java.io.FileOutputStream;

/*
    IO流分类：
        1、按照流向划分：（以java程序为中心）
            输入流：外部数据（文件，数据库，网页数据）--->java
            输出流：java--->写出到外部数据中（文件，数据库，等）
        2、按照数据类型划分：（使用记事本打开，看看是否能够看懂）
            字节流：无论是否能不能看懂都可以采用字节流进行读写，万能流
            字符流：只针对使用记事本能看懂的文件  （字符流=字节流+编码表）

   先讲解字节流：
        按照流向：
            字节输入流：InputStream -- FileInputStream
            字节输出流：OutputStream(这个抽象类是表示字节输出流的所有类的超类) -- FileOutputStream


   FileOutputStream的构造方法：
        FileOutputStream(File file)
        FileOutputStream(String name)

 */
public class FileOutputStreamDemo1 {
    public static void main(String[] args) throws Exception{
        //FileOutputStream(File file)
        //将目标文件封装成File对象
//        File file = new File("src/com/shujia/wyh/day13/ketang/a.txt");
//        FileOutputStream fos = new FileOutputStream(new File("src/com/shujia/wyh/day13/ketang/a.txt")); // 如果写入的文件不存在，将会自动创建
//        System.out.println(fos);
//
        //FileOutputStream(String name)
        FileOutputStream fos = new FileOutputStream("src/com/shujia/wyh/day13/ketang/a.txt");  //如果存在，就使用存在的那个文件
        System.out.println(fos);


    }
}
