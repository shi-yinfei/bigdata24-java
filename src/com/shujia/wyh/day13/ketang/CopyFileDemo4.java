package com.shujia.wyh.day13.ketang;

import java.io.*;

/*
    使用字符流复制一个文本文件：
    数据源：src/com/shujia/wyh/day13/ketang/d.txt
        字符输入流 -- Reader -- InputStreamReader
            1、一次读取一个字符
            2、一次读取一个字符数组
        字符缓冲输入流 BufferedReader
            1、一次读取一行
    目的地：E:\\a.txt
        字符输出流 -- Writer -- OutputStreamWriter
            1、一次写一个字符
            2、一次写一个字符数组的一部分
         字符缓冲输出流：BufferedWriter
            1、一次写一个字符串
 */
public class CopyFileDemo4 {
    public static void main(String[] args) throws Exception {
        //创建字符输入流对象
//        InputStreamReader isr = new InputStreamReader(new FileInputStream("src/com/shujia/wyh/day13/ketang/d.txt"));
//        //创建字符输出流对象
//        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("E:\\a.txt"));
//
//
//        //一次读写一个字符
////        int i = 0;
////        while ((i= isr.read())!=-1){
////            osw.write(i);
////            osw.flush();
////        }
//
//
//        //一次读写一个字符数组
//        char[] chars = new char[1024];
//        int length = 0;
//        while ((length = isr.read(chars)) != -1) {
//            osw.write(chars, 0, length);
//            osw.flush();
//        }

        BufferedReader br = new BufferedReader(new FileReader("src/com/shujia/wyh/day13/ketang/d.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("E:\\zhy.txt"));

        //使用字符缓冲流一次读写一行
        String line = null;
        while ((line=br.readLine())!=null){
            bw.write(line);
            bw.newLine();
            bw.flush();
        }


        //释放资源
        bw.close();
        br.close();
//        osw.close();
//        isr.close();
    }
}
