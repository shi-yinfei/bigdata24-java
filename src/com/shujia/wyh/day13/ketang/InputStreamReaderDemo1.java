package com.shujia.wyh.day13.ketang;

import java.io.FileInputStream;
import java.io.InputStreamReader;

/*
    字符输入流：使用字符流读取数据的时候需要考虑写入时编码的问题，因为直接影响到读取的数据能否看懂
        Reader -- InputStreamReader

    只要保证写入的编码和读取的编码一致，就能看懂，与中间存储的编码没关系
 */
public class InputStreamReaderDemo1 {
    public static void main(String[] args) throws Exception {
        //InputStreamReader(InputStream in)
        //创建一个使用默认字符集的InputStreamReader。
//        InputStreamReader isr = new InputStreamReader(new FileInputStream("src/com/shujia/wyh/day13/ketang/d.txt"));

        //InputStreamReader(InputStream in, String charsetName)
        //创建一个使用命名字符集的InputStreamReader。
        InputStreamReader isr = new InputStreamReader(new FileInputStream("src/com/shujia/wyh/day13/ketang/d.txt"));
        //int read() 一次读一个字符
//        System.out.println((char) isr.read());
//        int i = 0;
//        while ((i=isr.read())!=-1){
//            System.out.print((char) i);
//        }

        //int read(char[] cbuf, int offset, int length)
        //将字符读入数组的一部分。
        char[] chars = new char[1024];
        int length = 0;
        while ((length = isr.read(chars)) != -1) {
            String s1 = new String(chars, 0, length);
            System.out.print(s1);
        }
//        int length = isr.read(chars); //返回的是读取到的字符个数
//        String s1 = new String(chars, 0, length);
//        System.out.println(s1);

    }
}
