package com.shujia.wyh.day13.ketang;

import java.io.BufferedInputStream;
import java.io.FileInputStream;

/*
    字节缓冲输入流 BufferedInputStream
 */
public class BufferedInputStreamDemo1 {
    public static void main(String[] args) throws Exception {
        //创建字节缓冲输入流对象
        //BufferedInputStream(InputStream in)
        //创建一个 BufferedInputStream并保存其参数，输入流 in ，供以后使用。
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("src/com/shujia/wyh/day13/ketang/a.txt"));

        //一次读取一个字节
//        System.out.println(bis.read());
//        int i = 0;
//        while ((i = bis.read()) != -1) {
//            System.out.print((char) i);
//        }

        //一次读取一个字节数组
        byte[] bytes = new byte[1024];
        int length = 0;
        while ((length = bis.read(bytes)) != -1) {
            String s1 = new String(bytes, 0, length);
            System.out.println(s1);
        }


        //释放资源
        bis.close();

    }
}
