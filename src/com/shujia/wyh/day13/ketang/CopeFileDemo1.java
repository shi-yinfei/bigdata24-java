package com.shujia.wyh.day13.ketang;

import java.io.FileInputStream;
import java.io.FileOutputStream;

/*
    把当前项目目录下的a.txt内容复制到当前项目目录下的b.txt中

    数据源：a.txt --- 输入流 --- 字节输入流 --- InputStream --- FileInputStream
        1、一次读取一个字节
        2、一次读取一个字节数组
    目的地：b.txt --- 输出流 --- 字节输出流 --- OutputStream --- FileOutputStream
        1、一次写一个字节
        2、一次写一个字节数组（字节数组的一部分）

 */
public class CopeFileDemo1 {
    public static void main(String[] args) throws Exception {
        //创建字节输入流对象
        FileInputStream fis = new FileInputStream("src/com/shujia/wyh/day13/ketang/a.txt");
        //创建字节输出流对象
        FileOutputStream fos = new FileOutputStream("src/com/shujia/wyh/day13/ketang/b.txt");

        //方式1：一次读写一个字节
//        int i = 0;
//        while ((i=fis.read())!=-1){
//            fos.write(i);
//        }

        //方式2：一次读写一个字节数组
        byte[] bytes = new byte[1024];
        int length = 0;
        while ((length = fis.read(bytes)) != -1) {
            fos.write(bytes, 0, length);
        }


        //释放资源
        fos.close();
        fis.close();
    }
}
