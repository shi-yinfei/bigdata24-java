package com.shujia.wyh.day13.ketang;

import java.io.FileOutputStream;
import java.io.IOException;

/*
    加入异常处理的IO流操作

 */
public class FileOutputStreamDemo3 {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            //1、创建一个字节输出流对象
            fos = new FileOutputStream("src/com/shujia/wyh/day13/ketang/b.txt");
            fos.write("王宇杰是大帅哥！".getBytes());
            fos.write("\r\n".getBytes());

        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if(fos!=null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
