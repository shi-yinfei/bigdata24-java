package com.shujia.wyh.day13.ketang;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/*
    编码：将看得懂-->看不懂
    解码：将看不懂-->看得懂

    要想让读取的数据能够看懂，就必须知道存储的时候时候以什么编码进行写的
 */
public class BianMaDemo1 {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String s = "今天晚上出去活动";

        //编码
        byte[] bytes = s.getBytes("GBK");  //GBK一个字符占2个字节
        System.out.println(Arrays.toString(bytes));

        //解码
        String s1 = new String(bytes,"GBK");
        System.out.println(s1);

    }
}
