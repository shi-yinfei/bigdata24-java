package com.shujia.wyh.day13.ketang;

import java.io.File;

/*
    递归遍历目录下指定后缀名结尾的文件名称 E:\\ 包括子目录
 */
public class DiGuiDemo2 {
    public static void main(String[] args) {
        //将E盘封装成File对象
        File file = new File("E:\\");
        findJpgFile(file);
    }

    public static void findJpgFile(File file){
        //出口
        if (file.isFile() & file.getName().endsWith(".jpg")){
            System.out.println(file.getName());
        }else {
            File[] files = file.listFiles();
            if(files!=null){
                for (File file1 : files) {
                    findJpgFile(file1);
                }
            }
        }
    }
}
