package com.shujia.wyh.day13.ketang;

import java.io.FileInputStream;

/*
    字节输入流类：InputStream -- FileInputStream

    public int read()
    public int read(byte[] b)

 */
public class FileInputStreamDemo1 {
    public static void main(String[] args) throws Exception {
        //1、创建字节输入流对象
        FileInputStream fis = new FileInputStream("src/com/shujia/wyh/day13/ketang/b.txt");

//        System.out.println(fis);

        //public int read()
        //一次读取一个字节
//        System.out.println((char) fis.read());
//        System.out.println((char) fis.read());
//        System.out.println((char) fis.read());
//        System.out.println((char) fis.read());
//        System.out.println((char) fis.read());
//        System.out.println((char) fis.read());
//        System.out.println((char) fis.read());
        //因为一个文件的字节数个数不清楚，所以调用read()读取字节的次数也不知道
        //当我们使用字节流读取汉字的时候，发现，读不懂，因为一个字符在不同的编码下，所占的字节数是不同的
        //而我们现在是读取一个字节，就转字符，不是一个完整的汉字，所以看不懂
//        int i = 0;
//        while ((i = fis.read()) != -1) {
//            char read = (char) i;
//            System.out.print(read);
//        }


        //public int read(byte[] b)
        //创建一个空的字节数组，用于接收读取到字节
        byte[] bytes = new byte[1024];
//        int length = fis.read(bytes); //返回的是实际获取到的字节数
//        System.out.println(length);
//        String s1 = new String(bytes, 0, length);  //将字节数组一部分转成字符串
//        System.out.println(s1);
        //bcdbcdbcdbcd\r\n
        //bcd
        int length = 0;
        while ((length = fis.read(bytes)) != -1) {
            System.out.println(length);
            String s1 = new String(bytes, 0, length);  //将字节数组一部分转成字符串 \r\n占两个字节
            System.out.println(s1);
            //bcdbc
            //dbcdb
            //cd\r\n
            //b
            //cd
        }


        fis.close();
    }
}
