package com.shujia.wyh.day13.ketang;

import java.io.*;

/*
    将F:\录屏\22期day06 数据分析可视化 4.mp4 复制到E:\目录下
    数据源：F:\录屏\22期day06 数据分析可视化 4
        输入流 -- InputStream
                    -- FileInputStream
                        1、一次读取一个字节
                        2、一次读取一个字节数组
                    -- BufferedInputStream
                        1、一次读取一个字节
                        2、一次读取一个字节数组
    目的地：E:\wyj.mp4
        输出流 -- OutputStream
                    -- FileOutputStream
                        1、一次写一个字节
                        2、一次写一个字节数组（字节数组一部分）
                    -- BufferedOutputStream
                        1、一次写一个字节
                        2、一次写一个字节数组（字节数组一部分）
 */
public class CopyFileDemo3 {
    public static void main(String[] args) {
//        copy1(); // 大于2分钟
//        copy2(); //5308毫秒 -- 5秒钟左右

        copy3();  // 1秒种左右
    }

    //BufferedInputStream读取一个字节数组
    //BufferedOutputStream写一个字节数组
    public static void copy3() {
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            bis = new BufferedInputStream(new FileInputStream("F:\\录屏\\22期day06 数据分析可视化 4.mp4"));
            bos = new BufferedOutputStream(new FileOutputStream("E:\\wyj3.mp4"));

            byte[] bytes = new byte[1024];
            int length = 0;
            long startTime = System.currentTimeMillis();
            while ((length = bis.read(bytes)) != -1) {
                bos.write(bytes, 0, length);
                bos.flush();  //缓冲流记得刷新
            }

            long endTime = System.currentTimeMillis();

            System.out.println("复制结束，共耗时：" + (endTime - startTime) + "毫秒");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    //FileInputStream读取一个字节数组
    //FileOutputStream写一个字节数组
    public static void copy2() {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream("F:\\录屏\\22期day06 数据分析可视化 4.mp4");
            fos = new FileOutputStream("E:\\wyj2.mp4");

            byte[] bytes = new byte[1024];
            int length = 0;
            long startTime = System.currentTimeMillis();
            while ((length = fis.read(bytes)) != -1) {
                fos.write(bytes, 0, length);
            }

            long endTime = System.currentTimeMillis();

            System.out.println("复制结束，共耗时：" + (endTime - startTime) + "毫秒");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    //FileInputStream读取一个字节
    //FileOutputStream写一个字节
    public static void copy1() {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream("F:\\录屏\\22期day06 数据分析可视化 4.mp4");
            fos = new FileOutputStream("E:\\wyj.mp4");

            int i = 0;
            long startTime = System.currentTimeMillis();

            while ((i = fis.read()) != -1) {
                fos.write(i);
            }
            long endTime = System.currentTimeMillis();

            System.out.println("复制结束，共耗时：" + (endTime - startTime) + "毫秒");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
