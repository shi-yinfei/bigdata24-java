package com.shujia.wyh.day13.ketang;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/*
    字符输出流写数据的时候也要刷新
    字符输出流的方法：
        public void write(int c)
        public void write(char[] cbuf)
        public void write(char[] cbuf,int off,int len)
        public void write(String str)
        public void write(String str,int off,int len)

 */
public class OutputStreamWriterDemo2 {
    public static void main(String[] args) throws Exception {
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("src/com/shujia/wyh/day13/ketang/e.txt"),"GBK");

        //public void write(int c)
//        osw.write(97);
//        osw.flush();

        //public void write(char[] cbuf) 写一个字符数组
//        char[] chars = {'王', '宇', '杰', '真', '帅'};
//        osw.write(chars);
//        osw.flush();

        //public void write(char[] cbuf,int off,int len) 写字符数组的一部分
//        osw.write(chars,3,2);
//        osw.flush();

        //public void write(String str)  写一个字符串
        osw.write("今天的天气还不错");
        osw.flush();

        //public void write(String str,int off,int len) 写字符串的一部分
//        osw.write("今天的天气还不错",3,2);
//        osw.flush();

        osw.close();


    }
}
