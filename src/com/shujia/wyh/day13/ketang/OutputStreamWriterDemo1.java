package com.shujia.wyh.day13.ketang;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/*
    字符流（转换流） = 字节流+编码表
    字符输出流：Writer -- OutputStreamWriter
    字符输入流：Reader

 */
public class OutputStreamWriterDemo1 {
    public static void main(String[] args) throws Exception {
        //创建字符输出流对象
        //public OutputStreamWriter(OutputStream out)
        //如果目标文件不存在，将会自动创建
//        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("src/com/shujia/wyh/day13/ketang/d.txt"));
//        System.out.println(osw);


        //public OutputStreamWriter(OutputStream out,String charsetName)
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("src/com/shujia/wyh/day13/ketang/d.txt", true), "GBK");

        //释放资源
        osw.close();
    }
}
