package com.shujia.wyh.day13.ketang;

import java.io.*;

/*
    字符流的简化写法：
        字符输入流：Reader -- InputStreamReader -- FileReader
        字符输出流：Writer -- OutputStreamWriter -- FileWriter
 */
public class FileWriterDemo1 {
    public static void main(String[] args) throws Exception {
        //创建字符输入流对象
//        InputStreamReader isr = new InputStreamReader(new FileInputStream("src/com/shujia/wyh/day13/ketang/d.txt"));
        FileReader fr = new FileReader("src/com/shujia/wyh/day13/ketang/d.txt");
        //创建字符输出流对象
//        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("E:\\a.txt"));
        FileWriter fw = new FileWriter("E:\\b.txt");


        //一次读写一个字符
//        int i = 0;
//        while ((i= isr.read())!=-1){
//            osw.write(i);
//            osw.flush();
//        }


        //一次读写一个字符数组
        char[] chars = new char[1024];
        int length = 0;
        while ((length = fr.read(chars)) != -1) {
            fw.write(chars, 0, length);
            fw.flush();
        }


        //释放资源
        fw.close();
        fr.close();
    }
}
