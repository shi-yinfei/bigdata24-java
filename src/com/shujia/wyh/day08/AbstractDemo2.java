package com.shujia.wyh.day08;

/*
    学生案例
    具体事务：二十三期学员，二十四期学员
    共性：姓名，年龄，班级，学习，吃饭

 */

abstract class Student{
    String name;
    int age;
    String clazz;

    public abstract void study();

    public void eat(){
        System.out.println("吃大米饭");
    }
}

class TwentyThreeStudent extends Student{
    TwentyThreeStudent(String name,int age){
        this.name = name;
        this.age = age;
    }


    @Override
    public void study() {
        System.out.println("23期学员学习Hbase");
    }
}

class TwentyFourStudent extends Student{
    @Override
    public void study() {
        System.out.println("24期学院学习Java");
    }
}


public class AbstractDemo2 {
    public static void main(String[] args) {
        //创建一个23期的学员
        //使用多态
        Student s1 = new TwentyThreeStudent("王宇杰",18);  //抽象多态
        s1.eat();
        s1.study();
    }
}
