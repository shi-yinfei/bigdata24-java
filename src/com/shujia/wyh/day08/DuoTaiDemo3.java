package com.shujia.wyh.day08;

/*
    多态的好处
        提高了程序的维护性(由继承保证)
        提高了程序的扩展性(由多态保证)

 */

class Animal2{
    String name;
    int age;

    public void eat(){
        System.out.println("吃");
    }

    public void sleep(){
        System.out.println("睡");
    }
}

class Dog2 extends Animal2{
    @Override
    public void eat() {
        System.out.println("🐕吃🥩");
    }

    @Override
    public void sleep() {
        System.out.println("🐕侧着睡");
    }
}

class Cat extends Animal2{
    @Override
    public void eat() {
        System.out.println("🐱吃🐟");
    }

    @Override
    public void sleep() {
        System.out.println("🐱蜷着睡");
    }
}

class Pig extends Animal2{
    @Override
    public void eat() {
        System.out.println("🐖吃饲料");
    }

    @Override
    public void sleep() {
        System.out.println("🐖跪着睡");
    }
}

//创建动物工具类
class AnimalTool{

    private AnimalTool(){}

//    public static void useCat(Cat cat){
//        cat.eat();
//        cat.sleep();
//    }
//
//
//    public static void useDog(Dog2 dog2){
//        dog2.eat();
//        dog2.sleep();
//    }
//
//    public static void usePig(Pig pig){
//        pig.eat();
//        pig.sleep();
//    }

    //使用多态修改
    public static void useAnimal(Animal2 animal2){ //Animal2 animal2 = new Pig()
        animal2.eat();
        animal2.sleep();
    }
}




public class DuoTaiDemo3 {
    public static void main(String[] args) {
        //我想养一只狗
        Dog2 dog1 = new Dog2();
//        dog1.eat();
//        dog1.sleep();
//        useDog(dog1);
//        AnimalTool.useDog(dog1);
        AnimalTool.useAnimal(dog1);

        //我还想养一只狗
        Dog2 dog2 = new Dog2();
//        dog2.eat();
//        dog2.sleep();
//        useDog(dog2);
//        AnimalTool.useDog(dog2);
        AnimalTool.useAnimal(dog2);

        //我还想养10只狗，按照我们的做法应该new10次，调用20次方法
        //new对象不可避免，调用方法每次重复写很麻烦，可以使用方法进行封装

        //我现在不仅想要养狗，还想养猫

        Cat cat = new Cat();
//        cat.eat();
//        cat.sleep();
//        useCat(cat);
//        AnimalTool.useCat(cat);
        AnimalTool.useAnimal(cat);

        //假如我不仅想要养狗和猫，我还想养猪，牛，老虎，狮子等等动物
        //按照我们上面的做法，应该先创建对应的类，然后在创建对应的方法，然后创建对象，调用方法实现动物的功能。
        //创建类不可避免，而方法也会越来越多
        //当方法越来越多的时候，这个类就显得十分臃肿，我们可以提取到一个工具类中

        Pig pig = new Pig();
//        AnimalTool.usePig(pig);
        AnimalTool.useAnimal(pig); //new Pig()

        ///虽然我们使用工具类封装了方法，但是每当有一个新的动物，都要修改工具类，但是工具类一般情况下是不允许修改的

    }

//    public static void useCat(Cat cat){
//        cat.eat();
//        cat.sleep();
//    }
//
//
//    public static void useDog(Dog2 dog2){
//        dog2.eat();
//        dog2.sleep();
//    }
}
