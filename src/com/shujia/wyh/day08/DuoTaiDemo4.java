package com.shujia.wyh.day08;

/*
    多态访问成员方法的特点：编译看左，运行看右
    多态的弊端：不能访问子类中特有的方法
    解决方案：向下转型
 */

class Fu2{
    public void fun1(){
        System.out.println("这是父类中的成员方法fun1");
    }
}

class Zi2 extends Fu2{
    public void show(){
        System.out.println("这是子类中的show方法");
    }

}

public class DuoTaiDemo4 {
    public static void main(String[] args) {
        //使用多态创建出Zi2对象
        Fu2 fu2 = new Zi2();
//        fu2.fun1();
//        fu2.show();
        //向下转型，转成堆内存中实际的类型
        Zi2 zi2 = (Zi2) fu2;
        zi2.show();
    }
}
