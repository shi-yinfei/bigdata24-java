package com.shujia.wyh.day08;

/*
    接口：java提供接口这个技术，主要是实现某个类的扩展功能而存在的。
    动物类 Animal(抽象类)
    狗 Dog(具体的类)
    会钻火圈的狗 DrillingDog(具体的类)
    钻火圈 Drilling（接口）

    java提供一个关键字：interface来定义一个接口
 */
abstract class Animal4{
    public abstract void eat();

    public abstract void sleep();
}

//定义一个普通的狗的类
//生成实现方法的快捷键：alt+Enter
class Dog4 extends Animal4{

    @Override
    public void eat() {
        System.out.println("狗吃肉");
    }

    @Override
    public void sleep() {
        System.out.println("狗侧着睡");
    }
}

//定义一个钻火圈的接口
interface Drilling{
    public abstract void drilling();
}

class DrillingDog extends Dog4 implements Drilling{

    @Override
    public void drilling() {
        System.out.println("钻火圈");
    }
}


public class InterfaceDemo1 {
    public static void main(String[] args) {
        DrillingDog drillingDog = new DrillingDog();
        drillingDog.eat();
        drillingDog.sleep();
        drillingDog.drilling();
    }
}
