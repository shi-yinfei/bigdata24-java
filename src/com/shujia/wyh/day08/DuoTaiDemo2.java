package com.shujia.wyh.day08;

/*
    多态访问成员的特点：
        成员变量:  编译看左，运行也看左
        构造方法: 不能访问，因为构造方法不能被继承
        成员方法: 编译看左，运行看右
        静态的成员: 编译看左，运行也看左
 */

class Fu{
    int a = 10;

    public void fun1(){
        System.out.println("这是父类中的fun1方法");
    }

    public static void fun2(){
        System.out.println("这是父类中的静态方法fun2");
    }
}

class Zi extends Fu{
    int a = 20;

    @Override
    public void fun1(){
        System.out.println("这是子类中的重写后的fun1方法");
    }

    public static void fun2(){
        System.out.println("这是子类中的静态方法fun2");
    }

}

public class DuoTaiDemo2 {
    public static void main(String[] args) {
        Fu fu = new Zi();
//        System.out.println(fu.a); // 10
//        fu.fun1();
        fu.fun2();
    }
}
