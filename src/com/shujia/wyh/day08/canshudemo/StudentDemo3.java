package com.shujia.wyh.day08.canshudemo;

/*
    形式参数
        基本类型: 当基本数据类型作为方法的形式参数的时候，将来调用时传入的是具体的数值
        引用类型:
            具体的类：当你看到一个方法的形式参数类型是具体的类的话，将来调用时需要传入该类或者该类的子类对象
            抽象类：当你看到一个方法的形式参数类型是一个抽象类的话，将来调用时需要传入该抽象类的具体子类对象
            接口：当你看到一个方法的形式参数类型是一个接口的话，将来调用时需要传入实现该接口的子类对象

 */
interface Inter1{
    void fun1();
}

class Student3 implements Inter1{

    @Override
    public void fun1() {
        System.out.println("好好学习，天天向上");
    }
}

class StudentTest3{
    public void show(Inter1 inter1){ //Inter1 inter1 = new Student3()  接口多态
        inter1.fun1();
    }
}

public class StudentDemo3 {
    public static void main(String[] args) {
        StudentTest3 studentTest3 = new StudentTest3();
//        Inter1 inter1 = new Inter1();
        Student3 student3 = new Student3();
        studentTest3.show(student3); //new Student3()
    }
}
