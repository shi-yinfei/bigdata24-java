package com.shujia.wyh.day08.canshudemo;

/*
    形式参数
        基本类型: 当基本数据类型作为方法的形式参数的时候，将来调用时传入的是具体的数值
        引用类型:
            具体的类：当你看到一个方法的形式参数类型是具体的类的话，将来调用时需要传入该类或者该类的子类对象
            抽象类：当你看到一个方法的形式参数类型是一个抽象类的话，将来调用时需要传入该抽象类的具体子类对象
            接口：

 */
abstract class Student2{
    public void fun1(){
        System.out.println("好好学习，天天向上！");
    }
}

class Student2Zi extends Student2{

}

class StudentTest2{
    public void show(Student2 student2){ //Student2 student2 = new Student2Zi()
        student2.fun1();
    }
}


public class StudentDemo2 {
    public static void main(String[] args) {
        StudentTest2 studentTest2 = new StudentTest2();
//        Student2 student2 = new Student2();
        Student2Zi student2Zi = new Student2Zi();
        studentTest2.show(student2Zi);
    }
}
