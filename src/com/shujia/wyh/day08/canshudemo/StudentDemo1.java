package com.shujia.wyh.day08.canshudemo;

/*
    形式参数
        基本类型: 当基本数据类型作为方法的形式参数的时候，将来调用时传入的是具体的数值
        引用类型:
            具体的类：当你看到一个方法的形式参数类型是具体的类的话，将来调用时需要传入该类或者该类的子类对象
            抽象类：
            接口：

 */
class Student1{
    public void fun1(){
        System.out.println("好好学习，天天向上");
    }
}

class StudentTest1{
    public void show(Student1 student1){ //Student1 student1 = new Student1()
        student1.fun1();
    }
}


public class StudentDemo1 {
    public static void main(String[] args) {
        StudentTest1 studentTest1 = new StudentTest1();
        Student1 student1 = new Student1();
        studentTest1.show(student1);
    }
}
