package com.shujia.wyh.day08.quanxain.demo1;

public class Demo1 {
    public String name = "王宇杰";
    protected int age = 18;
    String address = "安徽合肥";
    private int phoneNum = 12345678;

    public void fun1(){
        System.out.println(name);
        System.out.println(age);
        System.out.println(address);
        System.out.println(phoneNum);
    }

}
