package com.shujia.wyh.day08.quanxain.demo1;


/*
    java中有4种权限修饰符：

                        public     protected    默认的     private
    同一类中               √            √          √          √
    同一包子类,其他类       √            √          √
    不同包子类             √            √
    不同包其他类           √




 */

public class PowerDemo1 {
    public static void main(String[] args) {
        Demo1 demo1 = new Demo1();
//        demo1.fun1();
        System.out.println(demo1.name);
        System.out.println(demo1.age);
        System.out.println(demo1.address);
//        System.out.println(demo1.phoneNum);
    }
}
