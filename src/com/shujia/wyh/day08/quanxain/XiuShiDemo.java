package com.shujia.wyh.day08.quanxain;

/*
    类：
        默认，public，final，abstract
        我们自己定义：public居多
    成员变量：
        四种权限修饰符均可,final,static
        我们自己定义：private居多
    构造方法：
        四种权限修饰符均可,其他不可
        我们自己定义：public 居多
    成员方法：
        四种权限修饰符均可，final,static,abstract
        我们自己定义：public居多

 */

//protected class Demo1{
//
//}

public class XiuShiDemo {
    public static void main(String[] args) {

    }
}
