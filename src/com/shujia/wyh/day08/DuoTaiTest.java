package com.shujia.wyh.day08;

/*
    不同地方饮食文化不同的案例
    Person
        eat()
    SouthPerson
        eat()
    NorthPerson
        eat()

 */

class Person{
    public void eat(){
        System.out.println("吃");
    }
}

class SouthPerson extends Person{
    @Override
    public void eat() {
        System.out.println("南方人吃大米饭");
    }

    public void daMaJiang(){
        System.out.println("打麻将");
    }
}

class NorthPerson extends Person{
    @Override
    public void eat() {
        System.out.println("北方人吃面食");
    }

    public void shaoKao(){
        System.out.println("吃烧烤");
    }
}

public class DuoTaiTest {
    public static void main(String[] args) {
        //创建一个南方人
        Person p1 = new SouthPerson();
        p1.eat();
//        p1.daMajiang();  //由于多态编译看左，运行看右，父类中没有该方法，编译不通过
        //向下转型
//        SouthPerson sp1 = (SouthPerson) p1;
//        sp1.daMaJiang();

        //ClassCastException 类型转换异常
        NorthPerson np = (NorthPerson) p1;  //向下转型的前提，堆内存中的对象类型要和转的类型是继承关系或者就是本身的类型
        np.shaoKao();

    }
}
