package com.shujia.wyh.day08;

/*
    一个类如果没有抽象方法，可不可以定义为抽象类?如果可以，有什么意义?  相当于一个逻辑概念的汇总
    abstract不能和哪些关键字共存
        private	冲突
        final	冲突
        static	无意义


 */

abstract class A2{
//    private abstract void fun1();

//    public abstract final void fun1();

//    public  static abstract void fun1();

}

public class AbstractDemo3 {
    public static void main(String[] args) {

    }
}
