package com.shujia.wyh.day08.fanhuizhidemo;

/*
    返回值类型
        基本类型: 当基本数据类型作为方法返回值类型的时候，将来调用完方法需要返回的时具体类型的值
        引用类型:
            具体的类：当你看到一个类作为方法的返回值类型的时候，将来方法中会返回一个该类的对象或者是该类的子类对象
            抽象类：当你看到一个抽象类作为方法的返回值类型的时候，将来方法中会返回一个该抽象类的具体子类对象
            接口：

 */

abstract class Teacher2{
    public void fun1(){
        System.out.println("明师短才，教书育人");
    }
}

class Teacher2Zi extends Teacher2{

}

class TeacherTest2{
    public Teacher2 show(){
        return new Teacher2Zi();
    }
}

public class TeacherDemo2 {
    public static void main(String[] args) {
        TeacherTest2 teacherTest2 = new TeacherTest2();
        Teacher2 t1 = teacherTest2.show(); //Teacher2 t1 = new Teacher2Zi()
        t1.fun1();
    }
}
