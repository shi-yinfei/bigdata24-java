package com.shujia.wyh.day08.fanhuizhidemo;


/*
    返回值类型
        基本类型: 当基本数据类型作为方法返回值类型的时候，将来调用完方法需要返回的时具体类型的值
        引用类型:
            具体的类：当你看到一个类作为方法的返回值类型的时候，将来方法中会返回一个该类的对象或者是该类的子类对象
            抽象类：当你看到一个抽象类作为方法的返回值类型的时候，将来方法中会返回一个该抽象类的具体子类对象
            接口：当你看到一个接口作为方法的返回值类型的时候，将来方法中会返回一个实现该接口的子类对象

 */
interface Inter3{
    void fun1();
}

class Inter3Impl implements Inter3{

    @Override
    public void fun1() {
        System.out.println("好好学习，天天向上");
    }
}

class TeacherTest3{
    public Inter3 show(){
        return new Inter3Impl();
    }
}

public class TeacherDemo3 {
    public static void main(String[] args) {
//        TeacherTest3 teacherTest3 = new TeacherTest3();
//        Inter3 i3 = new TeacherTest3().show(); //Inter3 i3 = new Inter3Impl()
        new TeacherTest3().show().fun1();   //链式编程，链式调用 scala
    }
}
