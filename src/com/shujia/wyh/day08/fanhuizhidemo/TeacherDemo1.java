package com.shujia.wyh.day08.fanhuizhidemo;

/*
    返回值类型
        基本类型: 当基本数据类型作为方法返回值类型的时候，将来调用完方法需要返回的时具体类型的值
        引用类型:
            具体的类：当你看到一个类作为方法的返回值类型的时候，将来方法中会返回一个该类的对象或者是该类的子类对象
            抽象类：
            接口：

 */
class Teacher {
    public void fun1() {
        System.out.println("明师短才，教书育人");
    }
}

class TeacherTest {
    public Teacher show() {
        return new Teacher();
    }
}


public class TeacherDemo1 {
    public static void main(String[] args) {
        TeacherTest teacherTest = new TeacherTest();
        Teacher t1 = teacherTest.show(); //Teacher t1 = new Teacher()
        t1.fun1();
    }
}
