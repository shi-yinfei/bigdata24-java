package com.shujia.wyh.day08;

/*
    interface关键字的使用注意事项：
        1、接口理解为一个特殊的一个类，将来也会被编译成一个class文件
        2、接口中只允许存在常量，而且是静态的，如果没有加修饰符，默认会加上public static final
        3、接口中只允许出现抽象方法，如果没有加上修饰符，默认会加上public abstract
        4、接口无法被实例化
        5、接口中没有构造方法
        6、接口和接口之间是继承关系，接口与接口的继承关系中，可以进行多继承
        7、当一个具体的类实现一个接口的时候，必须要重写所有的抽象方法
        8、当一个抽象类实现一个接口的时候，可以选择性的重写，也可以都不重写
 */
//interface Inter1{
//    void fun1();
//    void fun2();
//}
//
//abstract class Demo1 implements Inter1{
//    @Override
//    public void fun1() {
//
//    }
//}

//class Demo1 implements Inter1{
//
//    @Override
//    public void fun1() {
//
//    }
//
//    @Override
//    public void fun2() {
//
//    }
//}


//interface Inter1{
//
//}
//
//interface Inter3{
//
//}
//
//interface Inter2 extends Inter1,Inter3{
//
//}

//interface Inter1{
//
////    Inter1(){
////
////    }
//
//    public static final int a = 10;
//
//    void fun1();
//}
//
////当一个类实现一个接口的时候，需要使用implements关键字进行实现
//class Inter1Impl implements Inter1{
//    public void fun1(){
////        a=20;
//        System.out.println(a);
//    }
//}

public class InterfaceDemo2 {
    public static void main(String[] args) {
//        Inter1Impl inter1 = new Inter1Impl();
//        inter1.fun1();
//        System.out.println(Inter1.a);
//        Inter1 inter1 = new Inter1();
    }
}
