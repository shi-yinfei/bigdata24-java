package com.shujia.wyh.day08.test;

public class PingpangSportMan extends SportMan implements StudyEnglish{
    @Override
    public void fun() {
        System.out.println("乒乓球运动员");
    }

    @Override
    public void studyEnglish() {
        System.out.println("乒乓球运动员说英语");
    }
}
