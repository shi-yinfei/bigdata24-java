package com.shujia.wyh.day08.test;

public class PingpangCoach extends Coach implements StudyEnglish{
    @Override
    public void fun() {
        System.out.println("乒乓球教练");
    }

    @Override
    public void studyEnglish() {
        System.out.println("乒乓球教练说英语");
    }
}
