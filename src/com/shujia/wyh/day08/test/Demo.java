package com.shujia.wyh.day08.test;

public class Demo {
    public static void main(String[] args) {
        //创建一个篮球教练
        BasketBallCoach basketBallCoach = new BasketBallCoach();
        basketBallCoach.fun();
        //创建一个乒乓球教练
        PingpangCoach pingpangCoach = new PingpangCoach();
        pingpangCoach.fun();
        pingpangCoach.studyEnglish();
    }
}
