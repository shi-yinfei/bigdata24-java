package com.shujia.wyh.day08;

/*
    当现实生活中，有些概念并没有具体的事物体现的时候，我们在java定义该类的时候，就不应该定义成具体的类，因为具体的类将来可以创建对象
    java提供了一个关键字：abstract 抽象的
    abstract的用法：
        可以修饰类：abstract关键字加在class关键前
        修饰成员方法：在权限修饰符后面加上，注意如果一个方法被修饰成抽象方法的话，请注意不要有方法体，连大括号都不能有

    abstract关键字使用的注意事项：
        1、如果是一个具体的类继承抽象类的话，具体类中必须要重写抽象类中的所有抽象方法
        2、如果一个类中有抽象方法，这个类必须定义成抽象类
        3、抽象类既可以存在抽象方法，也可以存在具体实现的方法
        4、抽象类继承抽象类的时候，可以不重写抽象父类中的方法，也可以选择重写
        5、抽象类中既可以存在变量，也可以存在常量
        6、抽象类不能实例化，也就是不能new创建对象

 */
//这个类就叫抽象类
//abstract class Animal3{
//    //定义一个抽象方法吃
//    public abstract void eat();
//
//    //定义一个抽象方法睡
//    public abstract void sleep();
//}
//
////如果一个class类不加abstract关键字，被称作是一个具体的类
//class Dog3 extends Animal3{
//    @Override
//    public void eat() {
//        System.out.println("🐕吃🥩");
//    }
//
//    @Override
//    public void sleep() {
//        System.out.println("🐕侧着睡");
//    }
//}


//abstract class Animal3 {
//    //定义一个抽象方法吃
//    public abstract void eat();
//
//    public void fun1(){
//        System.out.println("Hello World!!");
//    }
//}

abstract class A{
    int a = 10;
    final int b = 20;

    //抽象类存在构造方法的目的是将来对父类做初始化的作用
//    A(){
//
//    }

    public abstract void fun1();
}

abstract class B extends A{

}




public class AbstractDemo1 {
    public static void main(String[] args) {
//        A a = new A();
    }
}
