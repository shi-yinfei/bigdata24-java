package com.shujia.wyh.day08;

/*
    多态：某一个事物在不同时刻下的不同状态
        举例：
            水 -->  液态 气态 固态
            动物  --> 狗 猫 猪 牛
            水果 --> 苹果 香蕉 西瓜

    java中使用多态的前提：
        1、要有继承关系
        2、要有方法的重写
            不是必须的，但是不重写，意义不大
        3、要有父类的引用指向子类对象


 */
//定义一个动物类
class Animal{
    String name;
    int age;

    public void eat(){
        System.out.println("吃");
    }
}

//狗动物
class Dog extends Animal{
    @Override
    public void eat(){
        System.out.println("🐕吃🥩");
    }
}



public class DuoTaiDemo1 {
    public static void main(String[] args) {
        //要有父类的引用执行子类对象
        Animal animal = new Dog();  //类之间的多态 从右向左读 狗是动物 Dog类和Animal类之间存在继承关系
    }
}
