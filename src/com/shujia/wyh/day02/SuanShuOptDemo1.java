package com.shujia.wyh.day02;


/*
    操作数：参与运算的常量或者变量值
    操作符：将参与运算的常量或者变量值连接起来的符号叫做操作符
    表达式：由操作数和操作符构成并且符合java语法规范的式子，叫做表达式，根据操作符的不同，即可以被称作不同的表述

    算术运算符：
        + - * / % ++ --

    重点：一个表达式的最终结果数据类型是由参与表达式中的最大的数据类型决定的
 */
public class SuanShuOptDemo1 {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        System.out.println(a+b);
        System.out.println(a-b);
        System.out.println(a*b);
        System.out.println(a/b); //取整数部分 0
        //需求：我就想让a/b整除具有小数部分
        //解决方案：将任意一个变量变成浮点类型就可以
        System.out.println((float)a/b);

        System.out.println(a%b); //取余数 10

    }
}
