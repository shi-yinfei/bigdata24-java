package com.shujia.wyh.day02;

import java.util.Scanner;

public class ScannerTest2 {
    public static void main(String[] args) {
        //创建Scanner对象
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入第一个数字：");
        int num1 = sc.nextInt();
        System.out.println("请输入第二个数字：");
        int num2 = sc.nextInt();
        int maxNumber = (num1 > num2) ? num1 : num2;
        System.out.println("两数中最大值为：" + maxNumber);
    }
}
