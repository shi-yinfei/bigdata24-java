package com.shujia.wyh.day02;

/*
    int a = 4;
    int b = (a++)+(++a)+(a*10);

 */
public class SuanShuOptDemo4 {
    public static void main(String[] args) {
        int a = 4;
        int b = (a++)+(++a)+(a*10);
        //   b=     70
        //   a=     6
        System.out.println("a: "+a); // 6
        System.out.println("b: "+b); // 70
    }
}
