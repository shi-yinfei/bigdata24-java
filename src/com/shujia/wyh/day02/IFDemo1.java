package com.shujia.wyh.day02;

/*
    选择结构：
        if语句
        switch语句

    if语句定义格式1：
        if(关系表达式){
            语句体;
        }

 */
public class IFDemo1 {
    public static void main(String[] args) {
        //需求：如果杨志是男生，就去男厕所
        //男生 1 女生 0
        int gender = 0;
        if(gender==1){
            System.out.println("请去男厕所");
        }
    }
}
