package com.shujia.wyh.day02;

/*
    三目运算符（三元运算符）
    语句定义格式：(条件表达式)?表达式1:表达式2

    注意事项：
        1、三目运算符一定是有一个结果值的
        2、三目运算符表达式中，最终的结果数据类型是由参与运算的最大的数据类型决定的

    获取三个整数中的最大值


 */
public class SanMuOptDemo1 {
    public static void main(String[] args) {
        //判断两个数中的最大值
//        int a = 10;
//        long b = 5L;
//        long c = (a>b)?a:b;
////        (a>b)?System.out.println(a):System.out.println(b);  //是一个错误的做法
//        System.out.println(c);

        int a = 3;
        int b = 4;
        int c = 5;
        int d = (a>b)?((a>c)?a:c):((b>c)?b:c);
        System.out.println(d);
    }
}
