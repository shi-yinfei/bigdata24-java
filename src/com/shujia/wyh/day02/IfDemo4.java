package com.shujia.wyh.day02;

/*
    编写if语句的注意事项：
        1、if语句大括号可以省略，但是只有当语句体只是一行语句的时候，可以省略，当语句体是多条语句的时候，如果省略了，只会针对第一行语句生效，建议永远不要省略大括号
        2、如果在if小括号后面加上分号，相当于小括号后面默认有一个空语句体
 */
public class IfDemo4 {
    public static void main(String[] args) {
        int a = 5;
        if(a>10){
            System.out.println("hello");
            System.out.println("world");
        }

    }
}
