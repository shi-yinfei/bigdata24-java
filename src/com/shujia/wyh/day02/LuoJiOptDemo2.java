package com.shujia.wyh.day02;

/*
    &&
        简单使用的情况下，和一个&结果没有区别，但是当&&左边的表达式结果为false的时候，就不会计算右边的表达式了，结果就为false
    ||
        简单使用的情况下，和一个|结果没有区别，但是当||左边的表达式结果为true的时候，就不会计算右边的表达式了，结果就为true
 */
public class LuoJiOptDemo2 {
    public static void main(String[] args) {
        int a = 3;
        int b = 4;
        int c = 5;

        // &
//        System.out.println(a > b & a < c);
//        System.out.println(a < b & a > c);
//        System.out.println(a > b & a > c);
//        System.out.println(a < b & a < c);

        // &&
//        System.out.println(a > b && a < c);
//        System.out.println(a < b && a > c);
//        System.out.println(a > b && a > c);
//        System.out.println(a < b && a < c);
//        System.out.println(a++ == 3 && b++ == 6);
//        System.out.println(a);
//        System.out.println(b);

        System.out.println(++a == 3 && b++ == 4);
        System.out.println(a); // 4
        System.out.println(b);


    }
}
