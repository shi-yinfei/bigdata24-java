package com.shujia.wyh.day02;
import java.util.Scanner;

/*
    if语句定义格式3：
        if(关系表达式1){
            语句体1;
        }else if(关系表达式2){
            语句体2;
        }else if(关系表达式n){
            ...
        }else{
            //建议在最后写上else，写上else的目的是为了让程序更加严谨
        }

    考试成绩案例：
    0-59：不及格 棍棒伺候
    60-80：良好  奖励看电视
    80-100：优秀  奖励吃肯德基
    其他：输入的数据有误
 */
public class ifDemo3 {
    public static void main(String[] args) {
        //创建Scanner对象
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入杨志的考试成绩：");
        int score = sc.nextInt();

        //java中不允许连续比较 使用使用逻辑运算符进行连接
        if(0<=score & score<60){
            System.out.println("不及格 棍棒伺候");
        }else if(60<=score & score<80){
            System.out.println("良好  奖励看电视");
        }else if(80<=score & score<=100){
            System.out.println("优秀  奖励吃肯德基");
        }else {
            System.out.println("您输入的数据有误！！");
        }
    }
}
