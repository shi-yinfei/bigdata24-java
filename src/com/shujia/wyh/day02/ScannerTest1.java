package com.shujia.wyh.day02;
import java.util.Scanner;

/*
    键盘录入两个数据，并对这两个数据求和，输出其结果
 */
public class ScannerTest1 {
    public static void main(String[] args) {
        //创建Scanner对象
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入第一个数字：");
        int num1 = sc.nextInt();
        System.out.println("请输入第二个数字：");
        int num2 = sc.nextInt();

        System.out.println("两数之和为："+(num1+num2));


    }
}
