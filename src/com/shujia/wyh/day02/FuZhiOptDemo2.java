package com.shujia.wyh.day02;

/*
    面试题
        short s=1; s = s+1;
        short s=1; s+=1;
        上面两个代码有没有问题，如果有，那里有问题

        第一行代码会出现强制类型转换的错误
        s+=1 =不等价=> s = s+1
        s+=1 =======> s = (s的数据类型)(s+1)

 */
public class FuZhiOptDemo2 {
    public static void main(String[] args) {
//        short s = 1;
//        s = (short)(s + 1);
//        System.out.println(s);

        short s = 1;
        s += 1;
        System.out.println(s);

    }
}
