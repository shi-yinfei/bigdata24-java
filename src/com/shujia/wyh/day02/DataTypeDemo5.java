package com.shujia.wyh.day02;

/*
    System.out.println(“hello”+’a’+1);
    System.out.println(‘a’+1+”hello”);
    System.out.println(“5+5=”+5+5);
    System.out.println(5+5+”=5+5”);

    1、加号两边，只要有一边是字符串，那么这个加号就做字符串的拼接运算
    2、加号两边如果没有字符串，就做加法运算
    3、可以使用小括号改变运算的顺序
 */
public class DataTypeDemo5 {
    public static void main(String[] args) {
        System.out.println("hello" + 'a' + 1);
        System.out.println('a' + 1 + "hello");
        System.out.println("5+5=" + (5 + 5));
        System.out.println(5 + 5 + "=5+5");
    }
}
