package com.shujia.wyh.day02;
import java.util.Scanner;
public class homework {
    public static void main(String[] args) {
        System.out.println("请输入圆的半径:");
        Scanner sc = new Scanner(System.in);
        int R = sc.nextInt();
        System.out.println("该圆的半径为:R=" + R);
        System.out.println("该圆的周长为:C=" + R*2*3.14);
        System.out.println("该圆的面积为:S=" + R*R*3.14);
    }
}
