package com.shujia.wyh.day02;
import java.util.Scanner;
/*
        if语句定义格式2：
            if(关系表达式){
                语句体1;
            }else{
                语句体2;
            }

        注意：if..else语句结构中，只有一个语句会执行
 */
public class IfDemo2 {
    public static void main(String[] args) {
        //创建Scanner对象
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入今天的天气：");
        String weather = sc.next();

        // name = "小虎"
        // "小虎".equals(name)

        //比较字符串内容是否相同，不可以使用==比较
        //应该比较的方式：比较的字符串.equals(待比较的字符串)
        if("晴天".equals(weather)){
            System.out.println("出去春游");
        }else {
            System.out.println("在家敲代码");
        }

    }
}
