package com.shujia.wyh.day02;

/*
    ++ : 自加1
        单独使用的情况下，++在变量的前面或者后面的结果是一样的。
        混合使用的时候，++在变量的后面，先进行赋值运算，再进行自加1运算
        ++在变量的前面的时候，先进行自加1运算，然后再进行赋值操作
    -- : 自减1
        单独使用的情况下，--在变量的前面或者后面的结果是一样的。
        混合使用的时候，--在变量的后面，先进行赋值运算，再进行自减1运算
        --在变量的前面的时候，先进行自自减1运算，然后再进行赋值操作
 */
public class SuanShuOptDemo2 {
    public static void main(String[] args) {
        int a = 10;

        //单独使用的情况下
//        a++;
//        System.out.println(a);
//        ++a;
//        System.out.println(a);

        //混合使用
//        int c = a++;
        int c2 = ++a;
        System.out.println("a: "+a); // 11
        System.out.println("c2: "+c2); // 11
    }
}
