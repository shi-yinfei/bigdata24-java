package com.shujia.wyh.day02;

/*
    关系运算符（比较运算符）
    == > < >= <= !=

    注意：
        1、一个关系表达式的计算结果一定是boolean类型
        2、==比较不能只写一个=
 */
public class GuanXiOptDemo1 {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        System.out.println(a==b);
    }
}
