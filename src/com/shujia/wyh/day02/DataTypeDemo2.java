package com.shujia.wyh.day02;

/*
    需求：求两个数之和

    1、常量之间参与运算，会先进行数值相加，然后拿着结果去看一看在不在目标数据类型的范围内，如果在，就直接赋值
    2、变量相加会先提升数据类型，然后在做运算

    数据类型之间有着两种转换：
        自动类型转换：
            (byte,short,char)--->int--->long--->float--->double
            byte,short,char类型的变量，参与运算的时候，会先提升变成int类型再参与运算
        强制类型转换:  有可能会造成精度损失，可能会将正确的结果截取掉了。
            语句定义格式：目标数据类型 变量名 = (目标数据类型)(要转型的值或表达式)
 */
public class DataTypeDemo2 {
    public static void main(String[] args) {
        int a = 3;
        byte b = 4;
//        byte c1 = a + b;  // 变量之间的运算

        byte c1 = (byte)(a + b);
        byte c2 = 3 + 4;  // 常量之间的运算
        System.out.println(c1);
        System.out.println(c2);

    }
}
