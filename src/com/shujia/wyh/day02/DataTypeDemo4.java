package com.shujia.wyh.day02;

/*
    看程序写结果：
    System.out.println('a');
    System.out.println('a'+1);

    当字符与数值相加的时候，会将字符对应的ASCII码值取出来与数值相加
    需要记住3个字符对应的ASCII码值：
        '0'   48
        'A'   65
        'a'   97
 */
public class DataTypeDemo4 {
    public static void main(String[] args) {
        System.out.println('a'); //单独打印字符，就是打印字符本身
        System.out.println('a' + 1); //'a'--97
    }
}
