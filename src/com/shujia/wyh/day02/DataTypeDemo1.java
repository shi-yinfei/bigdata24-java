package com.shujia.wyh.day02;

/*
    java是一个强类型语言，每一个变量都规定了一种数据类型
    在java中，不同的数据类型所占用的内存空间大小是不一样的
    分类：
        基本数据类型
            整数类型            所占字节数                 能够表示的数据范围
                byte              1                      -2^7 ~ 2^7-1
                short             2                      -2^15 ~ 2^15-1
                int               4                      -2^31 ~ 2^31-1
                long              8                      -2^63 ~ 2^63-1
            浮点类型
                float             4
                double            8
            字符类型
                char              不确定（根据编码的不同，一个字符所占的字节数也是不同的，后面IO流的时候详细讲解）
            布尔类型
                boolean           1
        引用数据类型（放到数组的时候讲解）

        使用变量的注意事项：
            1、如果要定义一个long类型的变量，在数值后面需要加上l或者L表示是一个long类型的数据，推荐使用L
            2、整数默认是int类型
            3、小数默认是double类型的
            4、如果要定义一个float类型的变量,在数值后面需要加上f或者F表示是一个float类型的数据,推荐使用F
            5、定义变量的时候需要注意作用域(大括号内部)，同一作用域下，不能定义重名的变量
            6、一个变量，如果没有初始化值，是无法被使用的，只要在使用之前进行赋值即可，推荐在定义变量的时候就赋值
            7、在方法上或者内部定义的变量称之为局部变量，局部变量定义的时候推荐要给值。
            8、建议一行只定义一个变量



 */
public class DataTypeDemo1 {
    public static void main(String[] args) {
        //变量的定义语句格式：
        //数据类型 变量名 = 初始化值;
        //定义一个byte类型的变量
        byte b1 = 10;
        //定义一个short类型的变量
        short s1 = 100;
        //定义一个int类型的变量
        int i1 = 1000;
//        int i2 = 10000000000;  // 2147483648
        long l1 = 10000000000L;
//        System.out.println(l1);  // 使用变量，实际上就是使用变量中存储的值

        //定义一个float类型的变量
        float f1 = 12.34F;
        System.out.println(f1);
        //定义一个double类型的变量
        double d1 = 12.34;
        System.out.println(d1);

        char c1 = 'a';
        System.out.println(c1);


        boolean b2 = true;
        System.out.println(b2);

        int i3;
        i3 = 300;
        System.out.println(i3);

//        int a=10;int b = 20;int c= 30;
//        System.out.println(a);
//        System.out.println(b);
//        System.out.println(c);

//        int a=10,int b = 5,int c = 30;
//        int a=20,b=10;
//        System.out.println(a);
//        System.out.println(b);
        int a = 10;
        int b = 20;
        int c = 30;



    }
}
