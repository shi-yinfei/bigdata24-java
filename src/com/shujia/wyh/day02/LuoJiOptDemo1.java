package com.shujia.wyh.day02;

/*
    逻辑运算符：
        & | ^ ! && ||

    注意：逻辑运算符两边放的是boolean类型的值
 */
public class LuoJiOptDemo1 {
    public static void main(String[] args) {
        int a = 3;
        int b = 4;
        int c = 5;

        // & 有false则false
//        System.out.println(a>b & a<c);  //一边是false，整体结果就是false
//        System.out.println(a<b & a>c);
//        System.out.println(a>b & a>c);
//        System.out.println(a<b & a<c);

        // | 有true则true
        //....

        // ^  相同则false，不同则true
        System.out.println(a>b ^ a<c);
        System.out.println(a<b ^ a>c);
        System.out.println(a>b ^ a>c);
        System.out.println(a<b ^ a<c);

        // !  将true变成false，将false变成true
        System.out.println(!(a>b ^ a<c));

    }
}
