package com.shujia.wyh.day02;

import java.util.Scanner;

/*
    我们在此之前的所有案例都是采用先人为的定义死数据，能否可以让用户自己去输入一个值，然后使用变量接收呢？

    java提供了一个类给我们使用：Scanner  键盘录入

    今天只需要记住使用的方式即可，不需要理解为什么。
    1、创建一支笔（创建Scanner类的对象）
    2、使用笔去写字（调用对象中的方法接收数据）
    3、使用写出来的字（使用用户输入的数据）

 */
public class ScannerDemo1 {
    public static void main(String[] args) {
        //首先需要创建一支笔
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入一个数字：");
        //用户输入数据，用变量接收
        int num = sc.nextInt(); //接收一个整数数据的方式
//        String s = sc.next(); //接收一个字符串

        System.out.println("用户输入的数据是："+num);
//        System.out.println("用户输入的数据是："+s);
    }
}
