package com.shujia.wyh.day10;

import java.util.ArrayList;
import java.util.Collection;

/*
    java中Collection集合继承体系：
        Collection: 接口

    接口要想实例化，借助实现类创建对象对象，借助其中实现类：ArrayList来创建对象

    Collection中的成员方法：
        boolean add(E e)
        boolean remove(Object o)
        void clear()
        boolean contains(Object o)
        boolean isEmpty()
        int size()

    ArrayList是Collection一个实现类，所以ArrayList类中必定实现了Collection接口中所有的抽象方法

 */
public class CollectionDemo1 {
    public static void main(String[] args) {
//        Collection collection = new Collection();
        //创建一个Collection集合对象
        Collection c1 = new ArrayList();
        System.out.println(c1);

        //boolean add(Object e)
        //向集合中添加元素
        c1.add("你好");
        c1.add(10); //存储基本数据类型值的时候，内部会做自动装箱，变成对应的包装类型传入
        c1.add(12.34);
        c1.add(120L);
        c1.add(true);
        System.out.println(c1);

        //boolean remove(Object o)
        //移除集合中某个元素
        System.out.println(c1.remove(120L));
        System.out.println(c1);

        //void clear()
//        //清空集合中所有的元素
//        c1.clear();
//        System.out.println(c1);

        //boolean contains(Object o)
        //判断集合中是否包含某一个元素
        System.out.println(c1.contains("你好"));

        //boolean isEmpty()
        System.out.println(c1.isEmpty());

        //int size()
        //获取集合中元素的个数
        System.out.println(c1.size());
        /**
         * 数组获取长度：length属性  数组名.length
         * String获取长度：length()方法 字符串对象名.length()
         * Collection集合获取长度：size()方法  集合对象名.size()
         */




    }
}
