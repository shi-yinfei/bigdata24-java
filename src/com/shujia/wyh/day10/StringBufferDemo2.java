package com.shujia.wyh.day10;

/*
    public StringBuffer append(String str)
    public StringBuffer insert(int offset,String str)

 */
public class StringBufferDemo2 {
    public static void main(String[] args) {
        //创建一个空的StringBuffer对象
        StringBuffer sb = new StringBuffer();

        //public StringBuffer append(String str)  返回的是自身
        //通过观察api发现，append方法在StringBuffer类中发生重载
        //不仅可以添加字符串，还可以添加任意数据类型的数据
        //注意：无论添加的时候是什么数据类型，一旦进入到StringBuffer中，就变成了字符类型
        sb.append(true);
        sb.append(12.34);
        sb.append("你好");
        System.out.println(sb);

        //true12.34你好
        //public StringBuffer insert(int index,String str)
        //指定索引位置添加字符串
        sb.insert(6,"王宇杰");
        System.out.println(sb);

    }
}
