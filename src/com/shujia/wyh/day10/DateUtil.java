package com.shujia.wyh.day10;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    //构造方法私有化
    private DateUtil(){}

    //将时间戳转化成标准日期输出
    public static String formatDate(Long timestamp){
        Date date = new Date(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
}
