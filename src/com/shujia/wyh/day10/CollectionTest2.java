package com.shujia.wyh.day10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/*
    存储自定义对象并遍历
        Student(name,age)

 */
public class CollectionTest2 {
    public static void main(String[] args) {
        //1、创建Collection集合对象
        Collection c1 = new ArrayList();

        //2、创建元素学生对象
        Student s1 = new Student("王宇杰", 18);
        Student s2 = new Student("陆澳", 17);
        Student s3 = new Student("杨志", 19);
        Student s4 = new Student("张怀远", 16);

        //3、向集合中添加元素
        c1.add(s1);
        c1.add(s2);
        c1.add(s3);
        c1.add(s4);

        //4、遍历集合
        Iterator iterator = c1.iterator();
        while (iterator.hasNext()) {
            Object next = iterator.next();
            //向下转型
            Student s = (Student) next;
            System.out.println(s.getName() + "---" + s.getAge());
        }
    }
}
