package com.shujia.wyh.day10;

/*
    看程序写结果：
    String作为参数传递
    StringBuffer作为参数传递

 */
public class StringBufferDemo7 {
    public static void main(String[] args) {
//        String s1 = "hello";
//        String s2 = "world";
//        System.out.println("s1: " + s1 + " , s2: " + s2); // s1:hello   s2: world
//        fun(s1, s2);
//        System.out.println("s1: " + s1 + " , s2: " + s2); //s1: hello  s2:world


        StringBuffer sb1 = new StringBuffer("hello");
        StringBuffer sb2 = new StringBuffer("world");
        System.out.println("sb1: " + sb1 + " , sb2: " + sb2);  // sb1: hello sb2:world
        fun(sb1,sb2);
        System.out.println("sb1: " + sb1 + " , sb2: " + sb2);  //sb1: hello sb2: worldworld

    }

    public static void fun(StringBuffer sb1,StringBuffer sb2){
        sb1 = sb2;
        sb2 = sb1.append(sb2);
        System.out.println("sb1: " + sb1 + " , sb2: " + sb2); //sb1:worldworld  sb2:worldworld
    }




    public static void fun(String s1, String s2) {
        s1 = s2;
        s2 = s1 + s2;
        System.out.println("s1: " + s1 + " , s2: " + s2);  // s1: world s2: worldworld
    }
}
