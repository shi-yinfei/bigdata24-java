package com.shujia.wyh.day10;

/*
    public String substring(int start)
    public String substring(int start,int end)

 */
public class StringBufferDemo6 {
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("abcdefgh");

        //public String substring(int start)
        String s = sb.substring(4);
        System.out.println("截取出来的字符串：" + s);
        System.out.println(sb);
        System.out.println("---------------------------");
        //public String substring(int start,int end)
        String s1 = sb.substring(2, 6);
        System.out.println("截取出来的字符串：" + s1);
        System.out.println(sb);

    }
}
