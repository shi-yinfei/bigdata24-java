package com.shujia.wyh.day10;

/*
    public StringBuffer deleteCharAt(int index)
    public StringBuffer delete(int start,int end)

 */
public class StringBufferDemo3 {
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("shujia666王宇杰和张怀远");

        //public StringBuffer deleteCharAt(int index)
        //指定索引删除字符
//        sb.deleteCharAt(5);
//        System.out.println(sb);

        //public StringBuffer delete(int start,int end)
        //删除一部分字符
        sb.delete(0,9);  //[start,end)
        System.out.println(sb);

    }
}
