package com.shujia.wyh.day10;

/*
把数组拼接成一个字符串
 */
public class StringBufferTest2 {
    public static void main(String[] args) {
        int[] arr = {11,22,33,44,55};
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<arr.length;i++){
            sb.append(arr[i]);
        }

        String s1 = sb.toString();
        System.out.println(s1);
    }
}
