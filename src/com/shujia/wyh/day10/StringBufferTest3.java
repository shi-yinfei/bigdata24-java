package com.shujia.wyh.day10;

/*
    把字符串反转

 */
public class StringBufferTest3 {
    public static void main(String[] args) {
        String s = "我爱莫提冯";

        //String-->StringBuffer
        StringBuffer sb = new StringBuffer(s);

        sb.reverse();

        String s1 = sb.toString();
        System.out.println("反转后：" + s1);
    }
}
