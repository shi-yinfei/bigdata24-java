package com.shujia.wyh.day10;

/*
    判断一个字符串是否是对称字符串
    例如"abc"不是对称字符串，"aba"、"abba"、"aaa"、"mnanm"是对称字符串

 */
public class StringBufferTest4 {
    public static void main(String[] args) {
        String s = "abbbb";

        //String-->StringBuffer
        StringBuffer sb = new StringBuffer(s);

        sb.reverse();

        String s1 = sb.toString();
        System.out.println("反转后：" + s1);

        if(s1.equals(s)){
            System.out.println("是对称字符串");
        }else {
            System.out.println("不是对称字符串");
        }
    }
}
