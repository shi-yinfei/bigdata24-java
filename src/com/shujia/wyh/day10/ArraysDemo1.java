package com.shujia.wyh.day10;
import java.util.Arrays;

/*
    Arrays：java提供的专门操作数组的工具类

    静态成员方法：
        public static String toString(int[] a)
        public static void sort(int[] a)
        public static int binarySearch(int[] a,int key)



 */
public class ArraysDemo1 {
    public static void main(String[] args) {
        int[] arr = {11,22,33,44,55};
//        System.out.println(Arrays.toString(arr));
        System.out.println("--------------------------------");
        //public static void sort(int[] a)
        int[] arr2 = {23,41,5,74,2,34,56};
        System.out.println("排序前："+Arrays.toString(arr2));
        Arrays.sort(arr2); //底层是快速排序
        System.out.println("排序后："+Arrays.toString(arr2));
        System.out.println("---------------------------------");
        //public static int binarySearch(int[] a,int key)  二分查找
        //前提：保证序列是有序 返回的是查找到元素的索引
        //排序后：[2, 5, 23, 34, 41, 56, 74]
        System.out.println(Arrays.binarySearch(arr2,100));  //-8
    }
}
