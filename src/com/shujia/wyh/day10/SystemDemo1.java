package com.shujia.wyh.day10;

/*
System:
    public static void gc()
    public static void exit(int status)
    public static long currentTimeMillis()

 */
public class SystemDemo1 {
    public static void main(String[] args) {
//        System.gc();  //手动的开启垃圾回收 立刻

//        for(int i=0;i<100;i++){
//            if(i==5){
//                System.exit(0);  //表示程序停止 强制停止一个程序
//            }
//
//            System.out.println(i);
//        }
//
//        System.out.println("你好");

        // public static long currentTimeMillis()
        System.out.println(System.currentTimeMillis());  //1683353919341 毫秒级别

    }
}
