package com.shujia.wyh.day10;

import java.util.ArrayList;
import java.util.Collection;

/*
    boolean addAll(Collection c)
    boolean removeAll(Collection c)
    boolean containsAll(Collection c)
    boolean retainAll(Collection c)

 */
public class CollectionDemo2 {
    public static void main(String[] args) {
        //创建Collection集合对象
        Collection c1 = new ArrayList();
        c1.add("java");
        c1.add("hadoop");
        c1.add("hive");
        c1.add("hbase");
        c1.add("maven");
        c1.add("redis");


        //创建另一个集合对象
        Collection c2 = new ArrayList();
        c2.add("maven");
        c2.add("git");
        c2.add("redis");
        c2.add("flume");
        System.out.println("c1: " + c1);
        System.out.println("c2: " + c2);
        System.out.println("-----------------------------");


//        //boolean addAll(Collection c)  向集合中添加另一个集合的所有元素
//        c1.addAll(c2);
//        System.out.println(c1);

        //boolean removeAll(Collection c) 向集合中删除另一个集合的所有元素
//        c1.removeAll(c2);
//        System.out.println(c1);

        //boolean containsAll(Collection c)  判断一个Collection集合是否包含另一个集合的所有元素
//        System.out.println(c1.containsAll(c2));

        //boolean retainAll(Collection c) 取交集，将交集的结果存储到调用方法的集合中,另一个集合元素不变
//        c1.retainAll(c2);
        c2.retainAll(c1);
        System.out.println("c1: " + c1);
        System.out.println("c2: " + c2);


    }
}
