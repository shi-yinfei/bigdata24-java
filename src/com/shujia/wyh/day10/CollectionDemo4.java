package com.shujia.wyh.day10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/*
    Collection集合的遍历方式2：迭代器遍历
        Iterator iterator() 迭代器，Collection集合的专用遍历方式


 */
public class CollectionDemo4 {
    public static void main(String[] args) {
        //创建一个集合对象
        Collection c1 = new ArrayList();

        //向集合中添加元素
        c1.add("hello");
        c1.add("world");
        c1.add("hadoop");
        c1.add("hive");
        c1.add("java");

        //遍历集合
        //Iterator iterator() 迭代器，Collection集合的专用遍历方式
        //Collection集合中有一个方法：iterator()返回的是一个包含集合中所有元素的迭代器
        //将来我们遍历迭代器就可以获取每一个集合元素
        //迭代器是Collection集合特有的遍历方式
        Iterator iterator = c1.iterator(); //  Iterator iterator = new Itr()
//        System.out.println(iterator.next());
//        System.out.println(iterator.next());
//        System.out.println(iterator.next());
//        System.out.println(iterator.next());
//        System.out.println(iterator.next());
//        System.out.println(iterator.next());

        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }


    }
}
