package com.shujia.wyh.day10;

import java.util.Random;

/*
    java提供了一个专门用于生成随机数的类：Random

 */
public class RandomDemo1 {
    public static void main(String[] args) {
        //创建一个Random随机数对象
        Random random = new Random();
        System.out.println(random.nextInt(100)+1);  // 1-100
    }
}
