package com.shujia.wyh.day10;

/*
    public StringBuffer reverse()
 */
public class StringBufferDemo5 {
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("我爱莫提冯");

        sb.reverse();
        System.out.println(sb);
    }
}
