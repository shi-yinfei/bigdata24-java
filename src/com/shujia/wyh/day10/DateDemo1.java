package com.shujia.wyh.day10;


import java.text.SimpleDateFormat;
import java.util.Date;

/*
    Date日期类和SimpleDateFormat类

    Date类：java中专门提供给用户处理日期的一个类
    构造方法：
        Date() 分配一个 Date对象，并初始化它，以便它代表它被分配的时间，测量到最近的毫秒。
        Date(long date) 分配一个 Date对象，并将其初始化为表示自称为“时代”的标准基准时间以后的指定毫秒数，即1970年1月1日00:00:00 GMT。

    java提供了另外一个类可以格式化日期：SimpleDateformat
        public SimpleDateFormat(String pattern)


 */
public class DateDemo1 {
    public static void main(String[] args) {
//        Date date = new Date();
//        System.out.println(date);

        //Date(long date)
//        Date date = new Date(1683353919341L);
//        System.out.println(date);

        //public SimpleDateFormat(String pattern)
        //传入要转化后的格式：xxxx年xx月xx日 xx时xx分xx秒
        //yyyy 年
        //MM 月
        //dd 日
        //HH 24小时制度
        //mm 分钟
        //ss秒钟
//        SimpleDateFormat sdf = new SimpleDateFormat("当前的时间是： yyyy-MM-dd HH:mm:ss a");
//        String s = sdf.format(date);
//        System.out.println(s);

        System.out.println("当前的时间是：" + DateUtil.formatDate(System.currentTimeMillis()));


    }
}
