package com.shujia.wyh.day10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/*
    向集合中添加字符串元素，然后遍历集合，如果遇到了java,向集合中添加shujia

    我们按照思路编写代码，获取迭代器，遍历迭代器的时候，使用集合添加元素报错了
    ConcurrentModificationException 并发修改异常
    解决方案：迭代器遍历，迭代器添加元素，集合遍历，集合添加元素


 */
public class CollectionTest3 {
    public static void main(String[] args) {
        //1、创建集合对象
        Collection c1 = new ArrayList();

        //2、向集合添加元素
        c1.add("hello");
        c1.add("world");
        c1.add("java");
        c1.add("hadoop");
        c1.add("hive");
        c1.add("spark");

        //3、遍历
        //对于目前这个需求而言，Iterator迭代器无法完成，因为Iterator并没有添加元素的方法
//        Iterator iterator = c1.iterator();
//        while (iterator.hasNext()){
//            Object next = iterator.next();
//            //向下转型
//            String s = (String) next;
//            if("java".equals(s)){
////                c1.add("shujia");
//            }
//        }

        Object[] array = c1.toArray();
        for(int i=0;i<array.length;i++){
            String s = (String) array[i];
            if("java".equals(array[i])){
                c1.add("shujia");
            }
        }

        System.out.println(c1);

    }
}
