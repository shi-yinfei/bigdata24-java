package com.shujia.wyh.day10;

/*
    StringBuffer：可以伸缩变化的字符串容器
    线程安全，可变的字符序列。 字符串缓冲区就像一个String ，但可以修改。
    在任何时间点，它包含一些特定的字符序列，但可以通过某些方法调用来更改序列的长度和内容。

    构造方法：
        public StringBuffer()
        public StringBuffer(int capacity)
        public StringBuffer(String str)

 */
public class StringBufferDemo1 {
    public static void main(String[] args) {
        //public StringBuffer()
        StringBuffer sb1 = new StringBuffer();
        System.out.println("sb1:" + sb1);
        //获取容器容量大小
        System.out.println(sb1.capacity()); // 默认一开始的容量大小为16字符大小
        //获取实际存储的字符长度
        System.out.println(sb1.length());
        System.out.println("=========================================================");
        //public StringBuffer(int capacity)
        StringBuffer sb2 = new StringBuffer(50);
        //获取容器容量大小
        System.out.println(sb2.capacity()); // 默认一开始的容量大小为16字符大小
        //获取实际存储的字符长度
        System.out.println(sb2.length());
        System.out.println("=========================================================");
        //public StringBuffer(String str)
        StringBuffer sb3 = new StringBuffer("hello");
        System.out.println(sb3.capacity());
        System.out.println(sb3.length());


    }
}
