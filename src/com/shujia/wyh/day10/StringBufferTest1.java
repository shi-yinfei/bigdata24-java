package com.shujia.wyh.day10;

/*
    String和StringBuffer的相互转换
 */
public class StringBufferTest1 {
    public static void main(String[] args) {
        //String-->StringBuffer
        String s = "abcef";
        StringBuffer sb = new StringBuffer(s);


        //StringBuffer-->String
        String s1 = sb.toString();
    }
}
