package com.shujia.wyh.day10;

/*
    java考虑到基本数据类型的值无法通过调用方法来处理，于是就针对每一个基本数据类型都提供了一个对应的引用数据类型（包装类类型）
    byte    ----  Byte
    short   ----  Short
    int     ----  Integer
    long    ----  Long
    float   ----  Float
    double  ----  Double
    boolean ----  Boolean
    char    ----  Character

    构造方法：
        Integer(int value)
        构造一个新分配的 Integer对象，该对象表示指定的 int值。
        Integer(String s)
        构造一个新分配 Integer对象，表示 int由指示值 String参数。



 */
public class BaoZhuangDemo1 {
    public static void main(String[] args) {
//        int a = 10;
//        a+=1;

//        Integer integer = new Integer(10);
//        System.out.println(integer);

//        Integer integer = 10;  //自动装箱
//        System.out.println(integer);
//
//        System.out.println(integer+10); //自动拆箱

        //Integer(String s) 构造一个新分配 Integer对象，表示 int由指示值 String参数。
//        Integer integer = new Integer("10");  //NumberFormatException
//        System.out.println(integer);

        //public int intValue()  自动拆箱
//        System.out.println(integer.intValue());

        //public static int parseInt(String s)
//        int i = Integer.parseInt("100");
//        System.out.println(i);

        //public static String toString(int i)
//        String s1 = Integer.toString(100);
//        System.out.println(s1);

        //public static Integer valueOf(int i)
//        Integer integer = Integer.valueOf(100);
//        System.out.println(integer);
//
//        //public static Integer valueOf(String s)
//        Integer integer1 = Integer.valueOf("100");
//        System.out.println(integer1);


        //public static String toBinaryString(int i)
//        System.out.println(Integer.toBinaryString(100));

        //public static String toOctalString(int i)
        //public static String toHexString(int i)

        //public static boolean isDigit(char ch)
        //char类型的包装类类型Character
        System.out.println(Character.isDigit('9')); //判断某一个字符是否是数字


    }
}
