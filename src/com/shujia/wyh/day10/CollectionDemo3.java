package com.shujia.wyh.day10;

import java.util.ArrayList;
import java.util.Collection;

/*
    Collection集合的遍历方式1：
        Object[] toArray() 把集合转成数组，可以实现集合的遍历

    学习集合的步骤：
        1、创建对应的集合对象
        2、创建元素对象
        3、向集合中添加元素
        4、遍历集合得到元素

 */
public class CollectionDemo3 {
    public static void main(String[] args) {
        //创建一个集合对象
        Collection c1 = new ArrayList();

        //向集合中添加元素
        c1.add("java");
        c1.add("hello");
        c1.add("world");
        c1.add("hadoop");

        //遍历集合
        //Object[] toArray() 把集合转成数组，可以实现集合的遍历
        Object[] array = c1.toArray();

        //遍历得到每一个元素的同时，获取每一个元素的长度
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
            //向下转型
            String s = (String) array[i];
            System.out.println(s.length());
        }


    }
}
