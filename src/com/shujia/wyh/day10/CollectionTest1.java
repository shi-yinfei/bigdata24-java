package com.shujia.wyh.day10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/*
    Collection集合存储字符串并遍历
 */
public class CollectionTest1 {
    public static void main(String[] args) {
        //1、创建Collection集合对象
        Collection c1 = new ArrayList();

        //2、创建字符串对象添加到集合中
        c1.add("hello");
        c1.add("world");
        c1.add("java");
        c1.add("hadoop");
        c1.add("hive");

        //3、遍历集合
        //方式1：转数组遍历
//        Object[] array = c1.toArray();
//        for(int i=0;i<array.length;i++){
//            System.out.println(array[i]);
//            //向下转型
//            String s = (String) array[i];
//            System.out.println(s.length());
//        }

        //方式2：迭代器遍历
        Iterator iterator = c1.iterator();
        while (iterator.hasNext()){
            Object next = iterator.next();
            System.out.println(next);
            //向下转型
            String s = (String) next;
            System.out.println(s.length());
        }
    }
}
