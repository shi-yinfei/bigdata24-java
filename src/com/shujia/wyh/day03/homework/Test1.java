package com.shujia.wyh.day03.homework;
import java.util.Scanner;

/*
         输入自己的名字，年龄和性别，分别用不同的变量接收，并将输入的信息做输 出。
 */
public class Test1 {
    public static void main(String[] args) {
        //创建Scanner对象
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入您的姓名：");
        String name = sc.next();
        System.out.println("请输入您的年龄");
        int age = sc.nextInt();
        System.out.println("请输入您的性别：");
        String gender = sc.next();

        System.out.println("您的姓名是："+name);
        System.out.println("您的年龄是："+age);
        System.out.println("您的性别是："+gender);

    }
}
