package com.shujia.wyh.day03.homework;

/*
    某个公司采用公用电话传递数据，数据是四位的整数，在传递过程中是加密的，
    加密规则如下：
        每位数字都加上 5,然后用和除以 10 的余数代替该数字，再将第一位和第四位交换，第二位和第三位交换。结果如图所示。

    输入：1357
    输出：2086
 */
public class Test2 {
    public static void main(String[] args) {
        int number = 1357;
        System.out.println("加密前：" + number);

        int qianWei = number / 1000;
        int baiWei = number % 1000 / 100;
        int shiWei = number % 1000 % 100 / 10;
        int geWei = number % 10;
//        System.out.println(qianWei);
//        System.out.println(baiWei);
//        System.out.println(shiWei);
//        System.out.println(geWei);

        //每位数字都加上 5,然后用和除以 10 的余数代替该数字
        qianWei = (qianWei + 5) % 10;
        baiWei = (baiWei + 5) % 10;
        shiWei = (shiWei + 5) % 10;
        geWei = (geWei + 5) % 10;

        //交换 再将第一位和第四位交换
        int temp1 = qianWei;
        qianWei = geWei;
        geWei = temp1;

        //第二位和第三位交换
        int temp2 = baiWei;
        baiWei = shiWei;
        shiWei = temp2;

        int number2 = qianWei * 1000 + baiWei * 100 + shiWei * 10 + geWei;
        System.out.println("加密后：" + number2);


    }
}
