package com.shujia.wyh.day03.ketang;

/*
    看程序写结果
 */
public class SwitchDemo3 {
    public static void main(String[] args) {
        int x = 2;
        int y = 3;
        switch(x){
            default:
                y++;  // 4
            case 3:
                y++;  // 5
            case 4:
                y++;  // 6
        }
        System.out.println("y="+y); // 6

    }
}
