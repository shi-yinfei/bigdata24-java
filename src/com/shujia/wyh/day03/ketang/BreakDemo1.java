package com.shujia.wyh.day03.ketang;

/*
    break: 打破，结束
        在选择结构switch语句中
        在循环语句中

 */
public class BreakDemo1 {
    public static void main(String[] args) {
        //在 switch 或 loop 外部中断
        //单独使用没有任何意义，需要有场景中使用
//        break;

//        int a = 10;
//        if(a==10){ //break关键字不能单独作用于if语句
//            break;
//        }

        //当i是5的时候，使用break
//        for (int i = 1; i <= 10; i++) {
//            if(i==5){
//                break; // 结束整个for循环
//            }
//            System.out.println(i);
//        }

        //在控制台输出九九乘法表。
        //当j==5的时候，使用break
        wc:for (int i = 1; i <= 9; i++) {
            nc:for (int j = 1; j <= i; j++) {
                if(j==5){
                    break wc; //结束的是最近的循环
                }
                if (j != i) {
                    System.out.print(i + "*" + j + "=" + (i * j) + "\t");
                } else {
                    System.out.println(i + "*" + j + "=" + (i * j));
                }
            }
        }

    }
}
