package com.shujia.wyh.day03.ketang;

/*
    循环结构第二种：while循环
    语句定义格式：
        初始化条件语句;
        while(判断条件表达式){
            循环体语句;
            控制条件语句;
        }

    while循环和普通for循环可以等价转换的。
 */
public class WhileDemo1 {
    public static void main(String[] args) {
        //for循环能否和while循环等价转换呢？
        //需求：打印10个Hello World
//        for(int i=1;i<=10;i++){
//            System.out.println("Hello World");
//        }

        System.out.println("-----------------------------------------");
        int i=1;
        while (i<=10){
            System.out.println("Hello World");
            i++;
        }
    }
}
