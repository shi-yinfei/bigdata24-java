package com.shujia.wyh.day03.ketang;

/*
    while循环语句格式2：do...while循环
    语句定义格式：
        初始化条件语句;
        do{
            循环体语句;
            控制条件语句;
        }while(判断条件语句);

    while循环和do...while循环的区别：
        do...while循环会先执行一次循环体内容，然后再判断是否符合循环条件；
        而while循环是先判断是否符合循环条件，当符合后才会执行循环体内容。
 */
public class DoWhileDemo1 {
    public static void main(String[] args) {
//        for (int i = 1; i <= 10; i++) {
//            System.out.println("Hello World");
//        }
        System.out.println("----------------------------------------------");
        int i = 11;
        do{
            System.out.println("Hello World");
            i++;
        }while (i <= 10);

        System.out.println("-----------------------------------------");
        int i2=11;
        while (i2<=10){
            System.out.println("Hello World");
            i2++;
        }
    }
}
