package com.shujia.wyh.day03.ketang;

/*
    方法：完成特定功能的代码块

    语句定义格式：
        修饰符 返回值类型 方法名(参数数据类型 参数名,参数数据类型 参数名,...){
                方法体;
                return 返回值;
        }

    名字解释：
        1、修饰符
            今天只需要记住一个固定写法，public static
        2、返回值
            调用者调用完方法后获取到的结果
        3、返回值类型
            调用者调用完方法后获取到的结果的数据类型
        4、方法名
            符合标识符命名规则即可
        5、参数
            形参：定义方法时，给定的参数，用于接收调用时传入的值
            实参：调用时传入的实际的值
        6、形参数据类型
            规定了调用方法时所传入的值的类型
        7、形参名
            符合标识符命名规则即可，其实就是局部变量
        8、方法体
            实现方法对应的功能的代码块
        9、return
            将返回值返回给调用者


     注意事项：
        1、方法与方法之间是平级关系，不能嵌套定义
        2、如果一个方法有具体的返回值，在调用完毕之后，要么使用对应类型的变量接收返回值，要么直接使用
        3、方法不调用不执行
        4、调用方法时，传入的是具体的值或者变量，不需要加上数据类型

 */
public class FunctionDemo1 {
    public static void main(String[] args) {
//        //需求：求两个数之和
        int a = 10;
        int b = 20;

//        int sumNumber = sum(a,b);
//        System.out.println(sumNumber);

//        System.out.println(sum(10,20));

        System.out.println(sum(10,20));

    }

    public static int sum(int x,int y){
        return x+y;
    }

}
