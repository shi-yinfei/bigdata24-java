package com.shujia.wyh.day03.ketang;

/*
    循环：重复做一件事情
    java中循环的分类：
        1、for循环
        2、while循环

    for循环语句定义格式：
        for(初始化条件语句;判断条件语句;控制条件语句){
            循环体;
        }

    使用for循环的注意事项：
        1、初始化条件语句只会在最开始的时候先执行一次，后面都不会执行了
        2、初始化条件语句能否定义在for循环外部呢？如果可以，与定义在内部有什么区别呢？
        3、控制条件语句能否不写呢？可以不写，但是可能会造成死循环
        4、判断条件语句能否不写？可以不写，但是可能会造成死循环
        5、大括号省略了，只会针对第一行语句生效，建议永远不要省略大括号

    for循环版本的死循环：
        for(;;){
            ...
        }

 */
//需求：在控制台中输出10行hello world
public class ForDemo1 {
    public static void main(String[] args) {
//        System.out.println("hello world");
//        System.out.println("hello world");
//        System.out.println("hello world");
//        System.out.println("hello world");
//        System.out.println("hello world");
//        System.out.println("hello world");
//        System.out.println("hello world");
//        System.out.println("hello world");
//        System.out.println("hello world");
//        System.out.println("hello world");

        //1、打印10次，循环10次 可以定义一个变量 i=1  初始化条件语句
        //2、每次循环变量i的值+1，表示已经经历过一次循环了 控制条件语句
        //3、当i<=10的时候，进入循环 判断条件语句
        //4、循环体   System.out.println("hello world");
//        int i=1;
        for(int i=1;i<=10;i++) {
            System.out.println("hello world" + i);
            System.out.println("王庆真的也很帅！");
        }


//        System.out.println(i);

    }
}
