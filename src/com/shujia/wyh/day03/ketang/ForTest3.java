package com.shujia.wyh.day03.ketang;

//需求：请输出一个4行5列的星星(*)图案。
public class ForTest3 {
    public static void main(String[] args) {
        /*
         *   *   *   *   *
         *   *   *   *   *
         *   *   *   *   *
         *   *   *   *   *
         */
//        System.out.println("*");
//        System.out.println("*");
//        System.out.println("*");
//        System.out.println("*");
//        System.out.println("*");
        //我们使用默认常用的输出语句的时候，在输出完内容后会进行换行，如果想要不换行，就需要使用另外一种输出语句
        //System.out.print()
//        System.out.print("*"+"\t");
//        System.out.print("*"+"\t");
//        System.out.print("*"+"\t");
//        System.out.print("*"+"\t");
//        System.out.print("*"+"\t");

        //使用for循环改进打印一行5个星
//        for(int i=1;i<=5;i++){
//            if(i!=5){
//                System.out.print("*"+"\t");
//            }else {
//                System.out.println("*");
//            }
//        }
//
//        for(int i=1;i<=5;i++){
//            if(i!=5){
//                System.out.print("*"+"\t");
//            }else {
//                System.out.println("*");
//            }
//        }
//
//        for(int i=1;i<=5;i++){
//            if(i!=5){
//                System.out.print("*"+"\t");
//            }else {
//                System.out.println("*");
//            }
//        }
//
//        for(int i=1;i<=5;i++){
//            if(i!=5){
//                System.out.print("*"+"\t");
//            }else {
//                System.out.println("*");
//            }
//        }

        //使用for循环改进打印4行星
        for (int j = 1; j <= 4; j++) {   // 外层for循环控制的行
            for (int i = 1; i <= 5; i++) { //内层for循环控制的列
                if (i != 5) {
                    System.out.print("*" + "\t");
                } else {
                    System.out.println("*");
                }
            }
        }

        System.out.println("==========================================");
        /*
            请输出如下图形
            *                     第1行，共1列
            *   *                 第2行，共2列
            *   *   *             第3行，共3列
            *   *   *   *         第4行，共4列
            *   *   *   *   *     第5行，共5列
         */
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                if (j != i) {
                    System.out.print("*" + "\t");
                } else {
                    System.out.println("*");
                }
            }
        }

        System.out.println("====================================");
        //在控制台输出九九乘法表。
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                if (j != i) {
                    System.out.print(i + "*" + j + "=" + (i * j) + "\t");
                } else {
                    System.out.println(i + "*" + j + "=" + (i * j));
                }
            }
        }

    }
}
