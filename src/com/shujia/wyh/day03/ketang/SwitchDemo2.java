package com.shujia.wyh.day03.ketang;

/*
    看程序写结果1
 */
public class SwitchDemo2 {
    public static void main(String[] args) {
        int x = 2;
        int y = 3;
        switch(x){
            default:
                y++;
                break;
            case 3:
                y++;
            case 4:
                y++;
        }
        System.out.println("y="+y); // 4
    }
}
