package com.shujia.wyh.day03.ketang;

/*
    小芳的妈妈每天给她2.5元钱，
    她都会存起来，但是，每当这一天是存钱的第5天或者5的倍数的话，她都会花去6元钱，
    请问，经过多少天，小芳才可以存到100元钱。

 */
public class BreakTest {
    public static void main(String[] args) {
        //定一个变量表示存的金额
        double money = 0.0;
        //定义一个变量表示天数
        int day = 0;

        while (money < 100) {
            money += 2.5;
            day++;
            if (day % 5 == 0) {
                money -= 6;
            }
        }
        System.out.println("经过了 " + day + " 天，小芳的存款为：" + money);
    }
}
