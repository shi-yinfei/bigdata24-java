package com.shujia.wyh.day03.ketang;

/*
    return: 返回，用于结束整个方法
 */
public class ReturnDemo1 {
    public static void main(String[] args) {

//        for(int i=1;i<=10;i++){
//            if(i==5){
//                break;
//            }
//            System.out.println(i);
//        }
//
//        System.out.println("王宇杰真帅！！");

        System.out.println("--------------------------------------");

        for(int i=1;i<=10;i++){
            if(i==5){
                return;
            }
            System.out.println(i);
        }

        System.out.println("王宇杰真帅！！");

    }
}
