package com.shujia.wyh.day03.ketang;

import java.util.Scanner;

//键盘录入行数和列数，输出对应的星形
public class FunctionTest2 {
    public static void main(String[] args) {
        //创建Scanner对象
        Scanner sc = new Scanner(System.in);

        //定义变量：见名知意
        System.out.println("请输入行数：");
        int rowNum = sc.nextInt();
        System.out.println("请输入列数：");
        int colNum = sc.nextInt();

        printJiuJiu(rowNum,colNum);


    }

    /**
     * 确定返回值类型：void
     * 确定参数列表：int row,int col
     */
    public static void printJiuJiu(int row, int col) {
        for (int i = 1; i <= row; i++) {
            for (int j = 1; j <= col; j++) {
                if (j != col) {
                    System.out.print("*" + "\t");
                } else {
                    System.out.println("*");
                }
            }
        }
    }
}
