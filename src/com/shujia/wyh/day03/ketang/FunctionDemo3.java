package com.shujia.wyh.day03.ketang;

/*
    在一个类中，如果出现多个方法，方法的名字相同，只是参数列表（参数的数据类型，参数的个数）不同的现象，称之为方法的重载,
    重载只看重方法名和参数列表，只要他们两个一样，就说明同一个方法，但是在同一个类中不能定义相同的方法。和返回值类型无关。
    调用方法的时候，注意类型的自动转换。
 */
public class FunctionDemo3 {
    public static void main(String[] args) {
        //定义一个方法，求两个数之和
//        int a = 10;
//        int b = 20;
////
//        int sumNumber = sum(a,b);
//        System.out.println(sumNumber);

        //定义一个方法，求三个数之和
//        int c = 30;
//        int sumNumber = sum2(a, b, c);
//        System.out.println(sumNumber);

        //定义一个方法，求四个数之和
//        int d = 40;
//        int sumNumber = sum(a, b, c, d);
//        System.out.println(sumNumber);

//        int a = 10;
//        byte b = 20;
//        int sumNumber = sum(a,b);
//        System.out.println(sumNumber);

        //定义一个方法，方法名依旧是叫做sum,求两个数之和，直接在方法中输出，不返回
        //该方法能不能被定义出来？


    }

    public static int sum(int x, int y, int z, int w) {
        return x + y + z + w;
    }

    public static int sum(int x, int y, int z) {
        return x + y + z;
    }

    public static int sum(int x, int y) {
        return x + y;
    }

//    public static void sum(int x, int y){
//        System.out.println(x+y);
//    }
}
