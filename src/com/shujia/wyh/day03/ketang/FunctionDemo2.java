package com.shujia.wyh.day03.ketang;

/*
    在现实生活中，优先行为是没有相对应的返回值的，那么在java中该如何定义一个没有返回值的方法呢？
    一个方法如果没有返回值，java提供了一个关键字给我们使用，这个关键字代表的是没有返回值的意思：void
    没有返回值的方法，可以不写return
 */
public class FunctionDemo2 {
    public static void main(String[] args) {
        //打印九九乘法表
//        for (int i = 1; i <= 9; i++) {
//            for (int j = 1; j <= i; j++) {
//                if (j != i) {
//                    System.out.print(i + "*" + j + "=" + (i * j) + "\t");
//                } else {
//                    System.out.println(i + "*" + j + "=" + (i * j));
//                }
//            }
//        }
        // 如果一个方法在定义的时候没有返回值，返回值类型是void，直接调用即可，不需要使用变量接收
        printJiuJiu();
        System.out.println("===============================");
        printJiuJiu();
    }

    /**
     * 定义一个方法所需要知道的东西：
     * 返回值类型：
     * 参数列表：
     */
    public static void printJiuJiu(){
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                if (j != i) {
                    System.out.print(i + "*" + j + "=" + (i * j) + "\t");
                } else {
                    System.out.println(i + "*" + j + "=" + (i * j));
                }
            }
        }
    }
}
