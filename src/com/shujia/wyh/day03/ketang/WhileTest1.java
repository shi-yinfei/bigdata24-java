package com.shujia.wyh.day03.ketang;

/*
    我国最高山峰是珠穆朗玛峰：8848m，我现在有一张足够大的纸张，厚度为：0.01m。请问，我折叠多少次，就可以保证厚度不低于珠穆朗玛峰的高度?
    初始化条件：
        high = 884800;
        thickness = 1;
        count = 0;
 */
public class WhileTest1 {
    public static void main(String[] args) {
        int high = 884800;
        int thickness = 1;
        int count = 0;
        while (thickness < high) {
            thickness *= 2;
            count++;
        }

        System.out.println("共折叠：" + count + "次，厚度为：" + thickness);
    }
}
