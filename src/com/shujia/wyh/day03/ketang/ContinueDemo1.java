package com.shujia.wyh.day03.ketang;

/*
    continue: 继续，跳过当次循环，继续下一次循环
    在循环语句中

 */
public class ContinueDemo1 {
    public static void main(String[] args) {
//        continue;  //

        //当i==5的时候，使用continue
        for (int i = 1; i <= 10; i++) {
            if(i==5){
                continue;
            }
            System.out.println(i);
        }
    }
}
