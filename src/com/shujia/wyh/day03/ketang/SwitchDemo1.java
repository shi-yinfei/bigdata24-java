package com.shujia.wyh.day03.ketang;

import java.util.Scanner;

/*
    java选择语句：switch
    语句定义格式：
        switch(表达式){
            case 常量值1:
                语句体1;
                break;
            case 常量值2:
                语句体2;
                break;
            ...
            case 常量值n:
                 语句体n;
                 break;
            default:
                 语句体n+1;
                 break;
        }

    switch语句的执行流程：
        1、先计算表达式中的值，注意，这里的值类型只能是byte,short,int,char,枚举,String
        2、拿着计算出来的值自上而下去匹配对应的case后面的常量值
        3、如果有对应匹配上的常量值的话，就执行对应case后面的语句体内容，最后直到运行到break,结束整个siwtch语句
        4、如果没有对应case进行匹配，那么就会执行默认default后面的语句体内容，直到运行到break,结束整个siwtch语句

    注意事项：
        1、case中break能不能不写？能，但是结果可能不是你想要的。会造成switch穿透
        2、default能不能不写？能，可能会导致程序不严谨
        3、default语句能不不写在末尾呢？可以，switch语句会严格遵循先执行case匹配，再进行default语句的执行，和default语句位置没有关系
        4、case后面只能是常量不能是变量
        5、case 之间的常量值不允许重复

 */
public class SwitchDemo1 {
    public static void main(String[] args) {
        //创建键盘录入对象
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入您带的金额：");
        int money = sc.nextInt();


        //前提：假设每次带的金额正好购买一瓶饮料的钱
        //可乐：3   旺仔：5   红牛：6   农夫山泉：2   冰露：1  雪碧：3
        switch (money) {
            case 1:
                System.out.println("欢迎购买 冰露！");
                break;
            default:
                System.out.println("没有对应金额的饮料。。。");
                break;
            case 2:
                System.out.println("欢迎购买 农夫山泉！");
                break;
            case 3:
                System.out.println("您是想购买可乐还是雪碧呢？");
                String name = sc.next();
                if ("可乐".equals(name)) {
                    System.out.println("欢迎购买 可乐！");
                } else if ("雪碧".equals(name)) {
                    System.out.println("欢迎购买 雪碧！");
                } else {
                    System.out.println("没有3元的" + name);
                }
                break;
            case 5:
                System.out.println("欢迎购买 旺仔！");
                break;
            case 6:
                System.out.println("欢迎购买 红牛！");
                break;


        }
    }
}
