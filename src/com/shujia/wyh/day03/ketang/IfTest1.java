package com.shujia.wyh.day03.ketang;

import java.util.Scanner;

/*
   1:x和y的关系满足如下：
    x>=3	y = 2x + 1;
    -1<x<3	y = 2x;
    x<=-1	y = 2x – 1;

 */
public class IfTest1 {
    public static void main(String[] args) {
        //创建Scanner键盘录入对象
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入x的值：");
        int x = sc.nextInt();
        int y = 0;
        if (x >= 3) {
            y = 2 * x + 1;
        } else if (x > -1) {
            y = 2 * x;
        } else {
            y = 2 * x - 1;
        }

        System.out.println("y的值为：" + y);
    }
}
