package com.shujia.wyh.day03.ketang;

import java.util.Scanner;

/*
    键盘录入两个数据，返回两个数中的较大值
 */
public class FunctionTest1 {
    public static void main(String[] args) {
        //创建键盘录入对象
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入第一个数字：");
        int num1 = sc.nextInt();
        System.out.println("请输入第二个数字：");
        int num2 = sc.nextInt();

//        if(num1>num2){
//            System.out.println(num1);
//        }else {
//            System.out.println(num2);
//        }

//        int maxNum = maxNumber(num1,num2);
//        System.out.println(maxNum);
//
//        //键盘录入两个数据，比较两个数是否相等
//        boolean b1 = ifEquals(num1,num2);
//        System.out.println(b1);

//        boolean b2 = ifEquals(num1,num2);
//        System.out.println(b1);

        //键盘录入三个数据，返回三个数中的最大值
        System.out.println("请输入第三个数字：");
        int num3 = sc.nextInt();

        int maxNum2 = maxNumber2(num1, num2, num3);
        System.out.println(maxNum2);

    }

    public static int maxNumber2(int x, int y, int z) {
        if (x > y) {
            if (x > z) {
                return x;
            } else {
                return z;
            }
        } else {
            if (y > z) {
                return y;
            } else {
                return z;
            }
        }
    }


    public static boolean ifEquals(int a, int b) {
        if (a == b) {
            return true;
        } else {
            return false;
        }
    }

    public static int maxNumber(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
//        return a;
//        return b;  //一个方法只能有一个return生效
    }
}
