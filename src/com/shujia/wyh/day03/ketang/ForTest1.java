package com.shujia.wyh.day03.ketang;

/*
    请在控制台输出数据1-10
    请在控制台输出数据10-1
    求出1-10之间数据之和
    求出1-100之间偶数和
    求出1-100之间奇数和
    求5的阶乘

 */
public class ForTest1 {
    public static void main(String[] args) {
        // 请在控制台输出数据1-10
//        for(int i=1;i<11;i++){
//            System.out.println(i);
//        }

        //请在控制台输出数据10-1
//        for (int i = 10; i > 0; i--) {
//            System.out.println(i);
//        }

        //求出1-10之间数据之和
//        int sumNumber = 0;
//        for (int i = 1; i <= 10; i++) {
//            sumNumber += i;
//        }
//        System.out.println("1-10之间的和为：" + sumNumber);

        //求出1-100之间偶数和
//        int ouShuSum = 0;
//        int jiShuSum = 0;
//        for (int i = 1; i <= 100; i++) {
//            //对2进行取余数，如果余数是1，就是奇数，如果余数是0，就是偶数
//            if (i % 2 == 1) {
//                jiShuSum += i;
//            } else {
//                ouShuSum += i;
//            }
//        }
//        //代码格式化的快捷键是：ctrl+alt+L
//
//        System.out.println("1-100之间的偶数之和：" + ouShuSum);
//        System.out.println("1-100之间的奇数之和：" + jiShuSum);


        //求5的阶乘
        //5!=5*4*3*2*1
        int result = 1;
        for (int i = 5; i >= 1; i--) {
            result *= i;
        }
        System.out.println("5的阶乘是：" + result);


    }
}
