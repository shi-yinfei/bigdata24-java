package com.shujia.wyh.day03.ketang;
import java.util.Scanner;

/*
    键盘录入月份的值，输出对应的季节
    春季：3-5
    夏季：6-8
    秋季：9-11
    冬季：12，1，2

 */
public class IfTest2 {
    public static void main(String[] args) {
        //创建键盘录入对象
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入月份：1-12");

        int month = sc.nextInt();

        if(month>=3 & month<=5){
            System.out.println("春季");
        }else if(month>=6 & month<=8){
            System.out.println("夏季");
        }else if(month>=9 & month<=11){
            System.out.println("秋季");
        }else if (month==12 | month==1 | month==2){
            System.out.println("冬季");
        }else {
            System.out.println("您输入的月份有误！！");
        }

    }
}
