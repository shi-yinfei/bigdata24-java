package com.shujia.wyh.day15;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/*
    tcp协议客户端编程：
        1:建立客户端的Socket服务,并明确要连接的服务器。
        2:如果连接建立成功,就表明,已经建立了数据传输的通道.就可以在该通道通过IO进行数据的读取和写入.该通道称为Socket流,
            Socket流中既有读取流,也有写入流.
        3:通过Socket对象的方法,可以获取这两个流
        4:通过流的对象可以对数据进行传输
        5:如果传输数据完毕,关闭资源

 */
public class ClientDemo1 {
    public static void main(String[] args) throws Exception {
        //1:建立客户端的Socket服务,并明确要连接的服务器。
        //客户端的Socket类就叫做Socket
        //Socket(String host, int port)
        //创建流套接字并将其连接到指定主机上的指定端口号。
        //如果这个对象能够成功创建出来，说明连接已经建立
        Socket socket = new Socket("192.168.7.25", 12345);
        System.out.println("===========================欢迎进入24期欢乐聊天室===========================");
        //创建键盘录入对象
        Scanner sc = new Scanner(System.in);

        //获取通道中的字节输出流对象
        OutputStream outputStream = socket.getOutputStream();
        //获取通道中的字节输入流接收服务器端给的反馈
        InputStream inputStream = socket.getInputStream();

        while (true){

            System.out.print("输入您要发送的消息：");
            String info = sc.next();
            //调用方法写数据
            outputStream.write(info.getBytes());
            if("886".equals(info)){
                break;
            }

//            socket.shutdownOutput();
        }
        //关闭通道连接
        socket.close();
    }
}
