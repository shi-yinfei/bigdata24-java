package com.shujia.wyh.day15;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
    Executors工具类来创建线程池
 */
public class ThreadPoolDemo1 {
    public static void main(String[] args) {
        //public static ExecutorService newFixedThreadPool(int nThreads)
        ExecutorService pool = Executors.newFixedThreadPool(3); //Thread-1 ~ Thread-3

        //Future<?> submit(Runnable task)
        //提交一个可运行的任务执行，并返回一个表示该任务的未来。
//        pool.submit(new MyRunnable());
//        pool.submit(new MyRunnable());
//        pool.submit(new MyRunnable());
//        pool.submit(new MyRunnable());

        //创建线程的第三种方式：实现Callable接口，实现call方法
        //<T> Future<T> submit(Callable<T> task)
        //提交值返回任务以执行，并返回代表任务待处理结果的Future。
        pool.submit(new MyCallable());
        pool.submit(new MyCallable());
        pool.submit(new MyCallable());



        pool.shutdown();
    }
}
