package com.shujia.wyh.day15.factorydemo1;

/*
    假设现在有一个动物工厂，我需要什么动物，工厂就会生产一个对应动物出来
 */
public class AnimalTest {
    public static void main(String[] args) {
        //需要养一只dog
        Animal dog = AnimalFactory.getAnimal("dog");

        if(dog!=null){
            dog.eat();
            dog.sleep();
        }

        //养一只猫
        Animal cat = AnimalFactory.getAnimal("cat");

        if(cat!=null){
            cat.eat();
            cat.sleep();
        }


        Animal pig = AnimalFactory.getAnimal("pig");

        if(pig!=null){
            pig.eat();
            pig.sleep();
        }
    }
}
