package com.shujia.wyh.day15.factorydemo1;

//动物工厂
public class AnimalFactory {
    //构造方法私有化
    private AnimalFactory(){}

    public static Animal getAnimal(String name){
        if("dog".equals(name)){
            return new Dog();
        }else if("cat".equals(name)){
            return new Cat();
        }else {
            System.out.println("目前工厂还没有生产该动物的能力");
            return null;
        }
    }
}
