package com.shujia.wyh.day15;

import java.net.InetAddress;

/*
    java提供了一个类InetAddress来描述ip地址
    但是经过我们看api后发现，这个类不是工具类，但是也没有构造方法，没有构造方法就意味着无法创建对象
    又发现，方法居然不是静态的，部分不是，不是静态的就意味着要有对象才能调用方法
    我们可以通过调用部分静态方法获取当前类的对象

 */
public class InetAddressDemo1 {
    public static void main(String[] args) throws Exception {
        //static InetAddress getByName(String host)
        //确定主机名称的IP地址。
        InetAddress inetAddress = InetAddress.getByName("192.168.7.25");
//        System.out.println(inetAddress);

        //String getHostName()
        //获取此IP地址的主机名。
        System.out.println(inetAddress.getHostName());

        //String getHostAddress()
        //返回文本显示中的IP地址字符串。
        System.out.println(inetAddress.getHostAddress());

    }
}
