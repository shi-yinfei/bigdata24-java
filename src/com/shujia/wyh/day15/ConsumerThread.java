package com.shujia.wyh.day15;

public class ConsumerThread extends Thread {
    private Student student;

    public ConsumerThread() {
    }

    public ConsumerThread(Student student) {
        this.student = student;
    }

    @Override
    public void run() {
//        Student student = new Student();
        while (true){
            synchronized (student){
                //作为消费者，在消费数据之前应该先看一看数据有没有生产出来，如果已经生产了就消费，如果没有就等待
                if(!student.flag){ //当flag为false的时候进行等待
                    try {
                        student.wait();  //线程变成了阻塞状态 直到锁对象调用了notify()的方法才可以继续执行
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                System.out.println(student.name + "---" + student.age);

                //消费完后，将flag变成false
                student.flag = false;
                //通知生产者生产数据
                student.notify();

            }

        }
    }
}
