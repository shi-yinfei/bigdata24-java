package com.shujia.wyh.day15;


/*
    线程组：ThreadGroup

    ThreadGroup(String name)构造一个新的线程组。
 */
public class ThreadGroupDemo1 {
    public static void main(String[] args) {
        //创建一个线程组
        ThreadGroup tg1 = new ThreadGroup("帅哥组");
        //Thread(ThreadGroup group, String name)
        //分配一个新的 Thread对象。

        MyThread t1 = new MyThread(tg1, "王宇杰");
        MyThread t2 = new MyThread(tg1, "李佳豪");
        System.out.println(t1.getName() + "线程是属于：" + t1.getThreadGroup().getName()); //默认是属于名字叫main的线程组
        System.out.println(t2.getName() + "线程是属于：" + t2.getThreadGroup().getName()); //默认是属于名字叫main的线程组

        //直接对一个线程组设置，将来其实就是对线程组中的所有线程做了设置
        tg1.setDaemon(true);


    }
}
