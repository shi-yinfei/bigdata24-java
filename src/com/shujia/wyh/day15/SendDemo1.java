package com.shujia.wyh.day15;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

/*
    UDP协议发送端编程：
    使用java提供给的一个类DatagramSocket中的无参构造方法来创建发送端的Socket对象
 */
public class SendDemo1 {
    public static void main(String[] args) throws Exception {
        //创建Socket对象
        DatagramSocket ds = new DatagramSocket();
        //创建一个键盘录入对象
        Scanner sc = new Scanner(System.in);

        while (true){
            System.out.print("您要发送的数据：");
            String info = sc.next();
            byte[] bytes = info.getBytes();

            //length代表的是字节数组的长度
            int length = bytes.length;
            //address代表的是目标接收端的ip地址
            InetAddress inetAddress = InetAddress.getByName("192.168.7.25");
            //port是接收端所占用的端口
            int port = 10086;

            //创建一个数据包
            DatagramPacket dp = new DatagramPacket(bytes, length, inetAddress, port);
            //Socket对象调用方法发送数据包
            ds.send(dp);

            if("886".equals(info)){
                break;
            }
        }

        //关闭ds连接
        ds.close();

    }
}
