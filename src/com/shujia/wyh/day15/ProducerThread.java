package com.shujia.wyh.day15;

public class ProducerThread extends Thread {

    private Student student;
    int a = 0;

    public ProducerThread() {
    }

    public ProducerThread(Student student) {
        this.student = student;
    }

    @Override
    public void run() {
//        Student student = new Student();
        while (true) {
            synchronized (student){
                //在生产数据之前，应该先看一看数据有没有被消费，或者是有没有数据
                //flag是否为false
                if(student.flag){ //当flag是true的时候，表示数据已经生产还没有被消费
                    //等待消费来消费
                    //这里是通过锁对象来进行调用wait方法等待
                    try {
                        student.wait();  // 线程变成阻塞状态
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                if (a % 2 == 0) {
                    student.name = "张怀远";
                    student.age = 18;
                } else {
                    student.name = "王宇杰"; // age=18
                    student.age = 17;
                }

                student.flag = true;
                //通知消费者消费数据
                student.notify();

                a++;
            }

        }

    }
}
