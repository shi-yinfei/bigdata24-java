package com.shujia.wyh.day15.danlidemo2;

public class Student {
    private static Student student;

    //构造方法私有化
    private Student(){}

    //t1,t2,t3
    public synchronized static Student getStudent(){
        if(student==null){
            //t1,t2,t3
            return student = new Student();
        }else {
            return student;
        }
    }
}
