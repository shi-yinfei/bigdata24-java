package com.shujia.wyh.day15.danlidemo2;

/*
    单例模式：懒汉式 懒汉式具有线程安全的问题，所以需要synchronized

    面试的时候说懒汉式，开发中使用饿汉式
 */
public class DanLiTest {
    public static void main(String[] args) {
        //获取一个学生对象
        Student s1 = Student.getStudent();
        Student s2 = Student.getStudent();

        System.out.println(s1==s2);

    }
}
