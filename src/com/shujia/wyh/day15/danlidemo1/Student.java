package com.shujia.wyh.day15.danlidemo1;
//如何实现类在内存中只有一个对象呢?
public class Student {
    //自己类的内部创建一个对象
    private static Student student = new Student();

    //构造方法私有化
    private Student(){}


    //提供一个公共的成员方法，让外界获取这个对象
    public static Student getStudent(){
        return student;
    }
}
