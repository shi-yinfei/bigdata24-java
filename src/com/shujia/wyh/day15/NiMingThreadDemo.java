package com.shujia.wyh.day15;

/*
    三种实现线程的方式：
        1、继承Thread类，实现run方法
        2、实现Runnable接口，实现run方法
        3、实现Callable接口，实现call方法，需要结合线程池去使用
 */
public class NiMingThreadDemo {
    public static void main(String[] args) {
        //使用匿名内部类创建一个线程对象
//        new Thread() {
//            @Override
//            public void run() {
//                for (int i = 1; i <= 200; i++) {
//                    System.out.println(getName() + "---" + i);
//                }
//            }
//        }.start();
//
//        new Thread() {
//            @Override
//            public void run() {
//                for (int i = 1; i <= 200; i++) {
//                    System.out.println(getName() + "---" + i);
//                }
//            }
//        }.start();

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 1; i <= 200; i++) {
//                    System.out.println(Thread.currentThread().getName() + "---" + i);
//                }
//            }
//        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("陆澳真帅");
            }
        }).start();


    }
}
