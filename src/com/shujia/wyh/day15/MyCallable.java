package com.shujia.wyh.day15;

import java.util.concurrent.Callable;

public class MyCallable implements Callable {
    @Override
    public Object call() throws Exception {
        for (int i = 1; i <= 10; i++) {
            System.out.println(Thread.currentThread().getName() + "---" + i);
        }
        return new Object();
    }
}
