package com.shujia.wyh.day15.factorydemo2;

public abstract class Animal {
    String name;
    int age;

    public abstract void eat();

    public abstract void sleep();
}
