package com.shujia.wyh.day15.factorydemo2;

/*
    工厂方法模式：每一个动物独立一个工厂
 */
public class AnimalTest {
    public static void main(String[] args) {
        //我需要养一只猫
        CatFactory catFactory = new CatFactory();
        Animal animal = catFactory.getAnimal();
        animal.eat();
        animal.sleep();

        //养一只狗
        DogFactory dogFactory = new DogFactory();
        Animal animal1 = dogFactory.getAnimal();
        animal1.eat();
        animal1.sleep();
    }
}
