package com.shujia.wyh.day15.factorydemo2;

public class CatFactory extends Factory{
    @Override
    public Animal getAnimal() {
        return new Cat();
    }
}
