package com.shujia.wyh.day15.factorydemo2;

public class Cat extends Animal {
    @Override
    public void eat() {
        System.out.println("🐱吃🐟");
    }

    @Override
    public void sleep() {
        System.out.println("🐱蜷着睡");
    }
}
