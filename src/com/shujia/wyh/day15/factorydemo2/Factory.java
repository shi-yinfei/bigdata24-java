package com.shujia.wyh.day15.factorydemo2;

public abstract class Factory {
    public abstract Animal getAnimal();
}
