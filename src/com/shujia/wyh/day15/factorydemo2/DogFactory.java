package com.shujia.wyh.day15.factorydemo2;

public class DogFactory extends Factory{
    @Override
    public Animal getAnimal() {
        return new Dog();
    }
}
