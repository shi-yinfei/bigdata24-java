package com.shujia.wyh.day15;

import java.util.Timer;
import java.util.TimerTask;

/*
    定时器：Timer 线程调度任务以供将来在后台线程中执行的功能。 任务可以安排一次执行，或定期重复执行。
    构造方法：
        public Timer()

 */
public class TimerDemo1 {
    public static void main(String[] args) {
        //创建一个定时器对象
        Timer timer = new Timer();

        //public void schedule(TimerTask task, long delay)  延迟delay毫秒后，做某一件事请task
//        timer.schedule(new MyTimerTask(timer),5000L);

//        timer.cancel(); //取消定时器

        //public void schedule(TimerTask task,long delay,long period)  延迟delay毫秒后，做某一件事情，然后每间隔period毫秒重复执行
        timer.schedule(new MyTimerTask(timer),5000,1000);


    }
}

class MyTimerTask extends TimerTask{
    private Timer timer;

    public MyTimerTask(Timer timer) {
        this.timer = timer;
    }

    @Override
    public void run() {
        //这里的定时任务根据您的情况修改
        System.out.println("砰！！爆炸了！");
//        timer.cancel();
    }
}




//class MyTimerTask extends TimerTask{
//    private Timer timer;
//
//    public MyTimerTask(Timer timer) {
//        this.timer = timer;
//    }
//
//    @Override
//    public void run() {
//        System.out.println("李佳豪变身");
//        timer.cancel();
//        System.out.println("定时器已关闭。。。");
//    }
//}
