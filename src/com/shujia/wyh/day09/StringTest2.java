package com.shujia.wyh.day09;

/*
    统计大串中小串出现的次数（作业）
    举例：在字符串” woaijavawozhenaijavawozhendeaijavawozhendehenaijavaxinbuxinwoaijavagun”中java出现了5次

 */
public class StringTest2 {
    public static void main(String[] args) {
        String bigStr = "woaijavawozhenaijavawozhendeaijavawozhendehenaijavaxinbuxinwoaijavagun";
        String small = "java";

//        System.out.println(bigStr.indexOf(small));
        // 使用while循环进行查找截取
        int index = bigStr.indexOf(small);
        // 定义一个变量记录次数
        int count = 0;
        while (index != -1) {
            count++;
            //截取
            bigStr = bigStr.substring(index + small.length());
            //重新找小串
            index = bigStr.indexOf(small);
        }

        System.out.println("大串中出现小串的次数为：" + count);


    }
}
