package com.shujia.wyh.day09;

/*
    String s = “hello”; s += “world”; 问s的结果是多少?

 */
public class StringDemo2 {
    public static void main(String[] args) {
        String s = "hello";
        s += "world";
        System.out.println(s);
    }
}
