package com.shujia.wyh.day09;

//在控制分别输出：30，20，10
class Outer {
    public int num = 10;
    class Inner {
        public int num = 20;
        public void show() {
            int num = 30;
            System.out.println(num);  // 30
            System.out.println(this.num);  // 20
            System.out.println(Outer.this.num);  // 10
        }
    }
}


public class InnerClassDemo2 {
    public static void main(String[] args) {
        Outer.Inner oi = new Outer().new Inner();
        oi.show();
    }
}
