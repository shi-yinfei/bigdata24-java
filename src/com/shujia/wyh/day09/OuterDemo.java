package com.shujia.wyh.day09;

//要求在控制台输出”HelloWorld”
// 常用类 枚举 IO流 多线程 网络编程 反射
interface Inter {
    void show();
}

class Outer3 {
    public static Inter method(){
        return new Inter() {
            @Override
            public void show() {
                System.out.println("HelloWorld");
            }
        };
    }
}

class OuterDemo {
    public static void main(String[] args) {
        //Outer3类中有一个静态的method方法，可以直接使用类名的方式调用
        //我们又发现调用完method()方法后又调用了show()方法，表示method()有返回值类型，而且返回的是一个对象
        //又观察发现，show()方法是接口中的show()方法
        Outer3.method().show();
    }
}
