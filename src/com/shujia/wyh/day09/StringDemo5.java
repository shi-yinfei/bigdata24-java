package com.shujia.wyh.day09;

/*
    byte[] getBytes()
    char[] toCharArray()
    static String valueOf(char[] chs)
    static String valueOf(int i)
    String toLowerCase()
    String toUpperCase()
    String concat(String str)

 */
public class StringDemo5 {
    public static void main(String[] args) {
        String s = "今天的天气还不错！";

        //byte[] getBytes()
        //将字符串转化成字节数组
//        byte[] bytes = s.getBytes();  // 在UTF-8的编码下，一个字符所对应三个字节
//        printArray(bytes);

        //char[] toCharArray()
        //将字符串转化为字符数组
//        char[] chars = s.toCharArray();
//        printArray(chars);

        //static String valueOf(char[] chs)
        //将字符数组转成字符串
//        System.out.println(String.valueOf(chars));

        //static String valueOf(int i)
//        System.out.println(String.valueOf(97)); // 97---> "97"

        //String toLowerCase() //将字符全部转小写
        String s2 = "HelloWorLd";
//        System.out.println(s2.toLowerCase());

        //String toUpperCase() 将字符全部转大写
        System.out.println(s2.toUpperCase());

        //String concat(String str) 字符串拼接
        String s3 = "shujia";
        System.out.println(s2.concat(s3));






    }

    /**
     * 返回值类型：void
     * 参数列表：char[] array
     */
    public static void printArray(char[] array) {
        for (int i = 0; i < array.length; i++) {
            if (i == 0) {
                System.out.print("[" + array[i] + ",");
            } else if (i == array.length - 1) {
                System.out.println(array[i] + "]");
            } else {
                System.out.print(array[i] + ",");
            }
        }
    }
}
