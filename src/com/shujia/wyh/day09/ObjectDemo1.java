package com.shujia.wyh.day09;

/*
   Object: java中所有的类(包括我们自己定义的类)都有一个默认的父类叫做Object

    public int hashCode()
    public final Class getClass()
    public String toString()


    一个标准类的最终写法：
        成员变量：使用private关键字修饰
        构造方法：提供一个无参一个带参数
        成员方法：提供公共的getXxx()和setXxx()
        toString()：重写父类Object中的toString()方法，展示所有的成员变量值的情况

 */

class Demo4{
//    Demo4(){
//        super();
//    }

    String name;
    int age;

    @Override
    public String toString() {
        return "Demo4{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

public class ObjectDemo1 {
    public static void main(String[] args) {
        Demo4 demo4 = new Demo4();
        System.out.println(demo4.hashCode()); //1163157884

        System.out.println(demo4.getClass());

        // 重要：直接打印对象名，打印的是调用toString()后的结果
        System.out.println(demo4); //以字符串的形式打印对象值 就把他理解为对象的地址值的字符串的表现形式
        //getClass().getName() + "@" + Integer.toHexString(hashCode())
        //我们直接打印对象名的目的，大多数情况下，我们希望看到的是对象中的成员变量值的情况
        //我们要想打印对象成员变量值的情况的话，需要类中重写toString()
        //自动生成即可

//        Demo4 demo5 = demo4;
//        System.out.println(demo5.toString());


    }
}
