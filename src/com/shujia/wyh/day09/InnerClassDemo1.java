package com.shujia.wyh.day09;

/*
    内部类：将类定义在其他类的内部
    根据定义的位置不同，分成两种内部类
        定义在成员位置：成员内部类
        定义在方法中位置：局部内部类

    成员内部类
 */

class Outer1{
    static int a = 10;
    private static int b = 20;

    public void show(){

    }

    static class Inner1{
        public void fun1(){
            // 成员内部类中的方法可以访问外部类中所有的成员变量，包括私有
            System.out.println(a);
            System.out.println(b);
        }
    }

//    public void fun2(){
//        Inner1 inner1 = new Inner1();
//        inner1.fun1();
//    }
}

public class InnerClassDemo1 {
    public static void main(String[] args) {
        //创建一个内部类的对象前提是要先创建外部类的对象
//        Outer1.Inner1 oi1 = new Outer1().new Inner1();
//        oi1.fun1();
//        Outer1 outer1 = new Outer1();
//        outer1.fun2();

        //这是当内部类是静态的时候的创建方式，直接通过外部类名获取内部类的构造方法
        Outer1.Inner1 oi1 = new Outer1.Inner1();
        oi1.fun1();

    }
}
