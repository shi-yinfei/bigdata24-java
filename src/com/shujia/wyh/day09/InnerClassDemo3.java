package com.shujia.wyh.day09;

/*
    局部内部类：在类中方法中定义的类


 */

class Outer2{
    int a = 10;
    public void fun1(){
        int d = 40;  // 在jdk1.8之后，方法中如果存在局部内部类，方法中定义的局部变量默认会加上final关键字
        //局部内部类
        class Inner2{
            private int b = 20;
            public void show(){
                a = 100;
                b = 200;
//                d = 55;
                int c = 30;
                System.out.println(a);
                System.out.println(b);
                System.out.println(c);
                System.out.println(d);
            }
        }

        Inner2 inner2 = new Inner2();
        inner2.show();

    }


}

public class InnerClassDemo3 {
    public static void main(String[] args) {
        Outer2 outer2 = new Outer2();
        outer2.fun1();
    }
}
