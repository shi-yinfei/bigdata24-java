package com.shujia.wyh.day09;

import java.util.Objects;
import java.util.Scanner;

/*
    public boolean equals(Object obj)

 */
class Student{
    String name;
    int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age && Objects.equals(name, student.name);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

public class ObjectDemo2 {
    public static void main(String[] args) {
        //创建一个学生对象
        Student student1 = new Student("王庆",18);
        System.out.println(student1);

        //在创建一个学生对象
        Student student2 = new Student("王庆", 18);
        System.out.println(student2);

        //==比较基本数据类型的时候，比较的是具体数值
        //==比较引用数据类型的时候，比较的是地址值
        System.out.println(student1==student2);

        //需求：当学生的姓名和年龄一样的时候，说明是同一个学生
        //Object中的equals方法底层实现是由==比较的，而==比较引用数据类型的时候，比较的是地址值
        //要想equals比较的是内容（成员变量的值）的话，类重写equals方法
        System.out.println(student1.equals(student2));


        Scanner sc = new Scanner(System.in);
        String s = sc.next();
        //我们回忆到之前说比较两个字符串是否相同的时候，我们采用equals方法比较的
        String s1 = "Hello";
//        String s2 = "Hello";


//        System.out.println(s1==s);
        System.out.println(s1.equals(s));
    }
}
