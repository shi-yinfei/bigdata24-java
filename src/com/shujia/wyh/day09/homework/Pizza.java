package com.shujia.wyh.day09.homework;

public abstract class Pizza {
    private String name;
    private  int size;
    private int price;
    public Pizza() {
        super();
    }
    public Pizza(String name, int size, int price){
        super();
        this.name = name;
        this.price = price;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    public abstract void show();
}
