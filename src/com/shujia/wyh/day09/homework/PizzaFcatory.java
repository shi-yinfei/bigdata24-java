package com.shujia.wyh.day09.homework;
import java.util.Scanner;
public class PizzaFcatory {
    public  static Pizza makePizza(){
        System.out.println("请选择想要制作的比萨(1.培根比萨 2.海鲜比萨)：");
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        Pizza pizza = null;
        if(i==1){
            System.out.println("请输入培根克数：");
            int gram = scanner.nextInt();
            System.out.println("请输比萨大小：");
            int size = scanner.nextInt();
            System.out.println("请输入披萨价格：");
            int price = scanner.nextInt();
            pizza = new BaconPizza("培根披萨", size, price, gram);
        }else if (i==2){
            System.out.println("请输入配料信息：");
            String mixture = scanner.next();
            System.out.println("请输比萨大小：");
            int size = scanner.nextInt();
            System.out.println("请输入披萨价格：");
            int price = scanner.nextInt();
            pizza = new SeaFoodPizza("海鲜披萨", size, price, mixture);
        }
        return pizza;
    }
    public static void main(String[] args){
        Pizza pizza = makePizza();
        pizza.show();
    }
}
