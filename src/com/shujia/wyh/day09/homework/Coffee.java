package com.shujia.wyh.day09.homework;

public class Coffee extends Drink{
    private String mixture;

    public Coffee(){
        super();
    }
    public Coffee(String name,int volume,String mixture){
        super(name, volume);
        this.mixture = mixture;
    }

    public String getMixture() {
        return mixture;
    }

    public void setMixture(String mixture) {
        this.mixture = mixture;
    }

    public void show() {
        System.out.println("您购买的饮料信息如下：");
        System.out.println("名称："+super.getName());
        System.out.println("容量："+super.getVolume());
        System.out.println("添加物："+this.mixture);
    }
}
