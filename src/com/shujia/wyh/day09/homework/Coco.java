package com.shujia.wyh.day09.homework;

public class Coco extends Drink{
    public Coco(){
        super();
    }
    public Coco(String name,int volume){
        super(name,volume);
    }
    public void show(){
        System.out.println("您购买的饮料信息如下：");
        System.out.println("名称："+super.getName());
        System.out.println("容量："+super.getVolume());
    }
}
