package com.shujia.wyh.day09.homework;

public abstract class Drink {
    private String name;
    private int volume;
    public Drink(){

    }
    public Drink(String name,int volume){
        super();
        this.name = name;
        this.volume = volume;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void show(){
        System.out.println("您购买的饮料信息如下：");
        System.out.println("名称："+name);
        System.out.println("容量："+volume);
    };
}
