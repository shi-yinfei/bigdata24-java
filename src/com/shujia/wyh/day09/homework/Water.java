package com.shujia.wyh.day09.homework;

public class Water extends Drink{
    public Water(){
        super();
    }
    public Water(String name,int volume){
        super(name,volume);
    }

    @Override
    public void show() {
        System.out.println("您购买的饮料信息如下：");
        System.out.println("名称："+super.getName());
        System.out.println("容量："+super.getVolume());
    }
}
