package com.shujia.wyh.day09.homework;
import  java.util.Scanner;
public class TestDrink {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("请选择饮料（1.咖啡  2 矿泉水  3 可乐）：");
        String choice = input.next();
        Drink drink = DrinkFactory.getDrink(choice);
        drink.show();
    }
}
