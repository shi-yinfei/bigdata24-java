package com.shujia.wyh.day09.homework;

public class SeaFoodPizza extends Pizza{
    private String mixture;
    public SeaFoodPizza(){
        super();
    }
    public SeaFoodPizza(String name, int price, int size, String mixture){
        super(name,price,size);
        this.mixture = mixture;
    }

    public String getMixture() {
        return mixture;
    }

    public void setMixture(String mixture) {
        this.mixture = mixture;
    }

    public void show() {
        System.out.println("名称"+super.getName());
        System.out.println("价格"+super.getPrice()+"元");
        System.out.println("大小"+super.getSize()+"寸");
        System.out.println("配料信息"+this.mixture);
    }
}


