package com.shujia.wyh.day09.homework;
import java.util.Scanner;
public class DrinkFactory {
    public  static Drink getDrink(String no){
        Scanner scanner = new Scanner(System.in);
        Drink drink = null;
        if("1".equals(no)){
            System.out.println("请输入购买容量：");
            int volume = scanner.nextInt();
            System.out.println("请问是否要配料（1、加糖 2、加奶 3、什么也不加）");
            int choice = scanner.nextInt();
            String mixture;
            switch(choice){
                case 1: mixture ="加糖";break;
                case 2: mixture ="加奶";break;
                default : mixture="什么一个不加";
            }
            drink = new Coffee("咖啡",volume, mixture);
        }else if("2".equals(no)){
            System.out.println("请输入购买容量：");
            int volume = scanner.nextInt();
            drink = new Water("矿泉水",volume);
        }else if("3".equals(no)){
            System.out.println("请输入购买容量：");
            int volume = scanner.nextInt();
            System.out.println("请问需要1、可口可乐还是2、百事可乐");
            int choice = scanner.nextInt();
            switch(choice){
                case 1: drink = new Coco("可口可乐",volume);
                case 2: drink = new Coco("百事可乐",volume);
                break;
            }
            }
            return drink;     }
}

