package com.shujia.wyh.day09;


/*
    字符串：一串字符，那一根签字，将若干个字符穿起来的串儿，一旦被创建，就不能被更改。
    java提供了一个类专门表示是字符串类型的：String

    构造方法：
        public String()
        public String(byte[] bytes)
        public String(byte[] bytes,int offset,int length)
        public String(char[] value)
        public String(char[] value,int offset,int count)
        public String(String original)



 */
public class StringDemo1 {
    public static void main(String[] args) {
//        String s = "Hello";
        //public String() 使用无参构造方法创建一个String类的对象
//        String s1 = new String();
//        System.out.println(s1); //String类中重写了toString()方法

        //public String(byte[] bytes)  将字节数组转化成字符串
//        byte[] bytes = {97, 98, 99, 100};
////        String s1 = new String(bytes);
////        System.out.println(s1);
//        //public String(byte[] bytes,int index,int length) //将字节数组的一部分转化成字符串
//        String s1 = new String(bytes, 1, 2);
//        System.out.println(s1);
        //public String(char[] value)  将字符数组转成一个字符串
//        char[] chars = {'我','爱','冯','提','莫'};
////        String s1 = new String(chars);
////        System.out.println(s1);
//        //public String(char[] value,int offset,int count) 将字符数组的一部分转化成字符串
//        String s1 = new String(chars, 2, 3);
//        System.out.println(s1);
        //public String(String original) // 将字符串转成字符串对象
        String s1 = new String("你好");
//        System.out.println(s1);
        String s2 = "你好";
        System.out.println(s1==s2);
        System.out.println(s1.equals(s2));


    }
}
