package com.shujia.wyh.day09;

/*
    匿名内部类当作参数使用
 */

//当接口作为方法的形式参数的时候，将来调用方法实际应该传入实现该接口类的对象
interface Inter3{
    void fun3();
}

//class Inter3Impl implements Inter3{
//    @Override
//    public void fun3() {
//        System.out.println("史俊超比杨志还帅！！");
//    }
//}

class Demo3{
    public void show(Inter3 inter3){
        inter3.fun3();
    }
}

public class NIMingClassDemo3 {
    public static void main(String[] args) {
        Demo3 demo3 = new Demo3();
//        Inter3Impl inter3 = new Inter3Impl();
        demo3.show(new Inter3() {
            @Override
            public void fun3() {
                System.out.println("王庆比史俊超还帅！");
            }
        });
    }
}
