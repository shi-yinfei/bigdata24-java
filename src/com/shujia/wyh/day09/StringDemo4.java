package com.shujia.wyh.day09;

/*
    int length()
    char charAt(int index)
    int indexOf(int ch)
    int indexOf(String str)
    int indexOf(int ch,int fromIndex)
    int indexOf(String str,int fromIndex)
    String substring(int start)
    String substring(int start,int end)

 */
public class StringDemo4 {
    public static void main(String[] args) {
        String s = "张玮真abc帅，比不史俊超def还帅，杨a志不服";
        System.out.println(s.length());

        //char charAt(int index)  根据索引获取对应的字符
        //字符串可以被看作是一个字符数组
        System.out.println(s.charAt(3));

        //int indexOf(int ch)  传入字符对应的ASCII码值，返回字符在字符串中的索引
        System.out.println(s.indexOf(97));

        //int indexOf(String str)  传入字符串，返回字符串第一个字符在大字符串中的位置索引  如果找不到 返回-1
        System.out.println(s.indexOf("王宇杰"));

        //int indexOf(int ch,int fromIndex)  从指定位置查找字符
        System.out.println(s.indexOf(97,8));

        //int indexOf(String str,int fromIndex)  从指定位置查找，返回字符串第一个出现的位置


        //String substring(int start)
        //截取字符串
        System.out.println(s.substring(10));  //从指定位置开始截取，一直截取到末尾

        //String substring(int start,int end)
        //截取字符串一部分 [start , end)
        System.out.println(s.substring(10,13));




    }
}
