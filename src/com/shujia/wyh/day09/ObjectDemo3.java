package com.shujia.wyh.day09;

/*
    protected void finalize()
        手动垃圾回收机制。但是具体什么时候回收不确定。

    protected Object clone()
        克隆，拷贝 （浅拷贝，深拷贝）

 */

class Demo {
    int a = 10;
}

//如果将来发现一个接口中什么都没有，连常量和抽象方法都没有，这种接口叫做标记接口
class Person implements Cloneable {
    String name;
    int age;
    Demo demo;


    public Person(String name, int age, Demo demo) {
        this.name = name;
        this.age = age;
        this.demo = demo;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", demo=" + demo +
                '}';
    }

    @Override
    public Person clone() {
        try {
            Person clone = (Person) super.clone();
            // TODO: copy mutable state here, so the clone can't change the internals of the original
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}

public class ObjectDemo3 {
    public static void main(String[] args) throws Exception {
        Demo demo = new Demo();

        //创建一个学生对象
        Person p1 = new Person("小虎", 18, demo);
        Person p2 = p1.clone(); //克隆出来的对象和原本的是两个不同的空间的对象  Object类中的clone方法指的是浅拷贝
        System.out.println(p1);
        System.out.println(p2);
    }
}
