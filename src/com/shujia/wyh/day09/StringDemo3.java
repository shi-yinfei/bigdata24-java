package com.shujia.wyh.day09;

/*
    boolean equals(Object obj)
    boolean equalsIgnoreCase(String str)
    boolean contains(String str)
    boolean startsWith(String str)
    boolean endsWith(String str)
    boolean isEmpty()

 */
public class StringDemo3 {
    public static void main(String[] args) {
        String s1 = "hello";
        String s2 = "HelLO";

        //boolean equals(Object obj) 比较两个字符串的内容值是否一样
//        System.out.println(s1.equals(s2));

        //boolean equalsIgnoreCase(String str)  忽略大小写比较内容
        System.out.println(s1.equalsIgnoreCase(s2));

        //boolean contains(String str) 判断字符串中是否包含某个小字符串
        System.out.println(s1.contains("www"));

        // boolean startsWith(String str)  判断字符串是否以某个小字符串开头  ^
        System.out.println(s1.startsWith("h"));

        //boolean endsWith(String str) 判断字符串是否以某个小字符串结尾    $
        System.out.println(s1.endsWith("llo"));

        //boolean isEmpty()  判断是否为空
        System.out.println(s1.isEmpty());




    }
}
