package com.shujia.wyh.day09;

import java.util.Scanner;

/*
    字符串反转
    举例：键盘录入”abc”		输出结果：”cba”

 */
public class StringTest1 {
    public static void main(String[] args) {
        //创建键盘录入对象
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入您的字符串：");
        String s = sc.next();
        System.out.println("反转前：" + s);

        String newStr = "";
//        //方案1：倒着遍历，拼接字符串
//        for (int i = s.length() - 1; i >= 0; i--) {
//            newStr += s.charAt(i);
//        }
//
//        System.out.println("反转后：" + newStr);

        //方案2：将字符串转成数组，逆序后再转字符串
        char[] array = s.toCharArray();
        for (int start = 0, end = array.length - 1; start < end; start++, end--) {
            char tmp = array[start];
            array[start] = array[end];
            array[end] = tmp;
        }

        //将字符数组转化成字符串
        String s1 = new String(array);
        System.out.println("反转后：" + s1);

    }
}
