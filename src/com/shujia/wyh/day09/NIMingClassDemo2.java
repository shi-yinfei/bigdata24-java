package com.shujia.wyh.day09;

/*
    匿名内部类的开发应用（重点）
 */
//当接口作为方法的返回值类型的时候，返回的是实现该接口的具体类的对象
interface Inter2{
    void fun();
}

//创建一个具体的类实现Inter2的接口，重写接口中的方法
//class Inter2Impl implements Inter2{
//
//    @Override
//    public void fun() {
//        System.out.println("好好学习，天天向上。");
//    }
//}

class Demo2{
    public Inter2 show(){

//        return new Inter2Impl();

        //使用匿名内部类的形式实现
        return new Inter2(){

            @Override
            public void fun() {
                System.out.println("好好学习，天天向上。");
            }
        };

    }
}

public class NIMingClassDemo2 {
    public static void main(String[] args) {
        Demo2 demo2 = new Demo2();
        Inter2 i = demo2.show();
        i.fun();
    }
}
