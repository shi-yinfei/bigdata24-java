package com.shujia.wyh.day09;

/*
        匿名内部类

        前提是要存在一个类（具体的类/抽象类）或者是接口
        语句定义结构：
            new  类名/抽象类名/接口名(..){
                要重写的方法;
            }
 */

interface Inter1{
    void fun();
}

abstract class Demo1{
    public abstract void fun1();

    public abstract void fun2();
}


//class Inter1Impl implements Inter1{
//    @Override
//    public void fun() {
//        System.out.println("好好学习，天天向上！");
//    }
//}

public class NIMingClassDemo1 {
    public static void main(String[] args) {
//        Inter1 inter1 = new Inter1();
//        Inter1Impl inter1 = new Inter1Impl();
//        inter1.fun();

        //匿名内部类
//        new Inter1(){
//            @Override
//            public void fun() {
//                System.out.println("好好学习，天天向上");
//            }
//        }.fun();

        Demo1 d = new Demo1(){
            @Override
            public void fun1() {
                System.out.println("好好学习，天天向上");
            }

            @Override
            public void fun2() {
                System.out.println("杨志真帅！");
            }
        };

        d.fun1();
        d.fun2();
    }
}
