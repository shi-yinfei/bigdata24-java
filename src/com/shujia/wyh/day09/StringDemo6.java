package com.shujia.wyh.day09;

/*
    替换功能
    String replace(char old,char new)
    String replace(String old,String new)

    去除字符串两空格
    String trim()


    //按字典顺序比较两个字符串
    //int compareTo(String str)
    //int compareToIgnoreCase(String str)


 */
public class StringDemo6 {
    public static void main(String[] args) {
        String s = "   shuji  ajun  666,shijun  chao666  ";

        //String replace(char old,char new)  将所有的旧字符用新字符进行替换
        System.out.println(s.replace('6','_'));

        //String replace(String old,String new)
        System.out.println(s.replace("jun","shuai"));

        //String trim() 只会去除字符串两边的空格
        System.out.println(s.trim());

        //int compareTo(String str)
        String s1 = "hello"; // h -- 104
        String s2 = "world"; // w -- 119
        String s3 = "hel";
        System.out.println(s1.compareTo(s2)); //  -15
        System.out.println(s1.compareTo(s3)); //  2


    }
}

/**
 *     s1.compareTo(s3)
 *     s1 -- "hello"
 *     s3 -- "hel"
 *     public int compareTo(String anotherString) {
 *         int len1 = value.length;  // 5
 *         int len2 = anotherString.value.length;  // 3
 *         int lim = Math.min(len1, len2);  // 3
 *         char v1[] = value;   // ['h','e','l','l','o']
 *         char v2[] = anotherString.value;   // ['h','e','l']
 *
 *         int k = 0;
 *         while (k < lim) {
 *             char c1 = v1[k];
 *             char c2 = v2[k];
 *             if (c1 != c2) {
 *                 return c1 - c2;
 *             }
 *             k++;
 *         }
 *         return len1 - len2;
 *     }
 */
