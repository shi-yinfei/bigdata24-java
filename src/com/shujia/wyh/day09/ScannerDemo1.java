package com.shujia.wyh.day09;
import java.util.Scanner;

//键盘录入
public class ScannerDemo1 {
    public static void main(String[] args) {
        //public Scanner(InputStream source) 构造方法
        Scanner sc = new Scanner(System.in);

        //获取整数
        System.out.println("获取一个整数：");
        int number = sc.nextInt();
        System.out.println(number);

        //获取字符串
        System.out.println("输入一个字符串：");
        String s1 = sc.next(); //不会接收特殊字符
        System.out.println(s1);
//        String s2 = sc.nextLine();  //可以接收特殊字符
//        System.out.println(s2);

    }
}
