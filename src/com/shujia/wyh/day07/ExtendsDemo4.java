package com.shujia.wyh.day07;

/*
    super不仅可以使用直接父类中的成员变量，还可以使用直接父类中成员方法，还可以使用直接父类重点 构造方法
 */
class Fu3 {
    int a = 10;

    Fu3(String s){
        System.out.println("这是父类带参数的构造方法");
    }

    public void show(){
        System.out.println("你好");
    }
}

class Zi3 extends Fu3 {
    int a = 20;

    //在初始化子类之前一定会先初始化其直接父类
    //要想初始化子类，就必须先初始化其父类
    Zi3(){
        this("你好");
//        super("你好");  //如果父类中没有无参构造方法，可以使用带参数的构造方法对父类进行初始化
        System.out.println("你好");
        //子类构造方法中第一句话默认会有一个super()
//        super(); //java中所有的类都不能进行二次初始化
    }

    Zi3(String s){
        super(s);
        System.out.println("这是子类中带参数的构造方法");
    }

    public void fun1() {
        int a = 30;
        System.out.println(a); //就近原则  现在本方法中寻找，找不到就去本类中成员位置，再去父类中去找
        //打印20,本类中的成员变量a
        System.out.println(this.a);
        //打印10，父类中的a
        //java提供了一个关键字代表父类的引用：super
        System.out.println(super.a);

//        super.show();
    }
}

public class ExtendsDemo4 {
    public static void main(String[] args) {
        Zi3 zi3 = new Zi3();
        zi3.fun1();
    }
}
