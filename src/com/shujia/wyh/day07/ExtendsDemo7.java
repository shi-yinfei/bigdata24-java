package com.shujia.wyh.day07;

/*
    使用重写的注意事项：
        1、父类中私有方法不能被重写
        2、子类重写父类方法时，访问权限不能更低   public protected 默认的 private ,建议重写的时候与父类的方法声明保持一致
        3、子类无法重写父类中的静态方法

 */
class Fu5{
    private void show(){
        System.out.println("这是父类中的私有方法show");
    }

    void show1(){
        System.out.println("这是父类中默认权限修饰符修饰的方法show1");
    }

    public static void fun1(){
        System.out.println("这是父类中的静态方法fun1");
    }
}

class Zi5 extends Fu5{
//    @Override  //一个方法能否被重写，加上@Override进行验证
//    private void show(){
//        System.out.println("这是子类中的私有方法show");
//    }

    @Override
    void show1(){
        System.out.println("这是子类中重写父类默认权限修饰符修饰的方法show1");
    }

//    @Override
//    public static void fun1(){
//        System.out.println("这是子类中的静态方法fun1");
//    }

}

public class ExtendsDemo7 {
    public static void main(String[] args) {
        Zi5 zi5 = new Zi5();
//        zi5.fun1();
    }
}
