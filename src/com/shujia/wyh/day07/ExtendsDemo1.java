package com.shujia.wyh.day07;

/*
    java中类与类之间的继承，不能多继承，只能单继承
    但是可以多层继承，形成继承体系。
 */
//class Father{}
//
//class Mather{}
//
//class Son extends Father{}

class GrandFather{
    public void fun1(){
        System.out.println("这是爷爷中的fun1方法");
    }
}

class Father extends GrandFather{}

class Son extends Father{}

public class ExtendsDemo1 {
    public static void main(String[] args) {

    }
}
