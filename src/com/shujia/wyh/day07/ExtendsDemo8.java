package com.shujia.wyh.day07;

/*
    super关键字的注意事项,子类中的super关键只能代表直接父类的引用，不能跨继承使用
 */

class Fu6{
    int a = 10;
}

class Zi6 extends Fu6{
    int a = 20;
}

class ZiZi6 extends Zi6{
    int a = 30;
    public void fun1(){
        System.out.println(a);
        System.out.println(super.a);
//        System.out.println(super.super.a);
        Fu6 fu6 = new Fu6();
        System.out.println(fu6.a);
//        System.out.println(Zi6.super.a);
    }
}

public class ExtendsDemo8 {
    public static void main(String[] args) {
        ZiZi6 ziZi6 = new ZiZi6();
        ziZi6.fun1();
    }
}
