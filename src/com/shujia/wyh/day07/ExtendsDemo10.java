package com.shujia.wyh.day07;

/*
    如果父类中的方法只想让子类去使用，而不是去重写，怎么办呢?
    java提供了一个关键字给我们使用：final 最终的，不可改变的
    final可以修饰
        类：修饰类，类不能被继承
        成员变量，局部变量：变量变成常量，不能重新赋值，自定义常量
        成员方法：修饰方法，方法可以被继承，但是不能被重写


 */

//final class Fu8{}

//class Zi8 extends Fu8{}

class Fu7 {
    final int a = 10;

    public final void fun1() {
        System.out.println("这是父类中的方法");
    }
}

class Zi7 extends Fu7 {
//    @Override
//    public void fun1(){
//        System.out.println("这是子类重写后的fun1");
//    }

    public void fun2() {
//        a = 20;
        System.out.println(a);
    }
}

public class ExtendsDemo10 {
    public static void main(String[] args) {
        Zi7 zi7 = new Zi7();
        zi7.fun2();
    }
}
