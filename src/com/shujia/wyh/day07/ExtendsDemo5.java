package com.shujia.wyh.day07;

/*
    继承与成员方法的关系
    当继承关系中子类出现与父类中的方法声明一摸一样，只是方法的实现不一样，这个现象叫做方法的重写
 */

class Fu4{
    public void fun1(){
        System.out.println("这是父类中的成员方法fun1");
    }
}

class Zi4 extends Fu4{
    public void fun2(){
        fun1();
    }

    @Override
    public void fun1(){
        System.out.println("这是子类中的成员方法fun1");
    }
}

public class ExtendsDemo5 {
    public static void main(String[] args) {
        Zi4 zi4 = new Zi4();
        zi4.fun2();
    }
}
