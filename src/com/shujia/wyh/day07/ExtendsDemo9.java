package com.shujia.wyh.day07;

/*
    猫狗案例讲解
    分析和实现

 */

class Animal{
    String name;
    int age;

    public void eat(){
        System.out.println("吃");
    }
}

class Cat extends Animal{
    @Override
    public void eat(){
        System.out.println("🐱吃🐟");
    }
}

class Dog extends Animal{
    @Override
    public void eat(){
        System.out.println("🐕吃🥩");
    }
}

public class ExtendsDemo9 {
    public static void main(String[] args) {
        //创建一只猫
        Cat cat = new Cat();
        cat.eat();
    }
}
