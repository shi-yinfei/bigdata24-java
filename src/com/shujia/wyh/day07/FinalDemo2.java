package com.shujia.wyh.day07;

/*
    final变量的赋值时机,在对象构造方法调用完毕之前,建议在创建final变量的时候，就给值。
 */
class Fu8 {
    final int a;

    {
        a=100;
    }

    Fu8() {
//        a = 100;
    }
}

public class FinalDemo2 {
    public static void main(String[] args) {
        Fu8 fu8 = new Fu8();
        System.out.println(fu8.a);
    }
}
