package com.shujia.wyh.day07.homework;




public class Car {
    private String type;
    private String carNo;
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getCarNo() {
        return carNo;
    }
    public void setCarNo(String carNo) {
        this.carNo = carNo;
    }
    public void start(){
        System.out.println("我是车，我启动");
    }
    public void stop() {
        System.out.println("我是车，我停止");
    }
}







