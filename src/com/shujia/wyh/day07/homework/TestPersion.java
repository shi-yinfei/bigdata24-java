package com.shujia.wyh.day07.homework;

public class TestPersion {
    public static void main(String[] args) {
        Person p = new Person();
        p.introduction();
        System.out.println("==============");
        Sportsman sm = new Sportsman("刘小翔",23,"男",
                "200米短跑","22秒30");
        sm.introduction();
        System.out.println("==============");
        Performer pf = new Performer("章依",27,"女","北京电影学院",
                "《寄往天国的家书》");
        pf.introduction();
    }
}
