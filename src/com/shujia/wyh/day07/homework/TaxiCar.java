package com.shujia.wyh.day07.homework;

public class TaxiCar extends Car {
    private String company;
    public TaxiCar(String company) {
        this.company = company;
    }
    public void start() {
        System.out.println("乘客您好\n我是"+this.company+
                "的，我的车牌号是"+this.getCarNo()+"，你要去那里？");
    }
    public void stop() {
        System.out.println("目的地已经到了，请您下车付款，欢迎再次乘坐");
    }
}