package com.shujia.wyh.day07.homework;

public class Performer extends Person {//演员类
    private String school;//学校
    private String production;//作品
    public Performer(){}
    public Performer(String name,int age,String sex,String school,String pro) {
        super(name,age,sex);
        this.school = school;
        this.production = pro;
    }
    public void introduction() {
        System.out.println("大家好，我是"+this.name);
        System.out.println("今年"+this.age);
        System.out.println("我毕业于："+this.school);
        System.out.println("代表作有："+this.production);
    }
}