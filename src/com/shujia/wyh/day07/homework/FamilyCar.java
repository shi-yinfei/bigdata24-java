package com.shujia.wyh.day07.homework;

public class FamilyCar extends Car {
    private String owner;
    public FamilyCar(String owner) {
        this.owner = owner;
    }
    public void start() {
        System.out.println("我是"+owner+"，我的汽车我做主");
    }
    public void stop() {
        System.out.println("目的地到了，我们去玩吧");
    }
}
