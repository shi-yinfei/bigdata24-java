package com.shujia.wyh.day07.homework;

public class Sportsman extends Person{//运动员类
    private String item;
    private String bestResult;
    public Sportsman(){}
    public Sportsman(String name,int age,String sex,String item,String best) {
        super(name,age,sex);
        this.item = item;
        this.bestResult = best;
    }
    public void introduction() {
        System.out.println("大家好，我是"+this.name);
        System.out.println("今年"+this.age);
        System.out.println("我擅长的项目是："+this.item);
        System.out.println("历史最好成绩是："+this.bestResult);
    }
}
