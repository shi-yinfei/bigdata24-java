package com.shujia.wyh.day07.homework;


public class Circle {
    double radius;
    public Circle() {}
    public Circle(double r) {
        this.radius = r;
    }
    double getArea() {
        double Area = Math.PI*radius*radius;
        return Area;
    }
    double getPerimeter() {
        double Perimeter = Math.PI*radius*2;
        return Perimeter;     }
    void show() {
        System.out.println("圆半径:"+radius);
        System.out.println("圆面积:"+getArea());
        System.out.println("圆周长:"+getPerimeter());
    }
}
class Cylinder extends Circle{
    double hight;
    public Cylinder(double radius,double h) {
        super(radius);
        this.hight = h;
    }
    double getVolume() {
        double v = super.getArea()*hight;
        return v;
    }
    void shouline() {
        System.out.println("圆柱体积:"+getVolume());
    }
    public static void main(String[] args) {
        Cylinder cylinder = new Cylinder(3,4);
        cylinder.show();         cylinder.shouline();
    }
}

