package com.shujia.wyh.day07.homework;

public class TestCar {
    public static void main(String[] args) {
        Car c = new Car();
        c.start();
        c.stop();
        System.out.println("===========================");
        FamilyCar fc = new FamilyCar("武大郞");
        fc.start();
        fc.stop();
        System.out.println("============================");
        TaxiCar tc = new TaxiCar("景顺出租公司");
        tc.setCarNo("京B123");
        tc.start();
        tc.stop();
    }
}