package com.shujia.wyh.day07;

/*
    final关键字使用的注意事项
 */
class Demo2{
    int a = 10;
}

public class FinalDemo1 {
    public static void main(String[] args) {
        final Demo2 demo2 = new Demo2();
        System.out.println(demo2.a);
        demo2.a = 40;
        System.out.println(demo2.a);
//        demo2 = new Demo2();
    }
}
