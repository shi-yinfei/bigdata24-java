package com.shujia.wyh.day07;
//将来不仅想要定义学生类和教师类，还想医生类，消防员类等等，都需要在定义类的时候重复写一样的属性和行为
//我就想，有没有什么方便的形式，将这些类相同的成员提取到一个类中去，然后其他类与这个类产生一个关系，其他就有该类中的成员了。
//java提供这样的关系机制：继承，使用extends关键字来实现
class Person{
    String name;
    int age;

    public void eat(){
        System.out.println("吃饭");
    }

    public void sleep(){
        System.out.println("睡觉");
    }
}

//学生类
//Student extends Person
//Student：子类或者派生类
//Person：父类或者基类或者超类
class Student extends Person{
    public void study(){
        System.out.println("学习");
    }
}


//class Student{
//    String name;
//    int age;
//
//    public void eat(){
//        System.out.println("吃饭");
//    }
//
//    public void sleep(){
//        System.out.println("睡觉");
//    }
//}
//
//class Teacher{
//    String name;
//    int age;
//
//    public void eat(){
//        System.out.println("吃饭");
//    }
//
//    public void sleep(){
//        System.out.println("睡觉");
//    }
//}


public class StudentDemo1 {
    public static void main(String[] args) {
        Student student = new Student();
        student.eat();
        student.sleep();
        student.study();
        System.out.println(student.name);
    }
}

