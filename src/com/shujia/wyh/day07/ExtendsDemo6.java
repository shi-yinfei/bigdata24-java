package com.shujia.wyh.day07;


/*
    面试题：重载和重写什么区别？
    1、重载是发生同一个类的内部，方法名一样，参数列表不一样的现象
    2、重写是发生在继承关系中的，子类中的方法与父类中的方法声明(指的是返回值类型，方法名，参数列表)一模一样，只是方法的实现不一样
 */

class OldPhone{
    public void call(){
        System.out.println("打电话");
    }

    public double getNumber(){
        return Math.random();
    }


}

class NewPhone extends OldPhone{
    @Override
    public void call() {
        super.call();
        System.out.println("边打电话边打游戏");
    }

    @Override
    public double getNumber(){
        return (int)(Math.random()*100+1);
    }

//    public double fun1(){
//        return super.getNumber();
//    }

    public void dingWei(){
        System.out.println("地图");
    }

}


public class ExtendsDemo6 {
    public static void main(String[] args) {
        NewPhone newPhone = new NewPhone();
        newPhone.call();
        double n = newPhone.getNumber();
        System.out.println(n);

        newPhone.dingWei();
    }
}
