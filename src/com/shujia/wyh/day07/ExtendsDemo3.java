package com.shujia.wyh.day07;

/*
    继承中成员变量的关系
 */

class Fu2{
//    int a = 10;
}

class Zi2 extends Fu2{
//    int a = 20;
    public void fun1(){
//        int a = 30;
//        System.out.println(a); //就近原则  现在本方法中寻找，找不到就去本类中成员位置，再去父类中去找
    }
}

public class ExtendsDemo3 {
    public static void main(String[] args) {
        Zi2 zi2 = new Zi2();
        zi2.fun1();
    }
}
