package com.shujia.wyh.day07;

/*
    使用继承的注意事项：
        1、子类无法继承父类中私有成员，就是被private修饰的成员
        2、构造方法无法被继承
 */
class Fu{
    int a = 10;
    private int b = 20;

    Fu(){}

    private void show(){
        System.out.println("这是父类中的私有方法");
    }
}

class Zi extends Fu{
    public void fun1(){
        System.out.println(a);
//        System.out.println(b);
    }
}

public class ExtendsDemo2 {
    public static void main(String[] args) {
        Zi zi = new Zi();
        zi.fun1();
//        zi.show();
    }
}
