package com.shujia.wyh.day06.ketang;

public class Person {
    //姓名 年龄 国籍
    private String name;
    private int age;
    private static String nationality;

    public Person() {
    }
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, String nationality) {
        this.name = name;
        this.age = age;
        this.nationality = nationality;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public void show() {
        System.out.println("----------------------------");
        System.out.println("姓名：" + name);
        System.out.println("年龄：" + age);
        System.out.println("国籍：" + nationality);
        System.out.println("----------------------------");
    }
}
