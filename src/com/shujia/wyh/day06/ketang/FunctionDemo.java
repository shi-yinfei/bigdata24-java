package com.shujia.wyh.day06.ketang;

public class FunctionDemo {
    public static void main(String[] args) {
        //创建Demo2类的对象
        Demo2 demo2 = new Demo2();

        //调用无参数无返回值的方法
        demo2.print();

        //调用无参数有返回值的方法
        String s = demo2.say();
        System.out.println(s);

        //调用有参数无返回值的方法
        demo2.call("杨志");

        //调用有参数有返回值的方法
        int i = demo2.fun1(10);
        System.out.println(i);

    }
}
