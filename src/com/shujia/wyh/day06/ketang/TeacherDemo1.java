package com.shujia.wyh.day06.ketang;

/*
    给对象成员变量赋值的两种赋值方式：
        1、使用setXxx()方法进行赋值
        2、使用带参数的构造方法在创建对象的时候赋值。
 */
public class TeacherDemo1 {
    public static void main(String[] args) {
        //创建一个教师对象
        Teacher t1 = new Teacher("小虎",18);
        t1.show();
    }
}
