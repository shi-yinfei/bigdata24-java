package com.shujia.wyh.day06.ketang;


/*
    一个标准类的3.0写法：
        成员变量：使用private关键字修饰
        构造方法：提供一个无参无方法体的构造方法和一个带参数的构造方法
        成员方法：getXxx()和setXxx()
        show():打印所有的成员变量值

 */
public class Teacher {
    private String name;
    private int age;

    public Teacher(){}

    public Teacher(String name,int age){
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void show() {
        System.out.println(name + "---" + age);
    }
}
