package com.shujia.wyh.day06.ketang;

/*
    定义一个长方形类,定义 求周长和面积的方法，然后定义一个测试了Test2，进行测试。
    注意：要想求周长或者面积，就必须先知道长和宽，而长和宽又可以用来描述一个长方形，所以这里的长和宽可以将其定义成 成员变量
 */
public class Rectangle {
    private int length;
    private int width;

    public Rectangle() {
    }

    public Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getPerimeter() {
        return (length + width) * 2;
    }

    public int getArea() {
        return length * width;
    }

}
