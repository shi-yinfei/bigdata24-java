package com.shujia.wyh.day06.ketang;

/*
    构造方法：创建对象时调用的方法
    语句定义格式：在类中方法外定义，
        修饰符 类名(){
            ...
        }
    注意：
        1、构造方法的名字和类名一样
        2、构造方法没有返回值，连void都没有，方法体中不能有return
        3、每次new的时候都会调用对应的构造方法
        4、构造方法也可以发生重载
 */
public class StudentDemo1 {
    public static void main(String[] args) {
        //创建一个学生对象
//        Student s1 = new Student();
////        student.show();
//
//        Student s2 = new Student();

//        Student s1 = new Student(); //调用无参的构造方法，会进行匹配对应构造方法
        Student s1 = new Student("杨志");
    }
}
