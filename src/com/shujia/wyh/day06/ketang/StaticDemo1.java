package com.shujia.wyh.day06.ketang;

/*
    static: 静态的
    可以修饰成员变量和成员方法：
        修饰成员变量，该类的对象共享一个静态的成员变量
        修饰成员方法，该类的对象共享一个静态的成员方法

    被static修饰的成员有什么特点：
        1、被static修饰的成员称之为类成员,可以直接通过类名的方式进行访问，不需要创建对象
        2、被static修饰的成员优先于对象存在，可以通过类名的方式进行访问，而没有使用static修饰的成员，必须通过创建的对象去访问
        3、静态的只能访问静态的，非静态的既可以访问静态也可以访问非静态的。

    注意事项：
        1、在static成员中是没有this关键字的。
        2、静态的只能访问静态的，非静态的既可以访问静态也可以访问非静态的。
 */

class Demo3 {
    int b = 5;
    static int a = 10;

    public void fun1(){
        System.out.println(a); //非静态的方法可以访问静态的成员
        System.out.println(b); //非静态的方法可以访问非静态的成员
    }


    public static void fun2(){
        System.out.println(a);  //静态的方法可以访问静态的成员
//        System.out.println(this.b);  //静态的方法不可以访问非静态的成员
    }



}

public class StaticDemo1 {
    public static void main(String[] args) {
//        Demo3 demo3 = new Demo3();
////        System.out.println(demo3.a);
//        System.out.println(Demo3.a);
//        demo3.fun1();

        Demo3.fun2();

        fun3();

    }


    public static void fun3(){
        System.out.println("hello World");
    }
}
