package com.shujia.wyh.day06.ketang;

/*
    代码块：顾名思义就是一批代码，只不过这一批代码语句被不同的大括号括起来了，叫做不同的代码块
    分类：
        1、局部代码块：在方法中被大括号括起来的代码块，叫做局部代码块
        2、构造代码块：在类中方法外定义(掌握)
        3、静态代码块：在类中方法外定义，需要被static修饰
        静态代码块--->构造代码块--->构造方法

        4、同步代码块（放到后面多线程的时候讲解）

 */
public class CodeDemo1 {
    //在类加载的时候，执行类中所有的静态代码块，自上而下执行
    //只会在程序运行最开始执行一次，往后都不会执行了。
    static {
        System.out.println("这是静态代码块 1");
    }

    //在类中方法外定义，叫做构造代码块,在创建对象的时候自动调用执行
    //如果类中有自己定义的构造方法，先执行构造代码块，再执行对应的构造方法
    //即便是类中存在多个构造代码块，也会先执行所有的构造代码块，然后再执行对应的构造方法，构造代码块之间自上而下的顺序执行的。
    {
        System.out.println("你好，这是构造代码块 1");
    }

    CodeDemo1() {
        System.out.println("这是我们自己定义的无参构造方法");
    }

    {
        System.out.println("你好，这是构造代码块 2");
    }

    static {
        System.out.println("这是静态代码块 2");
    }

    public static void main(String[] args) {
        //局部代码块,如果一个方法中有多个代码块，按照顺序结构执行
        //作用：限定变量的生命周期（作用域）
//        {
//            int a = 10;
//            System.out.println(a);
//        }
////        System.out.println(a);
//
//        {
//            int b = 20;
//            System.out.println(b);
//        }
//
//        {
//            int c = 30;
//            System.out.println(c);
//        }

        CodeDemo1 codeDemo1 = new CodeDemo1();
        System.out.println("=======================");
        CodeDemo1 codeDemo2 = new CodeDemo1();
        System.out.println("========================");
        {
            int c = 30;
            System.out.println(c);
        }
    }
}
