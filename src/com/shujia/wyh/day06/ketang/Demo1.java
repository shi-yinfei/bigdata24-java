package com.shujia.wyh.day06.ketang;

public class Demo1 {

//    public Demo1(){
//        System.out.println("这是我们自己提供的无参构造方法");
//    }

    public Demo1(String s){
        System.out.println("这是我们自己定义的带参数的构造方法");
    }

    public void fun1(){
        System.out.println("好好学习，天天向上！");
    }
}
