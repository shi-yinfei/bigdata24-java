package com.shujia.wyh.day06.ketang;

/*
        如何去定义工具类？
            1、构造方法私有化
            2、方法要是静态

        说明书--->帮助文档
        如何在写完一个工具类的同时生成对应的帮助文档呢？答案：文档注释
        如何使用文档注释呢？

 */
public class ArrayDemo {
    public static void main(String[] args) {
        int[] arr = {23, 45, 3, 243, 56, 2, 1, 34};
//
//        //需求1：求出数组中最大值
//        int maxNumber = getMaxNumber(arr);
//        System.out.println("数组中的最大值为：" + maxNumber);
        int maxNumber = ArrayTool.getMaxNumber(arr);
        System.out.println(maxNumber);


//        //需求：将数组逆序
//        int[] newArr = niXu(arr);
//        printArr(newArr);

    }


}
