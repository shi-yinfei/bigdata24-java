package com.shujia.wyh.day06.ketang;

/*
    定义一个类Demo,其中定义一个求两个数据和的方法，定义一个测试了Test，进行测试。
    对于这道题而言，第二种方式更加适合
    类是用来描述现实生活中的事物的，而最重要的是类中的成员变量或者成员方法是用来描述类的
    而a,b无法描述一个Demo,所以这道题而言，不太适合使用面向对象的思想编写


 */

class Demo {
//    public void sum(){
//        int a = 3;
//        int b = 4;
//        System.out.println(a+b);
//    }

    public void sum(int a, int b) {
        System.out.println(a + b);
    }


}

//使用面向对象的思想写这道题
//class Demo{
//    private int a;
//    private int b;
//
//    public Demo() {
//    }
//
//    public Demo(int a, int b) {
//        this.a = a;
//        this.b = b;
//    }
//
//    public int getA() {
//        return a;
//    }
//
//    public void setA(int a) {
//        this.a = a;
//    }
//
//    public int getB() {
//        return b;
//    }
//
//    public void setB(int b) {
//        this.b = b;
//    }
//
//    public void sum(){
//        System.out.println(a+b);
//    }
//}


public class Test1 {
    public static void main(String[] args) {
        Demo demo = new Demo();
        demo.sum(10,20);

//        Demo demo = new Demo(45, 55);
//        demo.sum();
    }
}
