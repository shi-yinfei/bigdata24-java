package com.shujia.wyh.day06.ketang;

public class RectangleDemo {
    public static void main(String[] args) {
        //创建一个长方形对象
        Rectangle rectangle = new Rectangle(10, 5);
        int length = rectangle.getPerimeter();
        System.out.println("周长为：" + length);
        int area = rectangle.getArea();
        System.out.println("面积为：" + area);
    }
}
