package com.shujia.wyh.day06.ketang;

public class PersonDemo {
    public static void main(String[] args) {
        //创建一个中国的明星
        Person p1 = new Person("蔡某某", 18, "中国");
        //创建另外一个中国明星
        Person p2 = new Person("陆澳", 21);
        //创建第三个中国明星
        Person p3 = new Person("李佳豪", 17);
        //....将来我们如果要创建许多个中国的明星的话，我们会发现，所有的对象共享一个国籍，那每次创建对象的时候，都要写一遍中国
        //java中提供了一个关键字给我们使用，代表共享的含义
        //static: 静态的
        p1.setNationality("俄罗斯");

        p1.show();
        p2.show();
        p3.show();

    }
}
