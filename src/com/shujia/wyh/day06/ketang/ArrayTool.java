package com.shujia.wyh.day06.ketang;

/**
 * 这个类是专门对数组做操作的，封装了大量了常见方法，例如求数组中的最大值，逆序等等...
 * @author 陆澳
 * @version v-1.0.0
 */
public class ArrayTool {
    private ArrayTool(){}

    /**
     * 该方法是以结构的方式打印一个int类型的数组
     * 例如：
     *   输出格式：[元素1,元素2,...,元素n]
     * @param arr 是形式参数的名字，将来接收int类型一维数组的地址值
     */
    public static void printArr(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (i == 0) {
                System.out.print("[" + arr[0] + ",");
            } else if (i == arr.length - 1) {
                System.out.println(arr[i] + "]");
            } else {
                System.out.print(arr[i] + ",");
            }
        }
    }

    /**
     * 该方法是实现一个int类型一维数组的元素逆序
     * 例如：
     * 输入：[1,2,3,4,5]
     * 输出：[5,4,3,2,1]
     * @param arr 是形式参数的名字，将来接收int类型一维数组的地址值
     * @return int[] 返回的是逆序后的一维数组的地址值
     */
    public static int[] niXu(int[] arr) {
        for (int start = 0, end = arr.length - 1; start < end; start++, end--) {
            int tmp = arr[start];
            arr[start] = arr[end];
            arr[end] = tmp;
        }
        return arr;
    }

    /**
     * 该方法可以获取一维int类型数组中的最大值
     * 例如：
     * 输入：[1,2,3,4,5]
     * 输出：5
     * @param arr 是形式参数的名字，将来接收int类型一维数组的地址值
     * @return int返回的是数组中的最大值
     */
    public static int getMaxNumber(int[] arr) {
        int maxNumber = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > maxNumber) {
                maxNumber = arr[i];
            }
        }
        return maxNumber;
    }
}
