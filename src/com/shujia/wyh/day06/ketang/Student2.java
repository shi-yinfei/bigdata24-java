package com.shujia.wyh.day06.ketang;

/*
    Student s = new Student();在内存中做了哪些事情?
    1. 加载Student.class文件进内存
    2. 在栈内存为s开辟空间
    3. 在堆内存为学生对象开辟空间
    4. 对学生对象的成员变量进行默认初始化
    5. 对学生对象的成员变量进行显示初始化
    6. 通过构造方法对学生对象的成员变量赋值
    7. 学生对象初始化完毕，把对象地址赋值给s变量

 */
public class Student2 {
    private String name = "小花";
    private int age = 18;

    public Student2() {
    }

    public Student2(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void show() {
        System.out.println(name + "---" + age);
    }
}
