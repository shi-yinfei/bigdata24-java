package com.shujia.wyh.day06.ketang;

public class Demo2 {
    //定义无参无返回值的方法
    public void print() {
        System.out.println("这是无参无返回值的方法");
    }

    //定义无参有返回值的方法
    public String say() {
        return "这是无参有返回值的方法";
    }

    //定义有参无返回值的方法
    public void call(String s) {
        System.out.println("这是带参数无返回值的方法" + s);
    }

    //定义有参数有返回值的方法
    public int fun1(int a) {
        return a + 10;
    }
}
