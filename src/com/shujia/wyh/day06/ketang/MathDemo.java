package com.shujia.wyh.day06.ketang;
/*
    使用帮助文档学习Math类：
        1、打开帮助文档，点击索引，搜索Math，回车
        2、看该类是属于哪一个包下的，今后如果发现一个类是属于java.lang包下的话，这个类在使用的时候不需要导包
        3、看这个类的解释，知道这个类是干什么的
        4、看成员
            成员变量Fields
            构造方法:如果一个类没有构造方法，意味着这个类是一个工具类，方法是静态
            成员方法:看参数和返回值，如何使用
 */
public class MathDemo {
    public static void main(String[] args) {
        //按住ctrl键，鼠标左击
//        Math
        System.out.println(Math.PI);
        //static int abs(int a)
        //返回值为 int绝对值。
        System.out.println(Math.abs(-10));
        //比较两个数中的最大
        System.out.println(Math.max(10,Math.max(3,56)));
    }
}
