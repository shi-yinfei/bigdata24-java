package com.shujia.wyh.day06.ketang;

import java.util.Scanner;

/*
    public static double random()返回一个double值为正号，大于等于0.0 ，小于1.0
 */
public class MathDemo2 {
    public static void main(String[] args) {
//        System.out.println(Math.random());
        //获取1-100
//        int number = (int) (Math.random() * 100 + 1);
////        System.out.println(number);
//        //创建键盘录入对象
//        Scanner sc = new Scanner(System.in);
        //定义一个标志位
//        boolean flag = true;

        //需求1:猜数字游戏
//        while (flag){
//            System.out.println("请输入您猜的数字：");
//            int num = sc.nextInt();
//            if (num > number) {
//                System.out.println("您猜大了，请往小的猜。。");
//            } else if (num < number) {
//                System.out.println("您猜小了，请往大的猜。。");
//            } else {
//                System.out.println("恭喜您！猜中了！数字是：" + number);
//                flag = false;
//            }
//        }

        //需求2：体育彩票（仅供上课案例使用，不参与线下赌博）
        //前区：5个数字 1-35
        //后区：2个数字 1-12

        for(int n=1;n<=5;n++){
            System.out.println("==================第"+n+"次随机生成==================");
            System.out.print("前区号：");
            for (int i = 1; i <= 5; i++) {
                int qianNum = (int) (Math.random() * 35 + 1);
                System.out.print(qianNum + "\t");
            }

            System.out.print("后区号：");
            for (int i = 1; i <= 2; i++) {
                int houNum = (int) (Math.random() * 12 + 1);
                System.out.print(houNum+"\t");
            }
            System.out.println();
        }
    }
}
