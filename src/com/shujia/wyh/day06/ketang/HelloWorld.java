package com.shujia.wyh.day06.ketang;

//定义一个权限最大的类，类名是HelloWorld
public class HelloWorld {
    //因为main方法将来要被JVM所调用，必须要保证权限最大
    //main是static的原因，因为在使用main之前，还没有对象，可以通过类名的方式直接调用
    //void表示main方法是没有返回值的
    //main是方法的名字，被JVM所识别
    //String[] 一维字符串类型的数组
    //args 形参的名字
    public static void main(String[] args) {
        //换行输出一句话
        System.out.println("Hello World!");

//        System.out.println(args.length);
        for (int i = 0; i < args.length; i++) {
            System.out.println(args[i]);
        }

    }
}
