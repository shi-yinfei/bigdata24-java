package com.shujia.wyh.day06.ketang;

/*
    使用构造方法的注意事项：
        我们在此之前并没有自己提供构造方法，但是呢，也可以进行创建对象，使用对象调用方法
        思考，我们没有提供构造方法，使用的是谁的呢？
    如果我们没有提供构造方法，在编译的时候会自动提供一个无参无方法体的构造方法
    如果我们自己提供了构造方法（不管是无参还是带参），系统将不会再提供了。

 */
public class GouZaoDemo1 {
    public static void main(String[] args) {
        Demo1 demo1 = new Demo1("杨志");
        demo1.fun1();
    }
}
