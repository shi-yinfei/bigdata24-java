package com.shujia.wyh.day06.ketang;

public class Student {
    private String name;
    private int age;


    //构造方法一般写在成员变量和成员方法之间
    //无参无方法体的构造方法
    public Student() {
        System.out.println("好好学习，天天向上");
    }

    public Student(String s) {
        System.out.println("这是带有一个字符串参数的构造方法" + s);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void show() {
        System.out.println(name + "---" + age);
    }
}
