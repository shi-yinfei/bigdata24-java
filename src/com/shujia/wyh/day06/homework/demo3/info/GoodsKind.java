package com.shujia.wyh.day06.homework.demo3.info;

/*
商品类别类：
属性：类别编号，类别名称
 */
public class GoodsKind {
    private int kId;
    private String kindName;

    public int getkId() {
        return kId;
    }

    public void setkId(int kId) {
        this.kId = kId;
    }

    public String getKindName() {
        return kindName;
    }

    public void setKindName(String kindName) {
        this.kindName = kindName;
    }
}
