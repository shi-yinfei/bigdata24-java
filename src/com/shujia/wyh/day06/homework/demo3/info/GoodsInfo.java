package com.shujia.wyh.day06.homework.demo3.info;

/*
    商品详细信息类：
属性：商品编号，商品名称，所属类别，商品数量（大于 0），商品价格（大于 0），
方法：盘点的方法，描述商品信息。内容包括商品名称，商品数量，商品价格，  现在商品总价以及所属类别信息
 */
public class GoodsInfo {
    private int gId;
    private String goodsName;
    private GoodsKind goodsKind;
    private int number;
    private double price;

    public int getgId() {
        return gId;
    }

    public void setgId(int gId) {
        this.gId = gId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public GoodsKind getGoodsKind() {
        return goodsKind;
    }

    public void setGoodsKind(GoodsKind goodsKind) {
        this.goodsKind = goodsKind;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        if (number <= 0) {
            System.out.println("库存数量异常，请联系管理员");
        } else {
            this.number = number;
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void show() {
        System.out.println("商品名称：" + goodsName);
        System.out.println("所属类别：" + goodsKind.getKindName());
        System.out.println("商品售价：" + price);
        System.out.println("库存数量：" + number);
        System.out.println("商品总价：" + (price * number));

    }
}
