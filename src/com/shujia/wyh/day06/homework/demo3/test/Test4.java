package com.shujia.wyh.day06.homework.demo3.test;

import com.shujia.wyh.day06.homework.demo3.info.GoodsInfo;
import com.shujia.wyh.day06.homework.demo3.info.GoodsKind;

/*
    4.某公司要开发名为”我爱购物狂”的购物网站，请使用面向对象的思想设计描述商  品信息
    要求：
    7)分析商品类别和商品详细信息属性和方法，设计商品类别类和商品详细信息类
    8)在商品详细信息类中通过属性描述该商品所属类别
    9)设置属性的私有访问权限，通过公有的 get,set 方法实现对属性的访问
    10)编写测试类，测试商品类别类和商品详细信息类的对象及相关方法（测试数据  信息自定）
    11)创建包 info—存放商品类别类和商品详细信息类，创建包 test—存放测试类参考分析思路:

 */
public class Test4 {
    public static void main(String[] args) {
        //创建一个洗发水类别对象
        GoodsKind goodsKind = new GoodsKind();
        goodsKind.setkId(1001);
        goodsKind.setKindName("洗发水");

        //创建第一件商品对象
        GoodsInfo g1 = new GoodsInfo();
        g1.setgId(1011);
        g1.setGoodsName("潘婷洗发水400ml");
        g1.setGoodsKind(goodsKind);
        g1.setPrice(40.5);
        g1.setNumber(16);
        g1.show();
        System.out.println("===============================");
        GoodsInfo g2 = new GoodsInfo();
        g2.setNumber(0);
        g2.setgId(1012);
        g2.setGoodsName("蜂花洗发水250ml");
        g2.setGoodsKind(goodsKind);
        g2.setPrice(11.5);
        g2.show();


    }
}
