package com.shujia.wyh.day06.homework.demo1;

//品种，年龄，心  情，名字；方法包括：叫，跑。
public class Dog {
    private String kind;
    private int age;
    private String mood;
    private String name;

    //快捷键生成getXxx()和setXxx()方法
    //alt+insert
    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        if ("心情很好".equals(mood) | "心情不好".equals(mood)) {
            this.mood = mood;
        } else {
            System.out.println("输入信息错误，这只狗狗今天心情很好!");
            this.mood = "心情很好";
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //跑
    public String run() {
        if ("心情很好".equals(mood)) {
            return "开心的围着主人身边转";
        } else {
            return "伤心的一动不动";
        }
    }

    //叫
    public String shout() {
        if ("心情很好".equals(mood)) {
            return "开心的汪汪叫";
        } else {
            return "伤心的呜呜叫";
        }
    }

    //show
    public void show() {
        System.out.println("名字叫" + name + "的" + kind + mood + "，" + run());
        System.out.println("名字叫" + name + "的" + kind + mood + "，" + shout());
    }


}
