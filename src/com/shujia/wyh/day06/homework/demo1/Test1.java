package com.shujia.wyh.day06.homework.demo1;
/*
    1.使用面向对象的思想，编写自定义描述狗的信息。设定属性包括：品种，年龄，心  情，名字；方法包括：叫，跑。
    要求：
    1)设置属性的私有访问权限，通过公有的 get,set 方法实现对属性的访问
    2)限定心情只能有“心情好”和“心情不好”两种情况，如果无效输入进行提示，  默认设置“心情好”。
    3)设置构造函数实现对属性赋值
    4)叫和跑的方法，需要根据心情好坏，描述不同的行为方式。
    5)编写测试类，测试狗类的对象及相关方法（测试数据信息自定义）  运行效果图:
 */
public class Test1 {
    public static void main(String[] args) {
        //创建第一只小狗
        Dog dog = new Dog();
        dog.setName("甜心");
        dog.setKind("贵宾犬");
        dog.setMood("五味杂陈");
        dog.show();
        System.out.println("===============================");
        Dog dog2 = new Dog();
        dog2.setName("太子");
        dog2.setKind("德国牧羊犬");
        dog2.setMood("心情不好");
        dog2.show();

    }
}
