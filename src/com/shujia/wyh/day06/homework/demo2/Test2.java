package com.shujia.wyh.day06.homework.demo2;

/*
    2.以面向对象的思想，编写自定义类描述 IT 从业者。设定属性包括：姓名，年龄， 技术方向，工作年限, 工作单位和职务；方法包括：工作
    要求：
    1)设置属性的私有访问权限，通过公有的 get,set 方法实现对属性的访问
    2)限定 IT 从业人员必须年满 15 岁，无效信息需提示，并设置默认年龄为 15。
    3)限定“技术方向”是只读属性(只提供 get 方法)
    4)工作方法通过输入参数，接收工作单位和职务，输出个人工作信息
    5)编写测试类，测试 IT 从业者类的对象及相关方法（测试数据信息自定义） 运行效果图:
 */
public class Test2 {
    public static void main(String[] args) {
        //创建第一个员工
        ITWorker itWorker = new ITWorker();
        itWorker.setName("马未龙");
        itWorker.setAge(35);
        itWorker.setJiShuKind("数据库维护");
        itWorker.setYears(10);
        itWorker.setWorkHouse("腾讯实业");
        itWorker.setJob("数据库维护工程师");
        itWorker.work();
        System.out.println("===============================");
        //创建第二个员工
        ITWorker worker2 = new ITWorker();
        worker2.setAge(5);
        worker2.setName("张凯");
        worker2.setYears(1);
        worker2.setJiShuKind("Java开发");
        worker2.setWorkHouse("鼎盛科技");
        worker2.setJob("Java开发工程师");
        worker2.work();

    }
}
