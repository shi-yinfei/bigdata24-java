package com.shujia.wyh.day06.homework.demo2;

public class ITWorker {
    //设定属性包括：姓名，年龄， 技术方向，工作年限, 工作单位和职务；方法包括：工作
    private String name;
    private int age;
    private String jiShuKind;
    private int years;
    private String workHouse;
    private String job;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age >= 15) {
            this.age = age;
        } else {
            System.out.println("年龄信息无效!已修改默认年龄为15");
            this.age = 15;
        }
    }

    public String getJiShuKind() {
        return jiShuKind;
    }

    public void setJiShuKind(String jiShuKind) {
        if (this.jiShuKind == null) {
            this.jiShuKind = jiShuKind;
        } else {
            System.out.println("已经存在技术方向");
        }
    }

    public int getYears() {
        return years;
    }

    public void setYears(int years) {
        this.years = years;
    }

    public String getWorkHouse() {
        return workHouse;
    }

    public void setWorkHouse(String workHouse) {
        this.workHouse = workHouse;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public void work() {
        System.out.println("姓名：" + name);
        System.out.println("年龄：" + age);
        System.out.println("技术方向:" + jiShuKind);
        System.out.println("技术年限：" + years);
        System.out.println("目前就职于：" + workHouse);
        System.out.println("职务：" + job);
    }
}
