package com.shujia.wyh.day05.ketang;

/*
    如何通过一个类创建对象?
    语句定义格式：
        类名 对象名 = new 类名();  //java中所有的类都是属于引用数据类型，包括自己定义的类

    java中所有的class类都会被编译生成一个class文件
 */
public class PersonDemo {
    public static void main(String[] args) {
        //根据Person类创建一个人的对象
        Person p1 = new Person();

        //如何使用对象调用成员变量呢？
        //成员变量：定义在类中方法外
        //对象名.成员变量名
//        p1.name; // 获取对象p1中成员变量name值
        String name = p1.name;
        System.out.println(name);
        System.out.println(p1.age);
        System.out.println(p1.gender);

        //给对象中的成员变量进行赋值
        p1.name = "杨志";
        p1.age = 18;
        p1.gender = "男";
        System.out.println(p1.name);
        System.out.println(p1.age);
        System.out.println(p1.gender);
        System.out.println("-----------------------------------");
        //如何使用对象中的成员方法呢？
        //对象名.方法名(...);
        p1.eat();
        p1.study("大数据");

    }
}
