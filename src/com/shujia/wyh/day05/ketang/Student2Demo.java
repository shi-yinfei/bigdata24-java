package com.shujia.wyh.day05.ketang;

/*
    匿名对象：顾名思义，就是该对象没有名字，用一次就扔
 */
public class Student2Demo {
    public static void main(String[] args) {
//        Demo d1 = new Demo();
//        Student2 s1 = new Student2();
        new Demo().show(new Student2());


    }
}
