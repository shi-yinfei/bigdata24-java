package com.shujia.wyh.day05.ketang;

public class Student4Demo {
    public static void main(String[] args) {
        //创建一个学生对象，对学生对象成员进行赋值
        Student4 s1 = new Student4();
//        s1.name = "小虎"; //外界无法直接获取被private修饰的成员
        s1.setName("小虎");
        s1.setAge(18);
        s1.setGender("男");
        s1.show();
        // 单个使用永远要比组合使用灵活度更高
        System.out.println(s1.getName() + "---" + s1.getAge() + "---" + s1.getGender());
    }
}
