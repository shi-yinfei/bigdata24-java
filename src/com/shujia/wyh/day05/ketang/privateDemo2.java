package com.shujia.wyh.day05.ketang;

/*
    private: 私有的
    private修饰成员变量
    private修饰成员方法
 */
class Demo2{
    private void fun1(){
        System.out.println("好好学习，天天向上！！");
    }

    public void fun2(){
        //方法可以嵌套使用，但是不能嵌套定义
        //可以借助公共的方法间接的调用私有的方法
        fun1();
    }
}

public class privateDemo2 {
    public static void main(String[] args) {
        //目前来说要想调用方法，必须通过对象去调用
//        fun1();
//        Demo2.fun1(); //目前还没有讲过这种方式
        Demo2 demo2 = new Demo2();
//        demo2.fun1();
        demo2.fun2();
    }
}
