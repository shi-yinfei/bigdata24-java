package com.shujia.wyh.day05.ketang;

/*
    一个标准类2.0版本写法
    学生类：
        成员变量：name,age,gender (使用private修饰所有的成员变量)
        成员方法：eat(),sleep()   (对每一个成员变量提供公共的getXxx()和setXxx()方法)
        show()：打印所有成员变量值的情况（临时，后面我们说完常用类的时候，会有另外一个方法替代）
 */
public class Student4 {
    private String name;
    private int age;
    private String gender;

    //对每一个成员变量提供公共的getXxx()和setXxx()方法
    public void setName(String s) {
        name = s;
    }

    public String getName() {
        return name;
    }

    public void setAge(int i) {
        age = i;
    }

    public int getAge() {
        return age;
    }

    public void setGender(String s) {
        gender = s;
    }

    public String getGender() {
        return gender;
    }

    public void show() {
        System.out.println(name + "---" + age + "---" + gender);
    }

}
