package com.shujia.wyh.day05.ketang;

public class Demo {
    //当你看到一个方法的参数数据类型是一个类的类型的时候，就说明将来调用时需要传入该类的对象
    public void show(Student2 s){
        s.fun1();
    }
}
