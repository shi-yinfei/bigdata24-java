package com.shujia.wyh.day05.ketang;

public class Animal {
    //姓名
    private String name;
    private int age;

    public void setName(String name) {
        //我们按照见名知意的原则，将局部变量的名字和成员变量名字修改成一样，修改后运行发现，我们自己赋的值并没有成功
        //运行时变量的查找规则：就近原则
        //我们的想法是将传进来的值name赋值给对象中的成员变量name
//        Animal.name = name; //使用使用类名的方式调用，我们目前并没有接触过这样的时候
        //java提供了一个关键字：this
        //this代表的是调用当前方法的对象
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public void setAge(int age) {
//        age = i;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    //show
    public void show() {
        System.out.println("姓名:" + name + ",年龄:" + age);
    }
}

class AnimalDemo{
    public static void main(String[] args) {
        //创建第一个动物
        Animal a1 = new Animal();
        a1.setName("旺财");
        a1.setAge(3);
        a1.show();

    }
}
