package com.shujia.wyh.day05.ketang;

/*
    人
        属性：姓名,年龄,性别..
        行为：吃，睡，玩，学习..
    java中不是所有的类都要进行运行的，如果只是定义一个类，定义类中的成员，不要加上main方法

    类的1.0版本写法，随着我们学习，总共有5个版本，最终的版本是我们开发时使用的版本。
 */
public class Person {
    //如何定义成员变量
    //数据类型 变量名;
    //姓名
    String name;
    int age;
    String gender;

    //如何定义成员方法
    //从现在开始，将static去掉
    //修饰符 返回值类型 方法名(参数列表){方法体;return 返回值;}
    //吃
    public void eat() {
        System.out.println("吃饭");
    }

    public void sleep() {
        System.out.println("睡觉");
    }

    public void study(String s) {
        System.out.println("学习" + s);
    }
}
