package com.shujia.wyh.day05.ketang;

/*
        教师：
            属性：姓名，年龄，性别
            行为：教书，学习
 */
public class Teacher {
    String name;
    int age;
    String gender;

    public void teach(String s) {
        int a = 10; //局部变量
        System.out.println("教" + s + "课程");
    }

    public void study() {
        System.out.println("学习");
    }
}

class TeacherDemo {
    public static void main(String[] args) {
        //创建第一个教师
        Teacher t1 = new Teacher();
        t1.name = "笑哥";
        t1.age = 18;
        t1.gender = "男";
        System.out.println("姓名：" + t1.name + ",年龄：" + t1.age + ",性别：" + t1.gender);
        t1.teach("flink");
        t1.study();
        System.out.println("---------------------------------------");
        //创建第二个教师，将第一个教师的地址值赋值给第二个教师
        Teacher t2 = t1;
        t2.age = 17;
        System.out.println("姓名：" + t1.name + ",年龄：" + t1.age + ",性别：" + t1.gender);
        System.out.println("姓名：" + t2.name + ",年龄：" + t2.age + ",性别：" + t2.gender);

    }
}
