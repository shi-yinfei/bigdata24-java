package com.shujia.wyh.day05.ketang;

/*
   我们按照正常地逻辑编写代码，先编写学生类，然后在测试类中创建对象，对成员变量进行赋值操作，看起来都没有问题
   但是我们可以对学生的年龄赋值一个不合常理的值
   于是，实际上如果要符合现实情况的话，非法的年龄应该是不允许赋值的，或者是赋值不成功
   应该在赋值之前先做一次判断，判断这个年龄是否合法，如果合法那么就进行赋值，反之不允许
   按道理说，判断这个逻辑肯定不是一句话就能搞定的，应该定义一个方法对成员变量进行赋值，方法中进行判断，将来如果我们要对年龄进行赋值的话
   就调用这个方法去赋值。
   我们写了方法之后，确实可以通过方法去限定年龄的范围，但是谁规定了你提供我就要去用呢？我偏偏不用方法去赋值依旧可以赋值非法的值。
   如果我们提供了方法后，让用户只能通过这个方法去赋值，不能让外界直接获取成员变量
   java提供了一个关键字给我们使用：private 私有的
   它可以修饰成员变量，成员方法，构造方法
   被private修饰的成员，外界无法进行直接访问
   说到现在，其实就是为了引出一个面向对象第一大特征：封装

   public是java中最大权限的权限修饰符
   private是java中最小权限的权限修饰（只能在本类中使用）


 */
class Student3 {
    String name;
    private int age;

    public void setAge(int i){
        if(i>0 & i<=100){
            age = i;
        }else {
            System.out.println("您输入的年龄不正常，默认是0");
        }
    }

    public int getAge(){
        return age;
    }


    public void eat() {
        System.out.println("吃饭");
    }

    public void sleep() {
        System.out.println("睡觉");
    }
}

public class PrivateDemo1 {
    public static void main(String[] args) {
        //创建一个学生对象，并对成员变量进行赋值
        Student3 s1 = new Student3();
        s1.name = "余杰";
//        s1.age = 1800;
        s1.setAge(18);
        System.out.println("姓名：" + s1.name + "，年龄：" + s1.getAge());
    }
}
