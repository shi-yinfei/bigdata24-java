package com.shujia.wyh.day05.ketang;

/*
    手机：
        属性：品牌，颜色，价格
        行为：打电话，发短信

    在一个文件中可以编写多个class类，将来编译产生的class文件，是由一个class类产生一个。
    当一个java文件中有多个class类的时候，只能有一个class类被public修饰，并且被public修饰的类名一定要与java文件的名字相同
 */
public class Phone {
    //属性---成员变量
    //品牌
    String brand;
    //颜色
    String color;
    //价格
    int price;

    //行为---成员方法
    public void call(String s) {
        System.out.println("打电话给：" + s);
    }

    //发短信
    public void sendMessage() {
        System.out.println("群发短信");
    }
}

class PhoneDemo {
    public static void main(String[] args) {
        //创建一个手机对象
        Phone p1 = new Phone();
        p1.brand = "华为";
        p1.color = "黑色";
        p1.price = 8999;
        System.out.println("手机的品牌是：" + p1.brand + ",颜色是：" + p1.color + ",价格是：" + p1.price);
        p1.call("马云");
        p1.sendMessage();
        System.out.println("-------------------------------------------------");
        //创建第二个手机
        Phone p2 = new Phone();
        p2.brand = "小米";
        p2.color = "蓝色";
        p2.price = 6999;
        System.out.println("手机的品牌是：" + p2.brand + ",颜色是：" + p2.color + ",价格是：" + p2.price);
        p2.call("杨老板");
        p2.sendMessage();

    }
}
