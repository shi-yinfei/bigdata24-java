package com.shujia.wyh.day05.homework;

/*
合并数组操作：现有如下一个数组： int oldArr[]={1,3,4,5,0,0,6,6,0,5,4,7,6,7,0,5}
要求将以上数组中值为0的项去掉，将不为0的值存入一个新的数组，
生成的新数组为： int newArr [] ={1,3,4,5,6,6,5,4,7,6,7,5}

分析：
    1、创建旧数组，存储元素
    2、遍历旧数组，得到不为0的元素个数
    3、创建新数组
    4、遍历旧数组，当元素不为0的时候，将元素添加到新数组中


 */
public class Test2 {
    public static void main(String[] args) {
        //1、创建旧数组，存储元素
        int[] oldArr = {1, 3, 4, 5, 0, 0, 6, 6, 0, 5, 4, 7, 6, 7, 0, 5};

        //2、遍历旧数组，得到不为0的元素个数
        int count = 0;
        for (int i = 0; i < oldArr.length; i++) {
            if (oldArr[i] != 0) {
                count++;
            }
        }

        //3、创建新数组
        int[] newArr = new int[count];

        //创建一个变量，表示新数组中的索引变化
        int index = 0;

        //4、遍历旧数组，当元素不为0的时候，将元素添加到新数组中
        for (int i = 0; i < oldArr.length; i++) {
            if (oldArr[i] != 0) {
                newArr[index] = oldArr[i];
                index++;
            }
        }

        for (int i = 0; i < newArr.length; i++) {
            if (i == 0) {
                System.out.print("[" + newArr[i] + ",");
            } else if (i == newArr.length - 1) {
                System.out.println(newArr[i] + "]");
            } else {
                System.out.print(newArr[i] + ",");
            }
        }

    }
}
