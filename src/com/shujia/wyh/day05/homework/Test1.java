package com.shujia.wyh.day05.homework;

import java.util.Scanner;

/*
    1.数组查找操作：定义一个长度为10 的一维字符串数组，在每一个元素存放一个单词；然后运行时从命令行输入一个单词，
    程序判断数组是否包含有这个单词，包含  这个单词就打印出“Yes”，不包含就打印出“No”。
 */
public class Test1 {
    public static void main(String[] args) {
        String[] arr = {"python", "linux", "mysql", "java", "maven-git", "jdbc", "redis", "clickhouse", "hadoop", "hive"};

        //创建键盘录入对象
        Scanner sc = new Scanner(System.in);

        System.out.println("请输入要查找的元素：");
        String s = sc.next();

        //定义一个标志位
        boolean flag = true;

        //遍历数组进行比较内容
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(s)) {
                System.out.println("Yes");
                flag = false;
                break;
            }
        }

        if(flag){
            System.out.println("No");
        }


    }
}
