package com.shujia.wyh.day05.homework;

import java.util.Scanner;

/*
    手动地输入8个数据，组成数组，然后查找元素是否存在数组中。
 */
public class Test4 {
    public static void main(String[] args) {
        //键盘录入对象
        Scanner sc = new Scanner(System.in);

        //创建长度为8的数组
        int[] arr = new int[8];

        for (int i = 0; i < arr.length; i++) {
            System.out.println("请输入第" + (i + 1) + "个数字：");
            int num = sc.nextInt();
            arr[i] = num;
        }
        System.out.println("排序之前：");
        printArray(arr);
        //冒泡排序
        maoPaoPaiXu(arr);
        System.out.println("排序之后：");
        printArray(arr);

        System.out.println("请输入您要查找的元素：");
        int number = sc.nextInt();
        boolean result = binarySearch(arr,0,arr.length-1,number);
        if (result){
            System.out.println("找到该元素！！");
        }else {
            System.out.println("数组中不存在该元素！！");
        }


    }

    public static boolean binarySearch(int[] arr,int front,int end,int number){

        while (front <= end) {
            int mid = (front + end) / 2;
            if (number > arr[mid]) {
                front = mid + 1;
            } else if (number < arr[mid]) {
                end = mid - 1;
            } else {
                return true;
            }
        }

        return false;

    }

    public static void maoPaoPaiXu(int[] arr){
        //但是不保证数组中的数据是有序的，所以要先进行冒泡排序，然后再二分查找
        //n个数据比较n-1轮次
        //每轮还要进行相邻的比较，每一轮都少比较一个
        //双重for循环解决，外层for循环实现比较的轮次，内层for循环实现每一轮中的比较次数
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
    }

    public static void printArray(int[] arr){
        for (int i = 0; i < arr.length; i++) {
            if (i == 0) {
                System.out.print("[" + arr[i] + ",");
            } else if (i == arr.length - 1) {
                System.out.println(arr[i] + "]");
            } else {
                System.out.print(arr[i] + ",");
            }
        }
    }


}
