package com.shujia.wyh.day14.sleepthread;

public class MySleepThread extends Thread{
    @Override
    public void run() {
        System.out.println("我开始睡觉了。。。");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("我醒了！！");
    }
}
