package com.shujia.wyh.day14.sleepthread;

/*
    休眠线程：Thread类中有一个静态方法sleep()
    休眠过程中的线程处于阻塞状态，并非运行状态，这时候其他线程就会抢到CPU执行权。
 */
public class SleepThreadDemo {
    public static void main(String[] args) {
        //创建一个线程对象
        MySleepThread t1 = new MySleepThread();
        MySleepThread2 t2 = new MySleepThread2();

        t1.start();
        t2.start();
    }
}
