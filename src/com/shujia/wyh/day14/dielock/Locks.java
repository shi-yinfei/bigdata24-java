package com.shujia.wyh.day14.dielock;

import java.util.concurrent.locks.ReentrantLock;

//造两把锁
public class Locks {
    public static final ReentrantLock LOCK1 = new ReentrantLock();
    public static final ReentrantLock LOCK2 = new ReentrantLock();
}
