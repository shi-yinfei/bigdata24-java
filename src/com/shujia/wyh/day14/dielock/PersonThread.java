package com.shujia.wyh.day14.dielock;

public class PersonThread extends Thread{
    private boolean flag;

    public PersonThread(boolean flag) {
        this.flag = flag;
    }

    @Override
    public void run() {
        if(flag){
            synchronized (Locks.LOCK1){
                System.out.println("if lock1");
                synchronized (Locks.LOCK2){
                    System.out.println("if lock2");
                }
            }
        }else {
            synchronized (Locks.LOCK2){
                System.out.println("else lock2");
                synchronized (Locks.LOCK1){
                    System.out.println("else lock1");
                }
            }
        }
    }
}
