package com.shujia.wyh.day14.dielock;

/*
    死锁：多个线程之间产生了互相等待的现象

    场景：
        中国人和外国人吃饭的问题
        前提：中国人吃饭必须要有两支筷子，外国人吃饭必须要有一把刀和一把叉子
        现在：
            中国人（一支筷子，一把刀）
            外国人（一支筷子，一把叉子）


 */
public class DieLockDemo {
    public static void main(String[] args) {
        //创建一个中国人
        PersonThread p1 = new PersonThread(true);
        //创建一个外国人
        PersonThread p2 = new PersonThread(false);

        //启动线程
        p1.start();
        p2.start();
    }
}
