package com.shujia.wyh.day14.stopthread;

public class MyStopThread extends Thread{
    @Override
    public void run() {
        System.out.println("我开始睡觉了。。。。");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("我醒了!!");
    }
}
