package com.shujia.wyh.day14.stopthread;

public class StopThreadDemo {
    public static void main(String[] args) {
        //创建一个线程对象
        MyStopThread t1 = new MyStopThread();
        t1.start();

        try {
            Thread.sleep(5000);
//            t1.stop(); //该方法已经被弃用了
            t1.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
