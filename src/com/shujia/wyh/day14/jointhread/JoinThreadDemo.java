package com.shujia.wyh.day14.jointhread;

public class JoinThreadDemo {
    public static void main(String[] args) {
        //创建一个线程对象
        MyJoinThread t1 = new MyJoinThread();  // Thread-0
        MyJoinThread t2 = new MyJoinThread();  // Thread-1
        MyJoinThread t3 = new MyJoinThread();  // Thread-2



        t1.start();
        try {
            t1.join(); //方法调用完start()之后使用，表示该线程一定是先执行，后面t1,t2都会等待该线程执行结束后再互相抢
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t2.start();
        t3.start();
    }
}
