package com.shujia.wyh.day14;

/*
    线程实现的方式2：实现Runnable接口，并且借助Thread来创建线程对象
 */
public class RunnableDemo1 {
    public static void main(String[] args) {
        //创建自定义的Runnable对象
        MyRunnable myRunnable = new MyRunnable();

//        myRunnable.start();

        //Thread(Runnable target, String name)
        //分配一个新的 Thread对象。
        //创建了一个线程对象
        Thread t1 = new Thread(myRunnable, "陆澳");
        //创建另一个线程对象
        Thread t2 = new Thread(myRunnable, "李佳豪");

        t1.start();
        t2.start();
    }
}
