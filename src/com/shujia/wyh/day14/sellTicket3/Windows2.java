package com.shujia.wyh.day14.sellTicket3;

import java.util.concurrent.locks.ReentrantLock;

public class Windows2 implements Runnable {
    int tickets = 100;
    Object obj = new Object();
    ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {
        while (true) {
            //相当于加了一把锁
            //t1,t2,t3
            //tickets = 1
            //t1
            lock.lock(); //加锁
            if (tickets > 0) {
                try {
                    //t1, t2, t3
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "窗口: 正在售卖第 " + (tickets--) + " 张票");
                //窗口1窗口: 正在售卖第 100 张票
                //窗口3窗口: 正在售卖第 100 张票
                //窗口2窗口: 正在售卖第 100 张票
                //...
                //窗口1窗口: 正在售卖第 1 张票
                //窗口2窗口: 正在售卖第 0 张票
                //窗口3窗口: 正在售卖第 -1 张票
            }
            lock.unlock(); //释放锁


        }
    }
}
