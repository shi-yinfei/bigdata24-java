package com.shujia.wyh.day14.sellTicket3;

/*
    使用Runnable的方式实现卖票

    为了模拟更加真实的卖票情况，我们适当的加入延迟
    问题1：当我们加入了循环和延迟之后，出现了重复的票和负数的票，0的票
        重复的票：在计算机内部中，小小的时间片足以运行很多次
        负数和0：这是因为线程执行具有随机性导致的，cpu执行是具有原子性
    线程安全的问题：如何判断一个程序是否存在线程安全的问题，从三个方面考虑，缺一不可。
        条件1：是否具备多线程环境？ 是 三个窗口线程
        条件2：是否有共享数据？ 是 共享数据是100张票
        条件3：是否有多条语句操作着共享数据？ 是
    解决方案1：java提供了一个关键字给我们使用：synchronized
        同步代码块
        这里的对象是三个线程对象共享且唯一的对象
        synchronized(对象){
            被多条语句操作的共享数据代码
        }
      同步方法的锁对象：是this
      静态同步方法的锁对象是：是当前类的class文件对象

   解决方案2：使用Lock锁进行加锁和释放锁
        ReentrantLock





 */
public class SellTicketDemo2 {
    public static void main(String[] args) {
        Windows2 windows2 = new Windows2();

        Thread t1 = new Thread(windows2, "窗口1");
        Thread t2 = new Thread(windows2, "窗口2");
        Thread t3 = new Thread(windows2, "窗口3");

        t1.start();
        t2.start();
        t3.start();

    }
}
