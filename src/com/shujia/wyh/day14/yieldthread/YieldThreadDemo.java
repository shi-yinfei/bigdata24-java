package com.shujia.wyh.day14.yieldthread;

/*
    礼让线程只是让线程之间看起来和谐一点，但是做不到你一次我一次
 */
public class YieldThreadDemo {
    public static void main(String[] args) {
        //创建一个线程对象
        MyYieldThread t1 = new MyYieldThread();
        MyYieldThread t2 = new MyYieldThread();
        t1.setName("陆澳");
        t2.setName("李佳豪");

        t1.start();
        t2.start();
    }
}
