package com.shujia.wyh.day14.yieldthread;

public class MyYieldThread extends Thread {
    @Override
    public void run() {
        for (int i = 1; i <= 200; i++) {
            System.out.println(getName() + "---" + i);
            Thread.yield();
        }
    }
}
