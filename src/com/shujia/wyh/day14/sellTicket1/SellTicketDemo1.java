package com.shujia.wyh.day14.sellTicket1;

/*
    某电影院目前正在上映贺岁大片，共有100张票，而它有3个售票窗口售票，请设计一个程序模拟该电影院售票。
    继承Thread类

 */
public class SellTicketDemo1 {
    public static void main(String[] args) {
        //创建三个窗口线程对象
        Windows1 windows1 = new Windows1();
        windows1.setName("窗口1");
        Windows1 windows2 = new Windows1();
        windows2.setName("窗口2");
        Windows1 windows3 = new Windows1();
        windows3.setName("窗口3");

        //启动线程开始售票
        windows1.start();
        windows2.start();
        windows3.start();
    }
}
