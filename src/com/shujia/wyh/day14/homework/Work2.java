package com.shujia.wyh.day14.homework;

import java.io.*;

public class Work2 {


        public static void main(String[] args) throws IOException,

                ClassNotFoundException {

            int num = 50;

            boolean flag = true;

            User user = new User("bjsxt","bjsxt");

            //使用数据包把数据封装起来

            //各种数据类型----->byte[]  ByteArrayOutputStream

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            ObjectOutputStream oos = new ObjectOutputStream(baos);//包装流

            oos.writeInt(num);

            oos.writeBoolean(flag);

            oos.writeObject(user);

            byte [] buf = baos.toByteArray();

            baos.close();

            //byte[]----------->各种数据类型

            ByteArrayInputStream bais = new ByteArrayInputStream(buf);

            ObjectInputStream ois = new ObjectInputStream(bais);

            int num2 = ois.readInt();

            boolean flag2 = ois.readBoolean();

            User user2 = (User)ois.readObject();

            System.out.println(num2);

            System.out.println(flag2);

            System.out.println(user2);

            bais.close();



        }

    }




