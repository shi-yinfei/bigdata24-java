package com.shujia.wyh.day14.homework;

import com.shujia.wyh.day14.sellTicket2.SellTicketDemo2;

import java.io.*;
import java.util.*;
/*
1.      实现字符串和字节数组之间的相互转换。必如将字符串“数加科技sjkj”转换为字节数组，并将字节数组再转换回字符串。
 */

//public class work1 {
//    public static void main(String[] args) throws IOException {
//        String a1 = "数加科技sjkj";
//        System.out.println(a1);
//        byte [] a2 = a1.getBytes();
//        String a3 = new String(a2, 0, a2.length);
//        System.out.println(a3);
//    }
//}
/*
2.     实现字节数组和任何基本类型和引用类型执行的相互转换  提示：使用ByteArrayInutStream和ByteArrayOutputStream。


 */
//public class work1 {
//    public static void main(String[] args) throws IOException,ClassNotFoundException,NotSerializableException{
//        int a = 10;
//        boolean flag = true;
//        User user = new User("glls","glls");
//        ByteArrayOutputStream bao = new ByteArrayOutputStream();
//        ObjectOutputStream oos = new ObjectOutputStream(bao);
//        oos.writeInt(a);
//        oos.writeBoolean(flag);
//        oos.writeObject(user);
//        byte [] b1 = bao.toByteArray();
//        bao.close();
//        ByteArrayInputStream bai = new ByteArrayInputStream(b1);
//        ObjectInputStream ois = new ObjectInputStream(bai);
//        int a2 = ois.readInt();
//        boolean flag2 = ois.readBoolean();
//        User user1 = (User)ois.readObject();
//        System.out.println(a2);
//        System.out.println(flag2);
//        System.out.println(user1);
//        bai.close();
//
//
//    }
//
//}
/*
3.     分别使用文件流和缓冲流复制一个长度大于100MB的视频文件，并观察效率的差异。
 */
//public class work1 {
//    public static void main(String[] args) {
//        copy();
//        copy2();
//    }

    //BufferedInputStream读取一个字节数组
    //BufferedOutputStream写一个字节数组
//    public static void copy() {
//        BufferedInputStream bis = null;
//        BufferedOutputStream bos = null;
//        try {
//            bis = new BufferedInputStream(new FileInputStream("C:\\Users\\DELL\\Desktop\\24期day14 序列化流 配置文件 1.mp4"));
//            bos = new BufferedOutputStream(new FileOutputStream("C:\\Users\\DELL\\Desktop\\24期day14 序列化流 配置文件 2.mp4"));
//            byte[] bytes = new byte[1024];
//            int length = 0;
//            long startTime = System.currentTimeMillis();
//            while ((length = bis.read(bytes)) != -1) {
//                bos.write(bytes, 0, length);
//                bos.flush();
//            }
//            long endTime = System.currentTimeMillis();
//            System.out.println("缓冲流复制结束，共耗时：" + (endTime - startTime) + "毫秒");
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (bos != null) {
//                try {
//                    bos.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (bis != null) {
//                try {
//                    bis.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//    public static void copy2() {
//        FileInputStream fis = null;
//        FileOutputStream fos = null;
//        try {
//            fis = new FileInputStream("C:\\Users\\DELL\\Desktop\\24期day14 序列化流 配置文件 1.mp4");
//            fos = new FileOutputStream("C:\\Users\\DELL\\Desktop\\24期day14 序列化流 配置文件 3.mp4");
//
//            byte[] bytes = new byte[1024];
//            int length = 0;
//            long startTime = System.currentTimeMillis();
//            while ((length = fis.read(bytes)) != -1) {
//                fos.write(bytes, 0, length);
//            }
//
//            long endTime = System.currentTimeMillis();
//
//            System.out.println("文件流复制结束，共耗时：" + (endTime - startTime) + "毫秒");
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (fis != null) {
//                try {
//                    fis.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            if (fos != null) {
//                try {
//                    fos.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//    }
//}
/*
4.      复制文件夹d:/java下面所有文件和子文件夹内容到d:/java2。 提示：涉及单个文件复制、 目录的创建、递归的使用
 */
//    public class work1 {
//        /**
//         * 复制单个文件
//         * @param sourceFile 源文件
//         * @param targetFile 目标文件
//         * @throws IOException
//         */
//        public static void copyFile(File sourceFile, File targetFile) throws IOException {
//            BufferedInputStream inBuff = null;
//            BufferedOutputStream outBuff = null;
//            try {
//                // 新建文件输入流
//                inBuff = new BufferedInputStream(new FileInputStream(sourceFile));
//                // 新建文件输出流
//                outBuff = new BufferedOutputStream(new FileOutputStream(targetFile));
//                // 缓冲数组
//                byte[] b = new byte[1024 * 5];
//                int len;
//                while ((len = inBuff.read(b)) != -1) {
//                    outBuff.write(b, 0, len);
//                }
//                // 刷新此缓冲的输出流
//                outBuff.flush();
//            } finally {
//                // 关闭流
//                if (inBuff != null)
//                    inBuff.close();
//                if (outBuff != null)
//                    outBuff.close();
//            }
//        }
//
//        /**
//         * 复制目录
//         * @param sourceDir 源目录
//         * @param targetDir 目标目录
//         * @throws IOException
//         */
//        public static void copyDirectiory(String sourceDir, String targetDir)
//                throws IOException {
//            // 检查源目录
//            File fSourceDir = new File(sourceDir);
//            if(!fSourceDir.exists() || !fSourceDir.isDirectory()){
//                System.out.println("源目录不存在");
//                return;
//            }
//            //检查目标目录，如不存在则创建
//            File fTargetDir = new File(targetDir);
//            if(!fTargetDir.exists()){
//                fTargetDir.mkdirs();
//            }
//            // 遍历源目录下的文件或目录
//            File[] file = fSourceDir.listFiles();
//            for (int i = 0; i < file.length; i++) {
//                if (file[i].isFile()) {
//                    // 源文件
//                    File sourceFile = file[i];
//                    // 目标文件
//                    File targetFile = new File(fTargetDir, file[i].getName());
//                    copyFile(sourceFile, targetFile);
//                }
//                //递归复制子目录
//                if (file[i].isDirectory()) {
//                    // 准备复制的源文件夹
//                    String subSourceDir = sourceDir + File.separator + file[i].getName();
//                    // 准备复制的目标文件夹
//                    String subTargetDir = targetDir + File.separator + file[i].getName();
//                    // 复制子目录
//                    copyDirectiory(subSourceDir, subTargetDir);
//                }
//            }
//        }
//        public static void main(String[] args) throws IOException {
//            copyDirectiory("e:/数加/java/day01","e:/数加/java/day1");
//        }
//    }
/*
1.使用IO包中的类读取D盘上exam.txt文本文件的内容，每次读取一行内容，
将每行作为 一个输入放入ArrayList的泛型集合中并将集合中的内容使用加强for进行输出显示。
 */
//    public class work1 {
//        public static void main(String[] args) throws IOException {
//            String path="E:\\数加\\java\\再别康桥.txt";
//            outputMethod(path);
//        }
//        public static  void outputMethod(String path) throws IOException {
//            List<String> list = new ArrayList<String>(); // 创建集合对象
//            // 创建缓冲区对象
//            BufferedReader br = new BufferedReader(new FileReader(path));
//            String line = br.readLine(); // 读取数据每次读一行
//            while (line != null) {
//                list.add(line);
//                line = br.readLine();
//            }
//            br.close();              //关闭
//            for(String s:list){
//                System.out.println(s);
//            }
//        }
//    }

//public class work1 {
//    private int count;
//    /**
//     * 统计一个java文件的行数
//     */
//    private void countLine(File sourceFile) throws IOException {
//        BufferedReader br = null;
//        try {
//            // 新建文件输入流
//            br = new BufferedReader(new FileReader(sourceFile));
//            while(br.readLine()!=null){
//                count++;
//                //System.out.println(count);
//            }
//        } finally {
//            br.close();
//        }
//    }
//    /**
//     * 统计一个目录下所有Java文件的行数
//     */
//    private void countDir(String sourceDir) throws IOException {
//        // 检查源目录
//        File fSourceDir = new File(sourceDir);
//        if(!fSourceDir.exists() || !fSourceDir.isDirectory()){
//            System.out.println("源目录不存在");
//            return;
//        }
//        // 遍历目录下的文件或目录
//        File[] file = fSourceDir.listFiles();
//        for (int i = 0; i < file.length; i++) {
//            if (file[i].isFile()) {
//                if(file[i].getName().toLowerCase().endsWith(".java")){
//                    // System.out.println(file[i].getName());
//                    countLine(file[i]);
//                }
//            }
//            //递归统计代码行数
//            if (file[i].isDirectory()) {
//                // 准备统计的文件夹
//                String subSourceDir = sourceDir + File.separator + file[i].getName();
//                // 统计子目录
//                countDir(subSourceDir);
//            }
//        }
//    }
//    public static void main(String[] args) throws IOException {
//
//        work1 tcd = new work1();
//        tcd.countDir("e:/数加/java/day01");
//        System.out.println(tcd.count);
//    }
//}
/*
4.由控制台按照固定格式输入学生信息，包括学号，姓名，年龄信息，当输入的内容为exit 退出；将输入的学生信息分别封装到一个Student对象中，
再将每个Student对象加入到 一个集合中，要求集合中的元素按照年龄大小正序排序；最后遍历集合，将集合中学生 信息写入到记事本，每个学生数据占单独一行。
 */
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.Set;
//2、由控制台按照固定格式输入学生信息，包括学号，姓名，年龄信息，当输入的内容为exit退出；
// 将输入的学生信息分别封装到一个Student对象中，
// 再将每个Student对象加入到一个集合中，
// 要求集合中的元素按照年龄大小正序排序；
// 最后遍历集合，将集合中学生信息写入到记事本，每个学生数据占单独一行
public class work1 {
    public work1(int i, String s, int i1) {
    }

    static class Student implements Comparable{
        String index;//学号
        String name;//姓名
        int age;//年龄信息
        public Student(String index,String name,int age){
            this.index = index;//编号
            this.name = name;//名称
            this.age = age;//年龄
        }
        public String getIndex(){
            return this.index;
        }
        public void setIndex(String index){
            this.index = index;
        }
        public String getName(){
            return this.name;
        }
        public void setName(String name){
            this.name = name;
        }
        public int getAge(){
            return this.age;
        }
        public void setAge(int age) {
            this.age = age;
        }
        public int compareTo(Object o) {
            if(!(o instanceof Student)){
                throw new RuntimeException("不是Student对象");
            }
            Student p = (Student) o;
            if(this.age > p.age){
                return 1;
            }else if(this.age == p.age){
                return this.name.compareTo(p.name);
            }else{
                return -1;
            }
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Set<Student> students = new TreeSet<Student>();
        while(true){
            String index = input.next();
            if("exit".equals(index)){
                break;
            }
            String name = input.next();
            int age = input.nextInt();
            Student x = new Student(index,name,age);
            students.add(x);
        }
        Iterator<Student> it = students.iterator();
        while(it.hasNext()){
            Student student = (Student)it.next();
            System.out.println("name:" + student.getName());
        }
    }
}

