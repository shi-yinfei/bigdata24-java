package com.shujia.wyh.day14.daemonthread;

public class DaemonThreadDemo {
    public static void main(String[] args) {
        //创建一个线程对象
        MyDaemonThread t1 = new MyDaemonThread();
        MyDaemonThread t2 = new MyDaemonThread();
        MyDaemonThread t3 = new MyDaemonThread();

        //后台线程在别的资料中称之为守护线程
        //如果没有了用户线程，守护线程就没有存在的意义了。
        t1.setName("刘备");
        t2.setName("关羽");
        t3.setName("张飞");

        t2.setDaemon(true);
        t3.setDaemon(true);

        t1.start();
        t2.start();
        t3.start();
    }
}
