package com.shujia.wyh.day14.daemonthread;

public class MyDaemonThread extends Thread {
    @Override
    public void run() {
        for (int i = 1; i <= 200; i++) {
            System.out.println(getName() + "--" + i);
        }
    }
}
