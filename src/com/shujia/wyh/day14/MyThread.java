package com.shujia.wyh.day14;

public class MyThread extends Thread {
    public MyThread() {
    }

    public MyThread(String name) {
        super(name);
    }

    //重写run方法

    //将来创建该线程类对象后，启动线程的时候，JVM会单独为这个线程对象开辟空间，启动线程，调用线程类中的run方法执行
    @Override
    public void run() {
        for (int i = 1; i <= 200; i++) {
            //如何获取线程的名字呢？
            //父类Thread类中有一个方法叫做getName()
            System.out.println(getName() + "---" + i);
        }
    }
}
