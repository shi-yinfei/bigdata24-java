package com.shujia.wyh.day14;

import java.io.*;
import java.util.Properties;
import java.util.Scanner;

/*
    登录注册案例：java版不和数据连接
 */
public class LoginRegistDemo1 {
    public static void main(String[] args) throws IOException {
        //创建一个键盘录入对象
        Scanner sc = new Scanner(System.in);

        //创建一个配置文件对象 用于读取我们配置文件中的用户名和密码
        //Properties()
        //创建一个没有默认值的空属性列表。
        Properties prop = new Properties();
        prop.load(new BufferedReader(new FileReader("src/com/shujia/wyh/day14/userInfo.properties")));

        //根据key获取对应的value值
        String name = prop.getProperty("username");
        String pwd = prop.getProperty("password");


        System.out.println("============欢迎进入陆澳婚恋所============");
        System.out.println("请输入您的用户名：");
        String username = sc.next();
        System.out.println("请输入您的密码：");
        String password = sc.next();

        //xiaohu 123456
        if (name.equals(username)) {
            if (pwd.equals(password)) {
                System.out.println("登录成功！祝您早日脱单！");
            } else {
                System.out.println("密码错误，请重新输入密码：");
                boolean flag = true;
                for (int i = 3; i > 0; i--) {
                    String pwd3 = sc.next();
                    if ("123456".equals(pwd3)) {
                        System.out.println("登录成功！祝您早日脱单！");
                        flag = false;
                        return;
                    }
                    System.out.println("密码错误，您还剩下" + (i - 1) + "次机会！");
                }
                if (flag) {
                    System.out.println("对不起，您的3次机会已用完，请过1小时后重新登录！！");
                }
            }
        } else {

            System.out.println("您的账户不存在，请先注册！");
            System.out.println("是否要进行注册？（Y/N）:");
            String flag = sc.next();
            if ("Y".equals(flag)) {
                System.out.println("欢迎新用户注册！！");
                System.out.println("请输入您的用户名：");
                String user2 = sc.next();
                System.out.println("请输入您的密码：");
                String pwd2 = sc.next();
                //创建字符输入流对象
                BufferedWriter bw = new BufferedWriter(new FileWriter("src/com/shujia/wyh/day14/userInfo.properties"));

                bw.write("username=" + user2);
                bw.newLine();
                bw.write("password=" + pwd2);
                bw.flush();
                System.out.println("注册成功并登录！！正在跳转到首页！！");
            } else if ("N".equals(flag)) {
                System.out.println("欢迎下次使用！！");
            } else {
                System.out.println("您输入操作有误，请等候5分钟后操作！");
            }


        }
    }
}
