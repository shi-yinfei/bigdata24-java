package com.shujia.wyh.day14;

/*
    java提供了一个类Thread用于描述线程和让我们创建一个线程对象
    java实现多线程的方式：
        1、继承Thread类


        2、实现Runnable接口
        3、实现Callable接口，结合线程池使用

   Thread: 每个线程都有优先权。 具有较高优先级的线程优先于优先级较低的线程执行。

 */
public class ThreadDemo1 {
    public static void main(String[] args) {
        //Thread.currentThread()获取当前方法所在的线程，getName()获取线程名字
        System.out.println(Thread.currentThread().getName());  //主线程名字叫做main，和我们的main方法重名

        //创建一个线程对象
        //通过构造方法给线程起名字，注意，自己的线程子类中要有对应的构造方法
        MyThread t1 = new MyThread("陆澳"); // Thread-0
        //多线程环境下，要创建多个线程
        MyThread t2 = new MyThread("李佳豪"); // Thread-1

        //给线程起名字，通过父类Thread类中的setName()方法进行设置名字
//        t1.setName("陆澳");
//        t2.setName("李佳豪");

        //启动线程
        //如果只是单纯的调用run方法，和普通对象调用成员方法没有任何区别，依旧是单线程程序
//        t1.run();
//        t2.run();

        //设置线程的优先级
        //public final void setPriority(int newPriority)更改此线程的优先级。
        //[1-10]
        //优先级高的不一定先执行，线程的执行具有随机性
        t1.setPriority(1);
        t2.setPriority(10);

        //默认线程优先级别是多少呢？
        //获取线程的优先级public final int getPriority()
        System.out.println("t1线程的优先级："+t1.getPriority());
        System.out.println("t2线程的优先级："+t2.getPriority());

        //而我们想要的效果是t1和她t2两个线程应该是互相抢CPU的执行权，谁先打印完200不确定
        //真正的调用线程应该是线程类调用start方法去启动线程
        //调用start方法只是表明该线程具有执行资格
        t1.start();
        t2.start();





    }
}
