package com.shujia.wyh.day14;

import java.io.Serializable;

/*
    将来序列化的时候，不希望将某个成员也序列化到文件中
    java提供了一个关键字，transient
 */
public class Student implements Serializable {
    private static final long serialVersionUID = -6240731508954360001L;
    //姓名，年龄
    private String name;
    int age;
    private transient int yinHangId;

    public Student() {
    }

    public Student(String name, int age, int yinHangId) {
        this.name = name;
        this.age = age;
        this.yinHangId = yinHangId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getYinHangId() {
        return yinHangId;
    }

    public void setYinHangId(int yinHangId) {
        this.yinHangId = yinHangId;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", yinHangId=" + yinHangId +
                '}';
    }
}
