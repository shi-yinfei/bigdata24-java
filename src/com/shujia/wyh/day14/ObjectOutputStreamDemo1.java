package com.shujia.wyh.day14;

import java.io.*;

/*
    序列化：将对象像数据流一样在网络中传输  ObjectOutputStream
    反序列化：将网络中数据流还原成一个对象  ObjectInputStream


    我们按照正常的思路编写代码后发现出错了：NotSerializableException
    不允许序列化，java中所有的类都可以进行序列化
    java中只有实现了 Serializable接口的类的对象，才可以进行序列化，像Serializable接口这样的没有常量也没有抽象方法的叫做标记接口

    我们写入对象结束后，开始读取对象，第一次读取没有问题，但是当我们修改了对象类里面的内容之后，发现读不出来了，并且出错
    com.shujia.wyh.day14.Student;
    local class incompatible:
        stream classdesc serialVersionUID = 1971310093833349592,
        local class serialVersionUID = -6240731508954360001
 */
public class ObjectOutputStreamDemo1 {
    public static void main(String[] args) throws Exception {
//        write();
        read();
    }

    public static void read(){
        ObjectInputStream ois = null;
        try {
            //创建对象输入流
            ois = new ObjectInputStream(new FileInputStream("obj.txt"));
            Object o = ois.readObject();
            System.out.println(o);

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void write(){
        ObjectOutputStream oos = null;
        try {
            //创建一个对象
            Student s1 = new Student("王宇杰", 18, 1234567);

            //创建对象输出流
            oos = new ObjectOutputStream(new FileOutputStream("obj.txt"));

            oos.writeObject(s1);
            oos.flush();



        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                oos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
