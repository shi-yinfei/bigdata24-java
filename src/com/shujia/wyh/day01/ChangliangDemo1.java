package com.shujia.wyh.day01;

/*
    常量：在java程序运行过程中，其值不可以改变的量

    分类：
        字面值常量
            1）字符串常量   使用双引号括起来的值整体叫做字符串常量 "abc"
            2）整数常量  10 -10
            3）小数常量  1.23 3.14 -1.45
            4）字符常量  使用单引号括起来的单个字符叫做字符常量
            5）布尔常量  两个值 true false
            6）空常量    null(放到数组的时候讲解)
        自定义常量（放到面向对象的时候讲解）

 */
public class ChangliangDemo1 {
    //快捷键：生成main方法 psvm
    public static void main(String[] args) {
        //打印一个字符串常量
        //快捷键：生成输出语句方法 sout
        System.out.println("abc");
        //打印一个整数常量
        System.out.println(10);
        //打印一个字符常量
        System.out.println('a');
        //打印一个布尔常量
        System.out.println(true);
    }
}
