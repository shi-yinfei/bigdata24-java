package com.shujia.wyh.day01;

/*
    注释：就是用于解释说明的文字，不会被JVM解释运行
    java中有哪些注释？
        1、单行注释  // 注释的文字
        2、多行注释  /星 注释的文字 星/
        3、文档注释  /星星  注释的文字  星/

    需求：对之前的HelloWorld案例加上注释

    注释的好处：
        1、方便我们和别人去理解代码
        2、方便排错  后面会学习一个新的排错技术：debug
 */

//定义一个类，类的名字是ZhuShiDemo1
public class ZhuShiDemo1 {
    //这是一个main方法，是java程序的运行入口，固定写法，将来会被JVM所识别调用
    public static void main(String[] args){
        //这是使用java中的输出语句 输出一行话：Hello World !

        //快捷键：批量注释代码 ctrl + /
        System.out.println("Hello World !");
        System.out.println("Hello World !");
//        System.out.println("Hello World !");
//        System.out.println("Hello World !");
//        System.out.println("Hello World !);
//        //。。。
//        System.out.printl("Hello World !");

    }
}
