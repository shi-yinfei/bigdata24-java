package com.shujia.wyh.day01;

/*
    计算中所有数据的运算都是采用补码的形式计算的。
    而要想知道一个数的补码，就必须知道其反码，而要想知道反码，就必须知道其原码
    而原码，反码，补码都是二进制的表示法

    举例：6+(-3)
    步骤1：算出所有数据的二进制表示
        6的二进制：00000110  --->  原码：00000110
        3的二进制：00000011  --->  原码：10000011   负数的原码最高位是1
          符号位（最高的一位，最左边的那一位）          数值位
    原码：        0                                0000110
                 1                                0000011

    反码：正数的反码和原码一样，负数的反码是原码符号位不变，数值位按位取反
                符号位           数值位
    6的反码：      0             0000110
    -3的反码：     1             1111100

    补码：正数的补码和原码一样，负数的补码是反码末位+1
                符号位           数值位
    6的补码：      0             0000110
    -3的补码：     1             1111101

    补码：         0             0000011   ===> 3






 */
public class ChangliangDemo3 {
}
