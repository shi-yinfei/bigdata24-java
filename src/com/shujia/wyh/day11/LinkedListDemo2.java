package com.shujia.wyh.day11;

import java.util.LinkedList;

/*
    因为LinkedList底层是链表，所以根据链表的特点，提供了几个特有的方法
    public void addFirst(E e)及addLast(E e)
    public E getFirst()及getLast()
    public E removeFirst()及public E removeLast()

 */
public class LinkedListDemo2 {
    public static void main(String[] args) {
        //创建集合对象
        LinkedList linkedList = new LinkedList();

        //添加元素
        linkedList.add("hello");
        linkedList.add("world");
        linkedList.add("java");
        linkedList.add("hadoop");
        linkedList.add("hive");

        //public void addFirst(E e)及addLast(E e)
//        linkedList.addFirst("mysql");
//        linkedList.addLast("spark");

//        //public E getFirst()及getLast()
//        System.out.println(linkedList.getFirst());
//        System.out.println(linkedList.getLast());

        //public Object removeFirst()及public Object removeLast()
//        System.out.println(linkedList.removeFirst());
//        System.out.println(linkedList.removeLast());
//
//        System.out.println(linkedList);


//        System.out.println(linkedList);


    }
}
