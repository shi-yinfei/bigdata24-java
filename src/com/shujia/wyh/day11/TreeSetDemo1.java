package com.shujia.wyh.day11;

import java.util.TreeSet;

/*
    TreeSet集合：是Set接口的实现子类
    底层数据结构是红黑树，自平衡二叉树，存储在集合中的元素进行按照我们想要的顺序进行排序
    TreeSet中排序方式：
        1、自然排序
        2、比较器排序

 */
public class TreeSetDemo1 {
    public static void main(String[] args) {
        //创建集合对象
        TreeSet<Integer> treeSet = new TreeSet<>();

        //向集合中添加元素
        treeSet.add(12);
        treeSet.add(65);
        treeSet.add(23);
        treeSet.add(17);
        treeSet.add(3);
        treeSet.add(58);

        System.out.println(treeSet);
    }
}
