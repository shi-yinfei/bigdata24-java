package com.shujia.wyh.day11;

import java.util.ArrayList;

/*
    泛型通配符<?>
    任意类型，如果没有明确，那么就是Object以及任意的Java类了
    ? extends E
    向下限定，E及其子类
    ? super E
    向上限定，E及其父类

 */
public class FanXingDemo1 {
    public static void main(String[] args) {
//        ArrayList<Animal> animals = new ArrayList<Animal>();

//        ArrayList<?> animals = new ArrayList<Animal>();
//        ArrayList<?> animals1 = new ArrayList<Dog>();
//        ArrayList<?> animals2 = new ArrayList<Cat>();

//        ArrayList<? extends Animal> animals2 = new ArrayList<Cat>();
//        ArrayList<? extends Animal> animals3 = new ArrayList<Dog>();
//        ArrayList<? extends Animal> animals4 = new ArrayList<Animal>();
//        ArrayList<? extends Animal> animals5 = new ArrayList<Object>();

        ArrayList<? super Animal> animals5 = new ArrayList<Object>();
        ArrayList<? super Animal> animals6 = new ArrayList<Animal>();
//        ArrayList<? super Animal> animals7 = new ArrayList<Dog>();

        // 表示将来传入的参数是一个Collection集合类型，并且Collection集合中的元素类型要是E的本身或者子类类型
        //boolean addAll(Collection<? extends E> c)
    }
}
