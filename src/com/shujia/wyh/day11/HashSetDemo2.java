package com.shujia.wyh.day11;

import java.util.HashSet;

/*
    使用HashSet存储自定义老师对象，当姓名和年龄一样的时候，认为是同一个老师
 */
public class HashSetDemo2 {
    public static void main(String[] args) {
        //创建集合对象
        HashSet<Teacher> set = new HashSet<>();

        //创建教师对象
        Teacher t1 = new Teacher("小虎", 18);
        Teacher t2 = new Teacher("笑哥", 19);
        Teacher t3 = new Teacher("杨老板", 20);
        Teacher t4 = new Teacher("小虎", 18);

        //向集合中添加元素
        set.add(t1);
        set.add(t2);
        set.add(t3);
        set.add(t4);

        System.out.println(set);


    }
}
