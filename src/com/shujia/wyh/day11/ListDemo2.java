package com.shujia.wyh.day11;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/*
    List接口中特有方法：因为List相关集合具备了索引的概念，所以提供的特有方法都是和索引相关
        void add(int index,E element)
        E remove(int index)
        E get(int index)
        E set(int index,E element)
        ListIterator listIterator()

 */
public class ListDemo2 {
    public static void main(String[] args) {
        //创建一个List集合
        List list1 = new ArrayList();
        //创建元素对象添加到集合中
        list1.add("hello");
        list1.add("world");
        list1.add("java");
        list1.add("hadoop");
        list1.add("hello");
        list1.add("hive");
        System.out.println(list1);
        System.out.println("--------------------------------");
        //[hello, world, java, hadoop, hive]
        //void add(int index,Object element)  // List集合索引是从左往右，从0开始的
//        list1.add(1,"mysql");
//        list1.add(-5,"mysql");  // java和python中的列表不同，没有负数的索引
//        System.out.println(list1);

        // Object remove(int index)  // 指定索引删除某一个元素，并且返回被删除的元素
        //根据索引删除对应位置上的元素
//        Object obj = list1.remove(2);
//        System.out.println(list1);
//        System.out.println(obj);

        //Object get(int index) // 根据索引获取对应位置上的值
//        Object obj = list1.get(3);
//        System.out.println(list1);
//        System.out.println(obj);

        //Object set(int index,Object element)
//        Object obj = list1.set(3,"spark"); // 指定索引设置值，返回被替换的元素
//        System.out.println(list1);
//        System.out.println(obj);


        //ListIterator listIterator()
        //是List集合特有的专属迭代器
//        ListIterator listIterator = list1.listIterator();
//        while (listIterator.hasNext()){
//            System.out.println(listIterator.next());
//        }
//        System.out.println("----------------------------------------");
//
//        //ListIterator迭代器中特有的方法
//        //hasPrevious() 判断上一个位置是否有元素  previous()  获取上一个元素
//        while (listIterator.hasPrevious()){
//            System.out.println(listIterator.previous());
//        }

    }
}
