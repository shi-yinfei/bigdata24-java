package com.shujia.wyh.day11;


/*
    静态导入
        1、必须保证使用的类要是静态的
        2、如果方法名冲突了，必须要写全部路径，建议，如果发生了，就不要使用静态导入了。

 */
public class StaticImportDemo1 {
    public static void main(String[] args) {

//        System.out.println(Math.max(10, 20));
//        System.out.println(max(10, 20));
        System.out.println(Math.max(10,20));
//        System.out.println(min(10, 30));

    }

    public static int max(int a, int b) {
        System.out.println("hello world");
        return a > b ? a : b;
    }
}
