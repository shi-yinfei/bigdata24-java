package com.shujia.wyh.day11;

import java.util.LinkedList;

/*
    请用LinkedList模拟栈数据结构的集合，并测试
    栈的特点：先进后出

    如果面试的时候，按照常规写法实现，得0分

    题目真正的意思是自己写一个集合类，底层是LinkedList
 */
public class ListTest3 {
    public static void main(String[] args) {

//        //创建集合对象
//        LinkedList linkedList = new LinkedList();
//
//        linkedList.add("hello");
//        linkedList.add("world");
//        linkedList.add("hadoop");
//        linkedList.add("hive");
//        linkedList.add("java");
//
//        for (int i = linkedList.size() - 1; i >= 0; i--) {
//            System.out.println(linkedList.get(i));
//        }

        //创建集合对象
        MyStack myStack = new MyStack();
        myStack.addElement("hello");
        myStack.addElement("word");
        myStack.addElement("java");
        myStack.addElement("hadoop");

        int size = myStack.getSize();

        for (int i = 0; i < size; i++) {
            System.out.println(myStack.getElement());
        }
    }
}
