package com.shujia.wyh.day11;

import java.util.Comparator;
import java.util.Scanner;
import java.util.TreeSet;

/*
    键盘录入5个学生信息(姓名,语文成绩,数学成绩,英语成绩),按照总分从高到低输出到控制台

 */
public class TreeSetTest1 {
    public static void main(String[] args) {
        //创建键盘录入对象
        Scanner sc = new Scanner(System.in);

        //创建一个TreeSet集合对象
        TreeSet<Student4> treeSet = new TreeSet<>(new Comparator<Student4>() {
            @Override
            public int compare(Student4 o1, Student4 o2) {
                //主要条件：按照总分从高到低
                int i = o2.getSumScore() - o1.getSumScore();
                //次要条件
                //总分一样，语文成绩不一定一样
                int i2 = (i == 0) ? o2.getChinese() - o1.getChinese() : i;
                //总分一样，语文一样，数学成绩不一定一样
                int i3 = (i2 == 0) ? o2.getMath() - o1.getMath() : i2;
                //各科成绩一样，姓名不一定一样
                int i4 = (i3 == 0) ? o2.getName().compareTo(o1.getName()) : i3;
                return i4;
            }
        });

        for (int i = 1; i <= 5; i++) {
            System.out.println("=================请输入第" + i + "个学生的信息==================");
            System.out.print("请输入姓名：");
            String name = sc.next();
            System.out.print("请输入语文成绩：");
            int chinese = sc.nextInt();
            System.out.print("请输入数学成绩：");
            int math = sc.nextInt();
            System.out.print("请输入英语成绩：");
            int english = sc.nextInt();
            Student4 student4 = new Student4(name, chinese, math, english);
            treeSet.add(student4);
        }

        System.out.println("====================学生成绩信息如下=========================");
        for (Student4 student4 : treeSet) {
            System.out.println("姓名：" + student4.getName() + ",语文成绩：" + student4.getChinese()
                    + ",数学成绩：" + student4.getMath() + ",英语成绩：" + student4.getEnglish() + ",总分：" + student4.getSumScore());
        }

    }
}
