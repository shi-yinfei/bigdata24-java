package com.shujia.wyh.day11;

import java.util.Comparator;
import java.util.TreeSet;

/*
    比较器排序：指的是在创建TreeSet集合对象的时候，构造方法中传入Comparator的对象，重写compare方法，来决定元素的去重和顺序
    按照年龄从小到大排序
 */
public class TreeSetDemo3 {
    public static void main(String[] args) {
        //借助匿名内部类创建TreeSet对象
        TreeSet<Student3> student3s = new TreeSet<Student3>(new Comparator<Student3>() {
            @Override
            public int compare(Student3 o1, Student3 o2) {
                //o1--待插入的元素
                //o2--已经存储在树的根节点元素
                int i = o1.getAge() - o2.getAge();
                //当年龄一样，姓名不一定一样
                int i2 = (i == 0) ? o1.getName().compareTo(o2.getName()) : i;
                return i2;
            }
        });

        //创建学生对象元素
        Student3 s1 = new Student3("陆澳", 18);
        Student3 s2 = new Student3("杨志", 17);
        Student3 s3 = new Student3("史俊超", 16);
        Student3 s4 = new Student3("小虎", 18);
        Student3 s5 = new Student3("陆澳", 18);

        //向集合中添加元素
        student3s.add(s1);
        student3s.add(s2);
        student3s.add(s3);
        student3s.add(s4);
        student3s.add(s5);

        System.out.println(student3s);

    }
}
