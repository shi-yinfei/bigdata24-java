package com.shujia.wyh.day11;

import java.util.LinkedList;

public class MyStack {
    private LinkedList list;

    MyStack(){
        list = new LinkedList();
    }

    public int getSize(){
        return list.size();
    }

    public void addElement(Object obj){
//        list.add(obj);
        list.addFirst(obj);
    }

    public Object getElement(){
//        return list.getFirst();
        return list.removeFirst();
    }

}
