package com.shujia.wyh.day11;


import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListDemo2 {
    public static void main(String[] args) {
        //创建集合对象
        ArrayList list = new ArrayList();

        //创建学生对象
        Student s1 = new Student("史俊超", 18);
        Student s2 = new Student("杨志", 17);
        Student s3 = new Student("张玮", 18);
        Student s4 = new Student("陆澳", 19);
        Student s5 = new Student("史俊超", 18);

        //需求：当学生对象的姓名和年龄一样的时候，我们认为是同一个学生，应该要做去重
        //对于ArrayList集合而言，底层就是一个数组，没有其他的任何算法，所以该需求无法实现，我们后面会学习HashSet进行去重
        list.add(s1);
        list.add(s2);
        list.add(s3);
        list.add(s4);
        list.add(s5);

        //遍历集合
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            Student student = (Student) iterator.next();
            System.out.println(student.getName() + "---" + student.getAge());
        }
    }
}
