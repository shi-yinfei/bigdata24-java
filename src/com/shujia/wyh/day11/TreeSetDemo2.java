package com.shujia.wyh.day11;

import java.util.TreeSet;

/*
    使用TreeSet存储自定义对象

    需求：当学生的姓名和年龄一样的时候，认为是同一个学生对象，按照年龄从小到大排序
    我们按照和存储Integer类型的案例编写存储自定义对象，发现运行时报错了。
    ClassCastException 类转换异常，com.shujia.wyh.day11.Student2 cannot be cast to java.lang.Comparable
 */
public class TreeSetDemo2 {
    public static void main(String[] args) {
        //创建集合对象
        TreeSet<Student2> set = new TreeSet<Student2>();

        //创建学生对象元素
        Student2 s1 = new Student2("陆澳", 18);
        Student2 s2 = new Student2("杨志", 17);
        Student2 s3 = new Student2("史俊超", 16);
        Student2 s4 = new Student2("小虎", 18);
        Student2 s5 = new Student2("陆澳", 18);

        //向集合中添加元素
        set.add(s1);
        set.add(s2);
        set.add(s3);
        set.add(s4);
        set.add(s5);

        System.out.println(set);
    }
}
