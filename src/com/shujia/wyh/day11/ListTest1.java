package com.shujia.wyh.day11;

import java.util.ArrayList;

/*
    去除集合中字符串的重复值(字符串的内容相同)

 */
public class ListTest1 {
    public static void main(String[] args) {
        ArrayList list = new ArrayList();

        list.add("hello");
        list.add("world");
        list.add("java");
        list.add("hello");
        list.add("java");

        //创建一个新的集合存储不重复的元素
        ArrayList list2 = new ArrayList();

        //遍历旧集合
        for (int i = 0; i < list.size(); i++) {
            String s = (String) list.get(i);
            if(!list2.contains(s)){  // String类已经重写过了equals方法
                //新集合不包含元素的时候，添加到新集合
                list2.add(s);
            }
        }

        System.out.println(list2);

//        System.out.println(list);
    }
}
