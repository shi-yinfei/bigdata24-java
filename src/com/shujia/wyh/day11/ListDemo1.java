package com.shujia.wyh.day11;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*
    Collection接口中子接口：List
    特点：元素有序(指的是存储和取出的顺序一致)，元素可以发生重复，并且有索引的概念，可以通过索引获取对应位置上的元素
    可以借助子类：ArrayList来创建对象
 */
public class ListDemo1 {
    public static void main(String[] args) {
        //创建一个List集合对象
        List list1 = new ArrayList();

        //创建元素对象添加到集合中
        list1.add("hello");
        list1.add("world");
        list1.add("java");
        list1.add("hadoop");
        list1.add("hive");

//        System.out.println(list1);
        Iterator iterator = list1.iterator();
        while (iterator.hasNext()) {
            Object next = iterator.next();
            String s = (String) next;
            System.out.println(s + "---" + s.length());
        }
    }
}
