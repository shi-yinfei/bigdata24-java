package com.shujia.wyh.day11;

import java.util.ArrayList;

/*
    为了简化Collection集合的遍历和数组的遍历，java提供了增强for循环遍历
    语句定义格式：
        for(元素的数据类型 变量名:Collection集合对象/数组){
            直接使用变量名;
        }

 */
public class ZengForDemo1 {
    public static void main(String[] args) {
        //创建一个集合对象
        ArrayList<String> list1 = new ArrayList<>();

        //向集合中添加元素
        list1.add("hello");
        list1.add("world");
        list1.add("java");
        list1.add("hadoop");
        list1.add("hello");
        list1.add("hive");

        for (String s : list1) {
            System.out.println(s + "---" + s.length());
        }

        System.out.println("----------------------------------------");
        int[] arr = {11,22,33,44,55};
        for(int i : arr){
            System.out.println(i);
        }
    }
}
