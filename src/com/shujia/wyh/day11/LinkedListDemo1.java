package com.shujia.wyh.day11;

import java.util.LinkedList;

/*
    LinkedList是List接口的一个实现子类
    特点：底层数据结构是链表，查询慢，增删快，线程不安全的集合，效率高
 */
public class LinkedListDemo1 {
    public static void main(String[] args) {
        //创建集合对象
        LinkedList linkedList = new LinkedList();

        //添加元素
        linkedList.add("hello");
        linkedList.add("world");
        linkedList.add("java");
        linkedList.add("hadoop");
        linkedList.add("hive");

        System.out.println(linkedList);
    }
}
