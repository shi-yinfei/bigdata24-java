package com.shujia.wyh.day11;

import java.util.ArrayList;

/*
    集合的嵌套遍历
 */
public class ListTest4 {
    public static void main(String[] args) {
        //创建一个集合表示数加大的集合
        ArrayList<ArrayList<Student>> shujia = new ArrayList<>();

        //创建22期的集合对象
        ArrayList<Student> erShiErClazz = new ArrayList<>();
        erShiErClazz.add(new Student("小花",18));
        erShiErClazz.add(new Student("小红",17));
        erShiErClazz.add(new Student("小白",19));
        shujia.add(erShiErClazz);

        //创建23期的集合对象
        ArrayList<Student> erShiSanClazz = new ArrayList<>();
        erShiSanClazz.add(new Student("花花",18));
        erShiSanClazz.add(new Student("小黑",19));
        shujia.add(erShiSanClazz);

        //创建一个24期的集合对象
        ArrayList<Student> erShiSiClazz = new ArrayList<>();
        erShiSiClazz.add(new Student("陆澳",16));
        erShiSiClazz.add(new Student("史俊超",15));
        shujia.add(erShiSiClazz);

        //使用增强for循环遍历
        for (ArrayList<Student> clazz : shujia) {
            System.out.println("-------------------------------");
            for (Student student : clazz) {
                System.out.println(student);
            }
        }



    }
}
