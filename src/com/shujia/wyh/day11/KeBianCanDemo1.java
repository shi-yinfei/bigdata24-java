package com.shujia.wyh.day11;

import java.util.Arrays;
import java.util.List;

/*
    可变参数概述及使用
 */
public class KeBianCanDemo1 {
    public static void main(String[] args) {
        //需求1：求两个数之和
        int a = 3;
        int b = 4;
        sum(a, b);

        //需求2：求三个数之和
        int c = 5;
        sum(a, b, c);

        //需求3：求4个数之和
        int d = 6;
        sum(a, b, c, d);

        //需求4：学生小红，考了三门试，98，89，99
        getSumScore("小红", 98, 89, 99);

        //可变参数的应用：
        //public static <T> List<T> asList(T... a)
        List<String> list = Arrays.asList("hello", "world", "java", "hadoop");
        List<Integer> list1 = Arrays.asList(10, 20, 30, 40);

    }

    //可变参数在定义的时候必须放在最后一个定义，一个方法的定义只能出现一个可变参数
    public static void getSumScore(String name, int... arr) {
        int sum = 0;
        for (int i : arr) {
            sum += i;
        }
        System.out.println(name + "的总分为：" + sum);
    }

    //可变参数应用到方法的定义
    //可变参数，会将同一种数据类型的元素接收进来，组合成一个数组，数组的名字就是形参的名字。
    public static void sum(int... arr) {
        int sum = 0;
        for (int i : arr) {
            sum += i;
        }

        System.out.println(sum);
    }


//    public static void sum(int a, int b, int c, int d) {
//        System.out.println(a + b + c + d);
//    }
//
//    public static void sum(int a, int b, int c) {
//        System.out.println(a + b + c);
//    }
//
//    public static void sum(int a, int b) {
//        System.out.println(a + b);
//    }
}
