package com.shujia.wyh.day11;

import java.util.HashSet;

/*
    Set接口的实现子类：HashSet
    特点：底层数据结构史哈希表
 */
public class HashSetDemo1 {
    public static void main(String[] args) {
        //创建集合对象
        HashSet<String> set1 = new HashSet<>();

        //向集合中添加元素
        //这里的add方法底层是根据元素类型中的hashCode方法和equals方法判断是否是同一个元素
        set1.add("hello");
        set1.add("world");
        set1.add("hadoop");
        set1.add("java");
        set1.add("world");
        set1.add("hello");

        System.out.println(set1);
    }
}
