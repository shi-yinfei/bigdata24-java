package com.shujia.wyh.day11;

import java.util.LinkedHashSet;

/*
    Set接口的实现子类：LinkedHashSet
    特点：底层数据结构是哈希表和双链表构成，线程不安全的，效率高，具有可预测的迭代顺序
    哈希表保证了元素的唯一性，链表保证了有序性

 */
public class LinkedHashSetDemo1 {
    public static void main(String[] args) {
        LinkedHashSet<String> strings = new LinkedHashSet<>();

        strings.add("hello");
        strings.add("world");
        strings.add("java");
        strings.add("hello");
        strings.add("world");

        System.out.println(strings);
    }
}
