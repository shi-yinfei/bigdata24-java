package com.shujia.wyh.day11;

import java.util.Enumeration;
import java.util.Vector;

/*
    List接口的第二个子类对象：Vector
    特点：底层数据结构是数组，查询快，增删慢，线程是安全的，效率低
    （即使是线程安全的，我们以后也不用它，因为后面我们会将不安全的ArrayList变成线程安全的）

    Vector集合的特有功能：
        public void addElement(Object obj)
        public Object elementAt(int index)
        public Enumeration elements()


 */
public class VectorDemo1 {
    public static void main(String[] args) {
        //创建一个集合对象
        Vector vector = new Vector();

        //添加元素
        vector.add("hello");
        vector.add("world");
        vector.add("hadoop");
        vector.add("hive");
        //public void addElement(Object obj)
//        vector.addElement("spark");  //该方法的效果和add方法一样，可以使用add方法替代
        // public Object elementAt(int index)
//        System.out.println(vector.get(2));
//        System.out.println(vector.elementAt(2)); //该方法的效果和get方法一样，可以使用get方法替代

        //public Enumeration elements() //该方法的效果和迭代器方法一样，可以使用迭代器方法替代
//        Enumeration elements = vector.elements();
//        while (elements.hasMoreElements()){
//            Object o = elements.nextElement();
//            System.out.println(o);
//        }


        System.out.println(vector);
    }
}
