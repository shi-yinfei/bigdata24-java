package com.shujia.wyh.day11;

public class Student2 implements Comparable<Student2> {
    private String name;
    private int age;

    public Student2() {
    }

    public Student2(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student2{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Student2 o) {
//        return 1;
        //this -- key
        //o -- 树中的根节点
        //主要条件：按照年龄从小到大排序
//        return this.getAge()-o.getAge();
        int i = this.getAge() - o.getAge();

        //次要条件：年龄一样，姓名不一定一样
        int i2 = (i == 0) ? this.getName().compareTo(o.getName()) : i;
        return i2;
    }
}
