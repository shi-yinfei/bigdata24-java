package com.shujia.wyh.day11;

import java.util.HashSet;
import java.util.Set;

/*
    Collection另一个子接口：Set
    Set相关集合的特点：元素唯一且无序（指的是存储和取出的顺序不一致）
    HashSet TreeSet
 */
public class SetDemo1 {
    public static void main(String[] args) {
        //创建集合对象
        Set<String> set1 = new HashSet<>();

        //向集合中添加元素
        set1.add("hello");
        set1.add("world");
        set1.add("hadoop");
        set1.add("java");
        set1.add("world");
        set1.add("hello");

        //使用增强for循环进行遍历
        for (String s1 : set1) {
            System.out.println(s1);
        }
    }
}
