package com.shujia.wyh.day11;

import java.util.ArrayList;
import java.util.Iterator;


/*
    即使集合可以存储任意的引用数据类型元素，我们今后也不这么做，一个集合只存储一种数据类型的元素
    回想一下，我们之前学习数组的时候，为什么同一个数组只能存储同一种数据类型
    int[] arr = new int[];

    如果创建一个集合的时候，也能像数组一样明确元素的数据类型就好了，如何在类上创建对象过程中明确存储的元素类型呢？
    java提供了一个机制，泛型机制。
    泛型语句定义格式：<只能存储引用数据类型>

    泛型的好处：
        1、明确规定了一个集合只能存储某一种数据类型
        2、在遍历的时候，无须做向下转型
        3、去掉了很多了黄色警告线，让程序看起来更加和谐

 */
public class ArrayListDemo3 {
    public static void main(String[] args) {
        //创建集合对象
        ArrayList<String> list = new ArrayList<>();

        //向集合中添加元素
        list.add("hello");
        list.add("world");
//        list.add(100);
//        list.add(12.34);

//        Iterator iterator = list.iterator();
//        while (iterator.hasNext()){
//
//        }

        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String s = iterator.next();
            System.out.println(s + "---" + s.length());
        }

    }
}
