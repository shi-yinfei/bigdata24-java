package com.shujia.wyh.day11;

import java.util.ArrayList;
import java.util.Iterator;

/*
    是List接口的实现子类：ArrayList
    特点：底层数据结构是数组，查询快，增删慢，线程不安全，效率高

 */
public class ArrayListDemo1 {
    public static void main(String[] args) {
        //创建集合对象
        ArrayList list = new ArrayList();

        //创建元素对象
        list.add("hello");
        list.add("world");
        list.add("java");
        list.add("hadoop");
        list.add("hive");

        //遍历集合
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            String s = (String) iterator.next();
            System.out.println(s + "--" + s.length());
        }
    }
}
